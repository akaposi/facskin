#!/bin/sh
cd -- "$(dirname "$BASH_SOURCE")"
ls FacsKin.jar 2>/dev/null
if [ $? -eq 0 ]
then
  /Library/Internet\ Plug-Ins/JavaAppletPlugin.plugin/Contents/Home/bin/java -cp 'FacsKin.jar:lib/*' facskin.FacsKinApp -open "$@"
else
  /Library/Internet\ Plug-Ins/JavaAppletPlugin.plugin/Contents/Home/bin/java -cp 'dist/FacsKin.jar:lib/*' facskin.FacsKinApp -open "$@"
fi
