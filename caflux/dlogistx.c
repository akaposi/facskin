#include <R.h>
#include <math.h>

// to compile: R CMD SHLIB dlogistx.c

//dlogistp (dlogist positive, először fölfele, majd lefele megy):
//    y0 + (y1 - y0) / (1 + ((x1 - x) / xd0) ^ (4 * xd0 * m0 / (y1 - y0))), ha x < x1
//    y2 + (y1 - y2) / (1 + ((x - x1) / xd2) ^ (4 * xd2 * m2 / (y2 - y1))), egyébként,
//    ahol y0, y1, y2, m0, x1, xd0, xd2 >= 0 && m2 <= 0 && xd0 <= x1 && y1 > y0 && y1 > y2
void dlogistp(double *x,
	      int *n_x,
	      double *_y0,
	      double *_y1,
	      double *_y2,
	      double *_x1,
	      double *_xd0,
	      double *_xd2,
	      double *_m0,
	      double *_m2,
	      int *n_pars,
	      double *y) {
  double y0, y1, y2, x1, xd0, xd2, m0, m2;
  for (int i = 0; i < *n_pars; i ++)
    for (int j = 0; j < *n_x; j ++) {
      y0 = fabs(_y0[i]);
      y1 = fabs(_y1[i]);
      y2 = fabs(_y2[i]);
      x1 = fabs(_x1[i]);
      xd0 = fabs(_xd0[i]);
      xd2 = fabs(_xd2[i]);
      m0 = fabs(_m0[i]);
      m2 = -fabs(_m2[i]);

      if (y0 > y1 && y0 >= y2) {
	double temp = y1;
	y1 = y0;
	y0 = temp;
      } else {
	if (y2 > y1) {
	  double temp = y1;
	  y1 = y2;
	  y2 = temp;
	}
      }

      if (x[j] < x1) {
	if (xd0 > x1) xd0 = x1;
	if (y1 == y0 || xd0 == 0)
	  y[(*n_pars) * j + i] = y0;
	else
	  y[(*n_pars) * j + i] = y0 + (y1 - y0) / (1 + pow((x1 - x[j]) / xd0, 4 * xd0 * m0 / (y1 - y0)));
      } else {
	if (x[j] == x1) {
	  y[(*n_pars) * j + i] = y1;
	} else {
	  if (y1 == y2 || xd2 == 0)
	    y[(*n_pars) * j + i] = y2;
	  else
	    y[(*n_pars) * j + i] = y2 + (y1 - y2) / (1 + pow((x[j] - x1) / xd2, 4 * xd2 * m2 / (y2 - y1)));
	}
      }
    }
}

//dlogistn (dlogist negative, először lefele, majd fölfele megy):
//    y0 + (y1 - y0) / (1 + ((x1 - x) / xd0) ^ (4 * xd0 * m0 / (y1 - y0))), ha x < x1
//    y2 + (y1 - y2) / (1 + ((x - x1) / xd2) ^ (4 * xd2 * m2 / (y2 - y1))), egyébként,
//    ahol y0, y1, y2, m2, x1, xd0, xd2 >= 0 && m0 <= 0 && xd0 <= x1 && y1 < y0 && y1 < y2
void dlogistn(double *x,
	      int *n_x,
	      double *_y0,
	      double *_y1,
	      double *_y2,
	      double *_x1,
	      double *_xd0,
	      double *_xd2,
	      double *_m0,
	      double *_m2,
	      int *n_pars,
	      double *y) {
  double y0, y1, y2, x1, xd0, xd2, m0, m2;
  for (int i = 0; i < *n_pars; i ++)
    for (int j = 0; j < *n_x; j ++) {
      y0 = fabs(_y0[i]);
      y1 = fabs(_y1[i]);
      y2 = fabs(_y2[i]);
      x1 = fabs(_x1[i]);
      xd0 = fabs(_xd0[i]);
      xd2 = fabs(_xd2[i]);
      m0 = -fabs(_m0[i]);
      m2 = fabs(_m2[i]);

      if (y0 < y1 && y0 <= y2) {
	double temp = y1;
	y1 = y0;
	y0 = temp;
      } else {
	if (y2 < y1) {
	  double temp = y1;
	  y1 = y2;
	  y2 = temp;
	}
      }

      if (x[j] < x1) {
	if (xd0 > x1) xd0 = x1;
	if (y1 == y0 || xd0 == 0)
	  y[(*n_pars) * j + i] = y0;
	else
	  y[(*n_pars) * j + i] = y0 + (y1 - y0) / (1 + pow((x1 - x[j]) / xd0, 4 * xd0 * m0 / (y1 - y0)));
      } else {
	if (x[j] == x1) {
	  y[(*n_pars) * j + i] = y1;
	} else {
	  if (y1 == y2 || xd2 == 0)
	    y[(*n_pars) * j + i] = y2;
	  else
	    y[(*n_pars) * j + i] = y2 + (y1 - y2) / (1 + pow((x[j] - x1) / xd2, 4 * xd2 * m2 / (y2 - y1)));
	}
      }
    }
}
