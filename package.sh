#!/bin/sh

./copy_private_data.sh

echo "compilation"
ant jar
if [ $? -ne 0 ]
then
  echo "error in compilation"
  exit
fi

#get FacsKin version
VERSION=`grep Application.version src/facskin/resources/FacsKinApp.properties | sed 's/.*= \(.*\)/\1/'`

#get release date
RELEASEDATE=`ls -l --time-style="+%d %b %Y" dist/FacsKin.jar | sed 's/.* \(.* .* .*\) dist.*/\1/'`

#this is the git repository where we distribute FacsKin
REPO=../facskin.bitbucket.org

echo "create zip bundle"
rm FacsKin.zip 2>/dev/null
cp -r dist FacsKin
cp -a FacsKin.bat FacsKin
cp -a FacsKin.sh FacsKin
cp -a FacsKin.command FacsKin
rm FacsKin/README.TXT 2>/dev/null
rm FacsKin/launch.* 2>/dev/null
rm -rf FacsKin/javadoc 2>/dev/null
mkdir -p FacsKin/lib
cp -a lib/AbsoluteLayout.jar FacsKin/lib
cp -a lib/appframework-1.0.3.jar FacsKin/lib
cp -a lib/commons-math-2.2.jar FacsKin/lib
cp -a lib/swing-worker-1.1.jar FacsKin/lib
mkdir FacsKin/caflux
cp -a caflux/fct-64.R FacsKin/caflux
cp -a caflux/dlogistx.c FacsKin/caflux
cp -a caflux/dlogistx-32.dll FacsKin/caflux
cp -a caflux/dlogistx-64.dll FacsKin/caflux
cp -a caflux/dlogistx.so FacsKin/caflux
mkdir FacsKin/analysis
cp -a run.R FacsKin/run.R
zip -r FacsKin.zip FacsKin
rm -rf FacsKin

echo "git pull in $REPO"
DIR=`pwd`
cd $REPO
git pull
cd $DIR

echo "copy zip bundle to $REPO"
cp -a FacsKin.zip $REPO

echo "copy documentation"
echo "User's Guide" >usersguide
# cutting off first last lines and replacing image urls with doc/ image url
tail -n +2 src/facskin/resources/usersguide.html | head -n -2 | sed 's/img src="/img src="doc\//' >>usersguide
echo "Introduction to using FacsKin" >shortintro
# cutting out the short introduction, also changing links to the usersguide
tail -n +4 usersguide | sed '/<!-- cut here -->/,$d' | sed 's/href="#/href="usersguide.html#/' >>shortintro
# copying stuff
cp usersguide $REPO/src/
cp shortintro $REPO/src/
rm -f $REPO/doc/*
mkdir -p $REPO/doc
cp -a src/facskin/resources/*.png $REPO/doc/
cp -a src/facskin/resources/*.jpg $REPO/doc/
echo $VERSION >$REPO/src/version
echo $RELEASEDATE >$REPO/src/releasedate

echo "delete temporary files"
rm FacsKin.zip 2>/dev/null
rm usersguide
rm shortintro

DIR=`pwd`
cd $REPO

echo "generate website"
./generate.sh

echo "commit changes in $REPO"
git add FacsKin-$VERSION.jar
git add FacsKin.zip
git add *.html
git add src/*
git add doc/*
git add icon.png
git commit -m "version $VERSION"

echo "pushing changes in $REPO" # url in $REPO/.git/config needs to be bitbucket_facskin instead of bitbucket.org
git push

cd $DIR
