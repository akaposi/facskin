#!/bin/sh
ls FacsKin.jar 2>/dev/null
if [ $? -eq 0 ]
then
  java -cp 'FacsKin.jar:lib/*' facskin.FacsKinApp -open "$@"
else
  java -cp 'dist/FacsKin.jar:lib/*' facskin.FacsKinApp -open "$@"
fi
