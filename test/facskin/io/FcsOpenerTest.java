package facskin.io;

import facskin.Arrays;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import java.io.FileInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ambi
 */
public class FcsOpenerTest {
    public FcsOpenerTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    private double[] getSummary(List<double[]> l, int par) {
        double min = Double.MAX_VALUE, max = Double.MIN_VALUE, sum = 0, median;

        ArrayList<Double> al = new ArrayList<Double>(l.size());

        for (int i = 0; i < l.size(); ++i) {
            if (l.get(i)[par] < min)
                min = l.get(i)[par];
            if (l.get(i)[par] > max)
                max = l.get(i)[par];
            sum += l.get(i)[par];
            al.add(l.get(i)[par]);
        }

        Collections.sort(al);

        if (al.size() % 2 == 1)
            median = al.get((al.size() - 1) / 2);
        else
            median = (al.get(al.size() / 2) + al.get(al.size() / 2 - 1)) / 2;

        return new double[] {min, max, sum / l.size(), median};
    }


    @Test
    public void testFcs2Mixed() {
        File baseline_file = new File("examples/baseline.fcs");
        try {
            FcsOpener baseline = new FcsOpener(new FileInputStream(baseline_file), (int) baseline_file.length() / 3, null);
            assertEquals(baseline.getFCSVersion(), "2.0");
            assertEquals(baseline.getParameterCount(), 7);
            assertEquals(baseline.getData().size(), 45375);
            assertEquals(baseline.getParamName(0), "FSC-A");
            assertEquals(baseline.getParamName(1), "SSC-A");
            assertEquals(baseline.getParamName(2), "FITC-A");
            assertEquals(baseline.getParamName(3), "APC-A");
            assertEquals(baseline.getParamName(4), "PerCP-Cy5-5-A");
            assertEquals(baseline.getParamName(5), "Ratio: FITC-A/PerCP-Cy5-5-A");
            assertEquals(baseline.getParamName(6), "Time");
            assertTrue(Arrays.eq(baseline.getData().get(0), new double[] {968, 678, 226, 1, 1004, 226, 0}));
            assertTrue(Arrays.eq(baseline.getData().get(4999), new double[] {564, 305, 67, 1, 653, 105, 1}));
            assertTrue(Arrays.eq(baseline.getData().get(20022), new double[] {140, 272, 202, 1, 39, 1023, 5}));
            assertTrue(Arrays.eq(baseline.getData().get(45374), new double[] {515, 399, 117, 1, 1013, 117, 17}));
            assertTrue(Arrays.eq(getSummary(baseline.getData(), 0), new double[] {66, 1023.000, 521.32055, 503}));
            assertTrue(Arrays.eq(getSummary(baseline.getData(), 1), new double[] {3, 1023.000, 439.50431, 304}));
            assertTrue(Arrays.eq(getSummary(baseline.getData(), 2), new double[] {0, 1023.000, 160.49393, 108}));
            assertTrue(Arrays.eq(getSummary(baseline.getData(), 3), new double[] {1, 9646.277, 39.92588, 1}));
            assertTrue(Arrays.eq(getSummary(baseline.getData(), 4), new double[] {0, 1015.000, 721.07134, 816}));
            assertTrue(Arrays.eq(getSummary(baseline.getData(), 5), new double[] {0, 1023.000, 250.38129, 145}));
            assertTrue(Arrays.eq(getSummary(baseline.getData(), 6), new double[] {0, 17.000, 7.11654, 6}));
        } catch (FileNotFoundException ex) {
            fail("input file not found");
        } catch (IOException ex) {
            fail("IO exception");
        }
    }
    

    @Test
    public void testFcs2AllLin() {
        File fcs2_all_lin_file = new File("examples/fcs2_all_lin.fcs");
        try {
            FcsOpener fcs2_all_lin = new FcsOpener(new FileInputStream(fcs2_all_lin_file), (int) fcs2_all_lin_file.length() / 3, null);
            assertEquals(fcs2_all_lin.getFCSVersion(), "2.0");
            assertEquals(fcs2_all_lin.getParameterCount(), 11);
            assertEquals(fcs2_all_lin.getData().size(), 836116);
            assertEquals(fcs2_all_lin.getParamName(0), "FSC-A");
            assertEquals(fcs2_all_lin.getParamName(1), "SSC-A");
            assertEquals(fcs2_all_lin.getParamName(2), "APC-Cy7-A");
            assertEquals(fcs2_all_lin.getParamName(3), "APC-A");
            assertEquals(fcs2_all_lin.getParamName(4), "PE-Cy7-A");
            assertEquals(fcs2_all_lin.getParamName(5), "PerCP-Cy5-5-A");
            assertEquals(fcs2_all_lin.getParamName(6), "PE-Texas Red-A");
            assertEquals(fcs2_all_lin.getParamName(7), "PE-A");
            assertEquals(fcs2_all_lin.getParamName(8), "FITC-A");
            assertEquals(fcs2_all_lin.getParamName(9), "Ratio: FITC-A/PerCP-Cy5-5-A");
            assertEquals(fcs2_all_lin.getParamName(10), "Time");
            assertTrue(Arrays.eq(fcs2_all_lin.getData().get(0), new double[] {195, 77, 18, 6, 24, 2, 0, 1, 71, 1023, 0}));
            assertTrue(Arrays.eq(fcs2_all_lin.getData().get(4999), new double[] {103, 92, 0, 0, 1, 5, 0, 3, 30, 1023, 1}));
            assertTrue(Arrays.eq(fcs2_all_lin.getData().get(20022), new double[] {465, 298, 1, 0, 7, 42, 0, 5, 161, 905, 7}));
            assertTrue(Arrays.eq(fcs2_all_lin.getData().get(200322), new double[] {185, 99, 0, 0, 0, 1, 0, 22, 5, 443, 74}));
            assertTrue(Arrays.eq(fcs2_all_lin.getData().get(836115), new double[] {289, 117, 0, 0, 90, 17, 1, 0, 17, 284, 353}));
            assertTrue(Arrays.eq(getSummary(fcs2_all_lin.getData(), 0), new double[] {15, 1023, 291.5433887, 265}));
            assertTrue(Arrays.eq(getSummary(fcs2_all_lin.getData(), 1), new double[] {0, 1023, 186.3442836, 109}));
            assertTrue(Arrays.eq(getSummary(fcs2_all_lin.getData(), 2), new double[] {0, 962, 1.5888585, 0}));
            assertTrue(Arrays.eq(getSummary(fcs2_all_lin.getData(), 3), new double[] {0, 989, 0.3678939, 0}));
            assertTrue(Arrays.eq(getSummary(fcs2_all_lin.getData(), 4), new double[] {0, 776, 16.2375424, 0}));
            assertTrue(Arrays.eq(getSummary(fcs2_all_lin.getData(), 5), new double[] {0, 1012, 18.4995228, 12}));
            assertTrue(Arrays.eq(getSummary(fcs2_all_lin.getData(), 6), new double[] {0, 548, 1.1682494, 0}));
            assertTrue(Arrays.eq(getSummary(fcs2_all_lin.getData(), 7), new double[] {0, 1020, 6.5046525, 0}));
            assertTrue(Arrays.eq(getSummary(fcs2_all_lin.getData(), 8), new double[] {0, 1020, 48.1040262, 19}));
            assertTrue(Arrays.eq(getSummary(fcs2_all_lin.getData(), 9), new double[] {0, 1023, 534.8239204, 359}));
            assertTrue(Arrays.eq(getSummary(fcs2_all_lin.getData(), 10), new double[] {0, 353, 161.7928888, 159}));
        } catch (FileNotFoundException ex) {
            fail("input file not found");
        } catch (IOException ex) {
            fail("IO exception");
        }
    }

    

    @Test
    public void testFcs3AllLinWOCompensation() {
        File fcs3_all_lin_file = new File("examples/fcs3_all_lin.fcs");
        try {
            FcsOpener fcs3_all_lin = new FcsOpener(new FileInputStream(fcs3_all_lin_file), (int) fcs3_all_lin_file.length() / 3, false, null);
            assertEquals(fcs3_all_lin.getFCSVersion(), "3.0");
            assertEquals(fcs3_all_lin.getParameterCount(), 11);
            assertEquals(fcs3_all_lin.getData().size(), 836116);
            assertEquals(fcs3_all_lin.getParamName(0), "FSC-A");
            assertEquals(fcs3_all_lin.getParamName(1), "SSC-A");
            assertEquals(fcs3_all_lin.getParamName(2), "APC-Cy7-A");
            assertEquals(fcs3_all_lin.getParamName(3), "APC-A");
            assertEquals(fcs3_all_lin.getParamName(4), "PE-Cy7-A");
            assertEquals(fcs3_all_lin.getParamName(5), "PerCP-Cy5-5-A");
            assertEquals(fcs3_all_lin.getParamName(6), "PE-Texas Red-A");
            assertEquals(fcs3_all_lin.getParamName(7), "PE-A");
            assertEquals(fcs3_all_lin.getParamName(8), "FITC-A");
            assertEquals(fcs3_all_lin.getParamName(9), "Ratio: FITC-A/PerCP-Cy5-5-A");
            assertEquals(fcs3_all_lin.getParamName(10), "Time");

            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 0), new double[] {3869.0, 262143.0, 74764.1720, 67840.00}));
            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 1), new double[] {-308.0, 262143.0, 47833.0477, 28007.00}));
            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 2), new double[] {-126.0, 262143.0, 726.9960, 109.00}));
            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 3), new double[] {-111.0, 262143.0, 222.4507, 64.00}));
            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 4), new double[] {-211.4, 262143.0, 6885.4255, 2307.20}));
            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 5), new double[] {-246.4, 262143.0, 5263.7346, 3413.20}));
            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 6), new double[] {-380.8, 262143.0, 4995.5912, 3098.20}));
            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 7), new double[] {-1134.0, 262143.0, 6145.3653, 2895.20}));
            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 8), new double[] {-358.4, 262143.0, 13002.2030, 5455.80}));
            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 9), new double[] {0.0, 262143.0, 137071.3792, 92098.16}));
            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 10), new double[] {0.0, 90513.4, 41546.9052, 40843.10}));

            assertTrue(Arrays.eq(fcs3_all_lin.getData().get(0), new double[] {50075, 19717.6, 5332, 2135, 6805.4, 1024.8, 1904.0, 4636.8, 18356.8, 262143.00, 0.0}));
            assertTrue(Arrays.eq(fcs3_all_lin.getData().get(4999), new double[] {26613, 23678.2, 70, 67, 1183.0, 1561.0, 1667.4, 3173.8, 8005.2, 262143.00, 479.0}));
            assertTrue(Arrays.eq(fcs3_all_lin.getData().get(20022), new double[] {119230, 76437.2, 658, 74, 8598.8, 12006.4, 10911.6, 14648.2, 42487.2, 231912.19, 2020.7}));
            assertTrue(Arrays.eq(fcs3_all_lin.getData().get(200322), new double[] {47427, 25599.0, 260, 222, 274.4, 1006.6, 2202.2, 6211.8, 1744.4, 113571.00, 19127.1}));
            assertTrue(Arrays.eq(fcs3_all_lin.getData().get(836115), new double[] {74220, 30154.6, 1688, 23, 25681.6, 4515.0, 3731.0, 2738.4, 5012.0, 72749.76, 90513.4}));
        } catch (FileNotFoundException ex) {
            fail("input file not found");
        } catch (IOException ex) {
            fail("IO exception");
        }
    }

    @Test
    public void testFcs3AllLinWCompensation() {
        File fcs3_all_lin_file = new File("examples/fcs3_all_lin.fcs");
        try {
            FcsOpener fcs3_all_lin = new FcsOpener(new FileInputStream(fcs3_all_lin_file), (int) fcs3_all_lin_file.length() / 3, true, null);

            //parameters of the comp. matrix: "APC-Cy7-A", "APC-A", "PE-Cy7-A", "PerCP-Cy5-5-A", "PE-Texas Red-A", "PE-A", "FITC-A"

            double[][] comp_mat =
            {{1.0000000000, 0.076592093, 0.01768381, 0.000000000, 0.000000000, 0.0000000, 0.0006685977},
            {0.0650759557, 1.000000000, 0.00000000, 0.000000000, 0.000000000, 0.0000000, 0.0219651429},
            {0.0809315949, 0.001497072, 1.00000000, 0.001137954, 0.001287393, 0.0037141, 0.0008338042},
            {0.0018954676, 0.005838020, 0.56898172, 1.000000000, 0.646323697, 0.3879094, 0.1085522555},
            {0.0000000000, 0.000000000, 0.00000000, 0.000000000, 1.000000000, 0.0000000, 0.0000000000},
            {0.0011529030, 0.008070332, 0.03015574, 0.098005999, 0.335846398, 1.0000000, 0.0325875717},
            {0.0001511704, 0.001632608, 0.01086709, 0.021963757, 0.087280954, 0.2158491, 1.0000000000}};

            // is the comp matrix read in normally:
            for (int i = 0; i < fcs3_all_lin.getCompensationMatrix().length; ++i) {
                assertTrue(Arrays.eq(fcs3_all_lin.getCompensationMatrix()[i], comp_mat[i]));
            }

            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 0), new double[] {3869.0000, 262143.0, 74764.1720, 67840.00000}));
            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 1), new double[] {-308.0000, 262143.0, 47833.0477, 28007.00000}));
            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 2), new double[] {-9795.9615, 246492.0, 386.5429, 29.38343}));
            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 3), new double[] {-7252.6789, 253238.9, 125.7349, 23.39115}));
            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 4), new double[] {-66617.8609, 198826.9, 3946.7640, 35.76342}));
            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 5), new double[] {-941.1061, 258978.3, 4832.0486, 3086.32548}));
            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 6), new double[] {-47797.6904, 140538.9, 254.2990, 25.74280}));
            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 7), new double[] {-9243.6542, 261218.0, 1575.4591, -19.02848}));
            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 8), new double[] {-11920.7582, 261280.8, 12420.0218, 4899.98499}));
            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 9), new double[] {0.0000, 262143.0, 137071.3792, 92098.16016}));
            assertTrue(Arrays.eq(getSummary(fcs3_all_lin.getData(), 10), new double[] {0.0000, 90513.4, 41546.9052, 40843.09961}));

            assertTrue(Arrays.eq(fcs3_all_lin.getData().get(0),      new double[] {50075, 19717.6, 4714.68346, 1727.84284, 6184.3346, 572.5924, -218.6360, 456.02754, 18233.521, 262143.00, 0.0}));
            assertTrue(Arrays.eq(fcs3_all_lin.getData().get(4999),   new double[] {26613, 23678.2, 36.03395, 35.49615, 332.2761, 1292.4907, -181.3307, 980.69728, 7831.858, 262143.00, 479.0}));
            assertTrue(Arrays.eq(fcs3_all_lin.getData().get(20022),  new double[] {119230, 76437.2, 485.43215, -109.27446, 1865.3628, 10952.3425, -269.9728, 1488.91596, 41250.298, 231912.19, 2020.7}));
            assertTrue(Arrays.eq(fcs3_all_lin.getData().get(200322), new double[] {47427, 25599.0, 254.97207, 151.61416, -153.8401, 412.3814, -119.1380, 5726.54833, 1509.648, 113571.00, 19127.1}));
            assertTrue(Arrays.eq(fcs3_all_lin.getData().get(836115), new double[] {74220, 30154.6, -191.61176, -29.76631, 23137.7047, 4392.0309, 477.0937, -26.36223, 4517.584, 72749.76, 90513.4}));

        } catch (FileNotFoundException ex) {
            fail("input file not found");
        } catch (IOException ex) {
            fail("IO exception");
        }
    }

    @Test
    public void testCFlowFCS() {
        File fcs_file = new File("examples/cflow.fcs");
        try {
            FcsOpener fcs = new FcsOpener(new FileInputStream(fcs_file), (int) fcs_file.length() / 3, true, null);
            assertEquals(fcs.getParameterCount(), 14);
            assertEquals(fcs.getData().size(), 51986);
        } catch (FileNotFoundException ex) {
            fail("input file not found");
        } catch (IOException ex) {
            fail("IO exception");
        }
    }

    @Test
    public void testLMD() {
        File fcs_file = new File("examples/a.lmd");
        try {
            FcsOpener fcs = new FcsOpener(new FileInputStream(fcs_file), (int) fcs_file.length() / 3, true, null);
            assertEquals(fcs.getParameterCount(), 11);
            assertEquals(fcs.getData().size(), 20000);
        } catch (FileNotFoundException ex) {
            fail("input file not found");
        } catch (IOException ex) {
            fail("IO exception");
        }
    }

    @Test
    public void testVersion_0_6_16() {
        File fcs_file = new File("examples/version-0.6.16.fcs");
        try {
            FcsOpener fcs = new FcsOpener(new FileInputStream(fcs_file), (int) fcs_file.length() / 3, true, null);
	    
            assertEquals(fcs.getParameterCount(), 12);
            assertEquals(fcs.getData().size(), 155802);

            double[][] comp_mat =
		{{1.0,0.1835342111,0.1827243241,0.0},
		 {0.0354478311,1.0,0.235776189,8.02545E-5},
		 {-4.48817E-5,0.0,1.0,0.0064826832},
		 {0.0045742895,0.0010213611,0.3614257164,1.0}};

            for (int i = 0; i < fcs.getCompensationMatrix().length; ++i) {
                assertTrue(Arrays.eq(fcs.getCompensationMatrix()[i], comp_mat[i]));
            }
	    
            assertTrue(Arrays.eq(fcs.getData().get(0), new double[] {111402.7421875, 97976.0, 74517.1171875, 53410.0078125, 43550.0, 80373.7890625, 0.0010000000474974513, 0.0, 168.30746459960938, 14610.9326171875, 18.746795654296875, 45.34587860107422}));
            assertTrue(Arrays.eq(fcs.getData().get(155801), new double[] {105786.5625, 74351.0, 93244.5859375, 109075.8359375, 67246.0, 106302.140625, 119.72200012207031, 155801.0, 654.2083129882812, -19.235645294189453, -46.536739349365234, 35.283226013183594}));
	    
        } catch (FileNotFoundException ex) {
            fail("input file not found");
        } catch (IOException ex) {
            fail("IO exception");
        }
    }
}
