package facskin.io;

import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.File;
import facskin.formats.KineticsData;
import java.io.OutputStream;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ambi
 */
public class KineticsDataWriterTest {

    public KineticsDataWriterTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of write method, of class KineticsDataWriter.
     */
    @Test
    public void testWrite() throws Exception {
        KineticsData data = new KineticsData(new FileInputStream(new File("examples/1.kinetics")), "1.kinetics", 500, null);

        // System.out.println("write");
        OutputStream stream = new FileOutputStream(new File("examples/1_output.kinetics"));
        KineticsDataWriter.write(stream, data);

        File data1_file = new File("examples/1_output.kinetics");
        KineticsData data1 = new KineticsData(new FileInputStream(data1_file), "1.kinetics", 500, null);
        data1_file.delete();

        assertTrue(KineticsComparison.compare(data, data1));
    }
}
