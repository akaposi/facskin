/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facskin.io;

import facskin.formats.FcsData;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ambi
 */
public class FcsWriterTest {
    
    // from https://stackoverflow.com/questions/22818590/java-how-to-check-that-2-binary-files-are-same
    private static boolean binaryDiff(File a, File b) throws IOException {
        if (a.length() != b.length()) return false;
        final int BLOCK_SIZE = 128;
        InputStream aStream = new FileInputStream(a);
        InputStream bStream = new FileInputStream(b);
        byte[] aBuffer = new byte[BLOCK_SIZE];
        byte[] bBuffer = new byte[BLOCK_SIZE];
        while (true) {
            int aByteCount = aStream.read(aBuffer, 0, BLOCK_SIZE);
            bStream.read(bBuffer, 0, BLOCK_SIZE);
            if (aByteCount < 0) return true;
            if (!Arrays.equals(aBuffer, bBuffer)) return false;
        }
    }
    
    @Test
    public void testSame() {
        FcsData data;
        try {
            File oldFile = new File("examples/1.baseline_gated.fcs");
            File newFile  = new File("examples/1.baseline_gated_1.fcs");
            data = new FcsData(oldFile, null);
            facskin.io.FcsWriter.write(newFile, data, null);
            if (!binaryDiff(oldFile, newFile))
                fail("files don't match");
            newFile.delete();
        } catch (IOException ex) {
            fail("input file not found");
        }
    }
}
