/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package facskin.io;

import java.io.File;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ambi
 */
public class KineticsZipTest {

    private static KineticsZip kz, kz1;

    private double delta = 0.0001;


    public KineticsZipTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        File f = new File("examples/cf.zip");
        kz = new KineticsZip(f, null);

        File f1 = new File("examples/usersguide_paired_example.zip");
        kz.writeDatas(f1);

        kz1 = new KineticsZip(f1, null);
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        File f1 = new File("examples/usersguide_paired_example.zip");
        f1.delete();
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getDatas method, of class KineticsZip.
     */
    @Test
    public void testGetDatas() {
        assertEquals(kz.getDatas().size(), kz1.getDatas().size());

        for (int i = 0; i < kz.getDatas().size(); ++i)
            assertTrue(KineticsComparison.compare(kz.getDatas().get(i), kz1.getDatas().get(i)));
    }

    /**
     * Test of getGroups method, of class KineticsZip.
     */
    @Test
    public void testGetGroups() {
        assertEquals(kz.getGroups().size(), kz1.getGroups().size());

        for (int i = 0; i < kz.getGroups().size(); ++i)
            assertEquals(kz.getGroups().get(i), kz1.getGroups().get(i));
    }

    /**
     * Test of getPairs method, of class KineticsZip.
     */
    @Test
    public void testGetPairs() {
        assertEquals(kz.getPairs().size(), kz1.getPairs().size());

        for (int i = 0; i < kz.getPairs().size(); ++i) {
            if (kz.getPairs().get(i) != null && kz1.getPairs().get(i) != null)
                assertTrue(KineticsComparison.compare(kz.getPairs().get(i), kz1.getPairs().get(i)));
            else if (!(kz.getPairs().get(i) == null && kz1.getPairs().get(i) == null))
                fail("Pairs are not the same.");
        }
    }

    /**
     * Test of getSelectedFunction method, of class KineticsZip.
     */
    @Test
    public void testGetSelectedFunction() {
        assertEquals(kz.getSelectedFunction(), kz1.getSelectedFunction());
    }

    /**
     * Test of getMaxTime method, of class KineticsZip.
     */
    @Test
    public void testGetMaxTime() {
        assertEquals(kz.getMaxTime(), kz1.getMaxTime(), delta);
    }
}
