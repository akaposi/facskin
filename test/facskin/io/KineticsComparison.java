package facskin.io;

import facskin.Arrays;
import facskin.formats.KineticsData;

/**
 *
 * @author ambi
 */
public class KineticsComparison {
    
    public static boolean compare(KineticsData data, KineticsData data1) {

        if (data.getBestFunction() != data1.getBestFunction()) return false;
        if (data.getFileLength() != data1.getFileLength()) return false;
        if (!data.getFilename().equals(data1.getFilename())) return false;

        if (data.getFunctionCount() != data1.getFunctionCount()) return false;

        for (int i = 0; i < data.getFunctionCount(); ++i) {
            if (!Arrays.eq(data.getFunction(i).getMedianParams(),
                    data1.getFunction(i).getMedianParams())) return false;

            for (int j = 0; j < data.getFunction(i).getParamCount(); ++j) {
                if (data.getFunction(i).getParam(j).getDataLength() !=
                        data1.getFunction(i).getParam(j).getDataLength()) return false;

                for (int k = 0; k < data.getFunction(i).getParam(j).getDataLength(); ++k)
                    if (!Arrays.eq(data.getFunction(i).getParam(j).getData(k),
                            data1.getFunction(i).getParam(j).getData(k))) return false;
            }
        }

        if (!Arrays.eq(data.getFirstY(), data1.getFirstY())) return false;
        if (!Arrays.eq(data.getLastY(), data1.getLastY())) return false;
        if (!Arrays.eq(data.getMaxX(), data1.getMaxX())) return false;
        if (!Arrays.eq(data.getMaxY(), data1.getMaxY())) return false;

        for (int i = 0; i < data.getMedians().length; ++i)
            if (!Arrays.eq(data.getMedians()[i], data1.getMedians()[i])) return false;

        if (!Arrays.eq(data.getStandardizer(), data1.getStandardizer())) return false;

        return true;
    }
}
