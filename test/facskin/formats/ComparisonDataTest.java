package facskin.formats;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ambi
 */
public class ComparisonDataTest {

    private static double delta = 0.000001;

    public ComparisonDataTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addData method, of class ComparisonData.
     */
    @Test
    public void testAddData_Data() {
        ComparisonData d = new ComparisonData("test_data");
        for (double i = 0; i < 100; i += 1.2)
            d.addData(i);
        assertEquals(d.getDataLength(), 84);

        int j = 0;
        for (double i = 0; i < 100; i += 1.2) {
            assertEquals(d.getData(j), i, delta);
            ++ j;
        }

        ComparisonData d1 = new ComparisonData("test_data");
        for (double i = 0; i < 50; i += 1.2)
            d1.addData(i);
        assertEquals(d1.getDataLength(), 42);

        // addData(Data) to a non-empty Data:

        d.addData(d1);
        assertEquals(d.getDataLength(), 126);


        // we test whether the same numbers are ther that we have put in:

        List<Double> tl = new ArrayList<Double>(d.getDataLength());
        for (double i = 0.0; i < 100; i += 1.2)
            tl.add(i);
        for (double i = 0.0; i < 50; i += 1.2)
            tl.add(i);
        Collections.sort(tl);

        List<Double> td = new ArrayList<Double>(d.getDataLength());
        for (int i = 0; i < d.getDataLength(); ++i)
            td.add(d.getData(i));
        Collections.sort(td);

        for (int i = 0; i < d.getDataLength(); ++i)
            assertEquals(tl.get(i), td.get(i), delta);

        // addData(Data) to an empty Data:

        ComparisonData d2 = new ComparisonData("test_data");
        d2.addData(d1);
        assertEquals(d2.getDataLength(), 42);
    }

    /**
     * Test of addData method, of class ComparisonData.
     */
    @Test
    public void testAddData_double() {
        // see testAddData_Data()
    }

    /**
     * Test of getName method, of class ComparisonData.
     */
    @Test
    public void testGetName() {
        ComparisonData d = new ComparisonData("test_data");
        assertEquals(d.getName(), "test_data");
    }

    /**
     * Test of setName method, of class ComparisonData.
     */
    @Test
    public void testSetName() {
        ComparisonData d = new ComparisonData("test_data");
        assertEquals(d.getName(), "test_data");
        d.setName("this_is_aNEWNAME");
        assertEquals(d.getName(), "this_is_aNEWNAME");
    }

    /**
     * Test of getDataLength method, of class ComparisonData.
     */
    @Test
    public void testGetDataLength() {
        // see testAddData_Data()
    }

    /**
     * Test of getData method, of class ComparisonData.
     */
    @Test
    public void testGetData() {
        // see testAddData_Data()
    }

    /**
     * Test of getMidData method, of class ComparisonData.
     */
    @Test
    public void testGetMidData() {
        ComparisonData d = new ComparisonData("aaa");
        d.addData(1);
        assertEquals(d.getMidData(), 1, delta);
        d.addData(1);
        d.addData(2);
        assertEquals(d.getMidData(), 1, delta);
        d.addData(2);
        d.addData(2);
        d.addData(3);
        d.addData(2);
        d.addData(1);
        assertEquals(d.getMidData(), 2, delta);
    }

    /**
     * Test of getQuantile method, of class ComparisonData.
     */
    @Test
    public void testGetQuantile() {
        ComparisonData d = new ComparisonData("aaa");
        d.addData(1);
        assertEquals(d.getQuantile(0.5), 1, delta);
        d.addData(2);
        assertEquals(d.getQuantile(0.333333333333333), 1, delta);
        assertEquals(d.getQuantile(0.666666666666666), 2, delta);
        d.addData(3);
        assertEquals(d.getQuantile(0.25), 1, delta);
        assertEquals(d.getQuantile(0.50), 2, delta);
        assertEquals(d.getQuantile(0.75), 3, delta);
        assertEquals(d.getQuantile(1.0), 3, delta);
    }

}