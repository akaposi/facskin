/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package facskin.formats;

import java.io.IOException;
import java.util.Random;
import facskin.Arrays;
import java.io.File;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ambi
 */
public class FcsDataTest {

    private static double delta = 0.0001;
    private static int repeatCount = 30;

    private static FcsData data, data1;

    public FcsDataTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        data = new FcsData(new File("examples/baseline.fcs"), null);
        data1 = new FcsData(new File("examples/test.fcs"), null);
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getFilename method, of class FcsData.
     */
    @Test
    public void testGetFilename() {
        assertEquals(data.getFilename(), "baseline.fcs");
        assertEquals(data1.getFilename(), "test.fcs");
    }

    /**
     * Test of getMeasurementTime method, of class FcsData.
     */
    @Test
    public void testGetMeasurementTime() {
        assertEquals(data.getMeasurementTime(), "01-JUN-2006 15:51:58");
        assertEquals(data1.getMeasurementTime(), "01-JUN-2006 15:06:40");
    }

    /**
     * Test of getGateDescription method, of class FcsData.
     */
    @Test
    public void testGetGateDescription() {
    }

    /**
     * Test of getDescription method, of class FcsData.
     */
    @Test
    public void testGetDescription() {
    }

    /**
     * Test of getParameterName method, of class FcsData.
     */
    @Test
    public void testGetParameterName() {
        assertEquals(data.getParameterName(0), "FSC-A");
        assertEquals(data.getParameterName(1), "SSC-A");
        assertEquals(data.getParameterName(2), "FITC-A");
        assertEquals(data.getParameterName(3), "APC-A");
        assertEquals(data.getParameterName(4), "PerCP-Cy5-5-A");
        assertEquals(data.getParameterName(5), "Ratio: FITC-A/PerCP-Cy5-5-A");
        assertEquals(data.getParameterName(6), "Time");
        assertEquals(data1.getParameterName(0), "FSC-A");
        assertEquals(data1.getParameterName(1), "SSC-A");
        assertEquals(data1.getParameterName(2), "FITC-A");
        assertEquals(data1.getParameterName(3), "APC-A");
        assertEquals(data1.getParameterName(4), "PerCP-Cy5-5-A");
        assertEquals(data1.getParameterName(5), "Ratio: FITC-A/PerCP-Cy5-5-A");
        assertEquals(data1.getParameterName(6), "Time");
    }

    /**
     * Test of getParameterCount method, of class FcsData.
     */
    @Test
    public void testGetParameterCount() {
        assertEquals(data.getParameterCount(), 7);
        assertEquals(data1.getParameterCount(), 7);
    }

    /**
     * Test of getEventCount method, of class FcsData.
     */
    @Test
    public void testGetEventCount() {
        data.clearGates(null);
        data1.clearGates(null);
        assertEquals(data.getEventCount(), 45375);
        assertEquals(data1.getEventCount(), 13013);
    }

    /**
     * Test of getEvent method, of class FcsData.
     */
    @Test
    public void testGetEvent() {
        assertTrue(Arrays.eq(data.getEvent(0), new double[] {968, 678, 226, 1, 1004, 226, 0}));
        assertTrue(Arrays.eq(data.getEvent(4999), new double[] {564, 305, 67, 1, 653, 105, 2.56}));
        assertTrue(Arrays.eq(data.getEvent(20022), new double[] {140, 272, 202, 1, 39, 1023, 12.8}));
        assertTrue(Arrays.eq(data.getEvent(45374), new double[] {515, 399, 117, 1, 1013, 117, 43.52}));
    }

    /**
     * Test of histGate method, of class FcsData.
     */
    @Test
    public void testHistGates() {
        data.clearGates(null);
        data.histGates(3, 0, new double[] {0.233}, new double[] {4.0}, true, false, null);
        assertEquals(data.getEventCount(), 11101);
        data.histGates(3, 0, new double[] {4.0}, new double[] {5.0}, true, false, null);
        assertEquals(data.getEventCount(), 0);
    }

    /**
     * Test of inverseHistGate method, of class FcsData.
     */
    @Test
    public void testInverseHistGate() {
        data.clearGates(null);
        data.inverseHistGates(3, 0, new double[] {0.233}, new double[] {4.0}, true, false, null);
        assertEquals(data.getEventCount(), 45375 - 11101);
        data.inverseHistGates(3, 0, new double[] {4.0}, new double[] {5.0}, true, false, null);
        assertEquals(data.getEventCount(), 45375 - 11101);
    }

    /**
     * Test of plotGate method, of class FcsData.
     */
    @Test
    public void testPlotGates() {
        data1.clearGates(null);
        data1.plotGates(0, 1, new double[] {309, 655, 666, 316}, new double[] {442, 447, 51, 44}, false, false, null);
        assertEquals(data1.getEventCount(), 7444);
        data1.plotGates(6, 6, new double[] {7, 30, 30}, new double[] {1, 24, 2}, false, false, null);
        assertEquals(data1.getEventCount(), 0);

        data.clearGates(null);
        data.plotGates(1, 4, new double[] {100, 400, 200}, new double[] {900, 900, 600}, false, false, null);
        int n = data.getEventCount();
        data.plotGates(1, 4, new double[] {100, 400, 200}, new double[] {900, 900, 600}, false, false, null);
        assertEquals(data.getEventCount(), n);
    }

    /**
     * Test of inversePlotGate method, of class FcsData.
     */
    @Test
    public void testInversePlotGates() {
        data1.clearGates(null);
        data1.inversePlotGates(0, 1, new double[] {309, 655, 666, 316}, new double[] {442, 447, 51, 44}, false, false, null);
        assertEquals(data1.getEventCount(), 13013 - 7444);
        data1.inversePlotGates(6, 6, new double[] {7, 30, 30}, new double[] {1, 24, 2}, false, false, null);
        assertEquals(data1.getEventCount(), 13013 - 7444);

        data.clearGates(null);
        data.inversePlotGates(1, 4, new double[] {100, 400, 200}, new double[] {900, 900, 600}, false, false, null);
        int n = data.getEventCount();
        data.inversePlotGates(1, 4, new double[] {100, 400, 200}, new double[] {900, 900, 600}, false, false, null);
        assertEquals(data.getEventCount(), n);
    }

    /**
     * Test of clearGates method, of class FcsData.
     */
    @Test
    public void testClearGates() {
        data1.clearGates(null);
        data1.inversePlotGates(0, 1, new double[] {309, 655, 666, 316}, new double[] {442, 447, 51, 44}, false, false, null);
        assertEquals(data1.getEventCount(), 13013 - 7444);
        data1.clearGates(null);
        assertEquals(data1.getEventCount(), 13013);
    }

    /**
     * Test of getMaxValues method, of class FcsData.
     */
    @Test
    public void testGetMaxValues() {
        data.clearGates(null);
        assertEquals(data.getMaxValues(true)[0], 3.009875633, delta);
        assertEquals(data.getMaxValues(true)[1], 3.009875633, delta);
        assertEquals(data.getMaxValues(true)[2], 3.009875633, delta);
        assertEquals(data.getMaxValues(true)[3], 3.9843597, delta);
        assertEquals(data.getMaxValues(true)[4], 3.006466, delta);
        assertEquals(data.getMaxValues(true)[5], 3.009875633, delta);
        assertEquals(data.getMaxValues(true)[6], 1.63868888669, delta);

        assertEquals(data.getMaxValues(false)[0], 1023, delta);
        assertEquals(data.getMaxValues(false)[1], 1023, delta);
        assertEquals(data.getMaxValues(false)[2], 1023, delta);
        assertEquals(data.getMaxValues(false)[3], 9646.27694, delta);
        assertEquals(data.getMaxValues(false)[4], 1015, delta);
        assertEquals(data.getMaxValues(false)[5], 1023, delta);
        assertEquals(data.getMaxValues(false)[6], 43.52, delta);
    }

    /**
     * Test of getMinValues method, of class FcsData.
     */
    @Test
    public void testGetMinValues() {
        data.clearGates(null);

        assertEquals(data.getMinValues(false)[0], 66, delta);
        assertEquals(data.getMinValues(false)[1], 3, delta);
        assertEquals(data.getMinValues(false)[2], 0, delta);
        assertEquals(data.getMinValues(false)[3], 1, delta);
        assertEquals(data.getMinValues(false)[4], 0, delta);
        assertEquals(data.getMinValues(false)[5], 0, delta);
        assertEquals(data.getMinValues(false)[6], 0, delta);
    }

    /**
     * Test of getHistogramScale method, of class FcsData.
     */
    @Test
    public void testGetHistogramScale() {
        assertEquals(data.getHistogramScale(), 75);
        assertEquals(data1.getHistogramScale(), 75);
    }

    /**
     * Test of getHistogramData method, of class FcsData.
     */
    @Test
    public void testGetHistogramData() {
        data.clearGates(null);

        Random generator = new Random();

        for (int j = 0; j < data.getParameterCount(); ++j) {
            int count = 0;
            for (int i = 0; i < data.getHistogramData(j, false).size(); ++i)
                count += data.getHistogramData(j, false).get(i);
            // assertEquals(count, 45375);
            assertEquals(count, 58388);

            count = 0;
            for (int i = 0; i < data.getHistogramData(j, true).size(); ++i)
                count += data.getHistogramData(j, true).get(i);
            // assertEquals(count, 45375);
            assertEquals(count, 58388);

            for (int rep = 0; rep < repeatCount; ++rep) {
                count = 0;

                int k = generator.nextInt(data.getHistogramScale());
                HistogramList hd = data.getHistogramData(j, false);

                if (k < data.getHistogramScale() - 1) {
                    for (int i = 0; i < data.getEventCount(); ++i)
                        if (data.getEvent(i)[j] >= hd.getMinX() + (double) k / data.getHistogramScale() * (hd.getMaxX() - hd.getMinX()) &&
                            data.getEvent(i)[j] < hd.getMinX() + (double) (k+1) / data.getHistogramScale() * (hd.getMaxX() - hd.getMinX()))
                                ++count;
                } else {
                    for (int i = 0; i < data.getEventCount(); ++i)
                        if (data.getEvent(i)[j] >= hd.getMinX() + (double) k / data.getHistogramScale() * (hd.getMaxX() - hd.getMinX()) &&
                            data.getEvent(i)[j] <= hd.getMinX() + (double) (k+1) / data.getHistogramScale() * (hd.getMaxX() - hd.getMinX()))
                                ++count;
                }

                if (count != (int) hd.get(k))
                    System.out.println("testGetHistogramData fail: " + k);
                assertEquals(count, (int) hd.get(k));
            }
        }
    }

    /**
     * Test of getPlotScale method, of class FcsData.
     */
    @Test
    public void testGetPlotScale() {
        assertEquals(data.getPlotScale(), 100);
        assertEquals(data1.getPlotScale(), 100);
    }

    /**
     * Test of getPlotData method, of class FcsData.
     */
    @Test
    public void testGetPlotData() {
        data.clearGates(null);

        Random generator = new Random();

        for (int p1 = 0; p1 < data.getParameterCount(); ++p1) {
            for (int p2 = 0; p2 < data.getParameterCount(); ++p2) {
                int count = 0;
                for (int i = 0; i < data.getPlotScale(); ++i)
                    for (int j = 0; j < data.getPlotScale(); ++j)
                        count += data.getPlotData(p1, p2, true, true).get(i, j);
                assertEquals(count, 45375);

                for (int rep = 0; rep < repeatCount; ++rep) {
                    count = 0;

                    int x = generator.nextInt(data.getPlotScale());
                    int y = generator.nextInt(data.getPlotScale());

                    PlotList pl = data.getPlotData(p1, p2, false, false);

                    if (x != data.getPlotScale() - 1 && y != data.getPlotScale() - 1) {
                        for (int i = 0; i < data.getEventCount(); ++i)
                            if (data.getEvent(i)[p1] >= data.getMinValues(false)[p1] + (double) x / data.getPlotScale() * (data.getMaxValues(false)[p1] - data.getMinValues(false)[p1]) &&
                                data.getEvent(i)[p1] < data.getMinValues(false)[p1] + (double) (x + 1) / data.getPlotScale() * (data.getMaxValues(false)[p1] - data.getMinValues(false)[p1]) &&
                                data.getEvent(i)[p2] >= data.getMinValues(false)[p2] + (double) y / data.getPlotScale() * (data.getMaxValues(false)[p2] - data.getMinValues(false)[p2]) &&
                                data.getEvent(i)[p2] < data.getMinValues(false)[p2] + (double) (y + 1) / data.getPlotScale() * (data.getMaxValues(false)[p2] - data.getMinValues(false)[p2]))
                                    ++count;
                    } else if (x == data.getPlotScale() - 1 && y != data.getPlotScale() - 1) {
                        for (int i = 0; i < data.getEventCount(); ++i)
                            if (data.getEvent(i)[p1] >= data.getMinValues(false)[p1] + (double) x / data.getPlotScale() * (data.getMaxValues(false)[p1] - data.getMinValues(false)[p1]) &&
                                data.getEvent(i)[p1] <= data.getMinValues(false)[p1] + (double) (x + 1) / data.getPlotScale() * (data.getMaxValues(false)[p1] - data.getMinValues(false)[p1]) &&
                                data.getEvent(i)[p2] >= data.getMinValues(false)[p2] + (double) y / data.getPlotScale() * (data.getMaxValues(false)[p2] - data.getMinValues(false)[p2]) &&
                                data.getEvent(i)[p2] < data.getMinValues(false)[p2] + (double) (y + 1) / data.getPlotScale() * (data.getMaxValues(false)[p2] - data.getMinValues(false)[p2]))
                                    ++count;
                    } else if (x != data.getPlotScale() - 1 && y == data.getPlotScale() - 1) {
                        for (int i = 0; i < data.getEventCount(); ++i)
                            if (data.getEvent(i)[p1] >= data.getMinValues(false)[p1] + (double) x / data.getPlotScale() * (data.getMaxValues(false)[p1] - data.getMinValues(false)[p1]) &&
                                data.getEvent(i)[p1] < data.getMinValues(false)[p1] + (double) (x + 1) / data.getPlotScale() * (data.getMaxValues(false)[p1] - data.getMinValues(false)[p1]) &&
                                data.getEvent(i)[p2] >= data.getMinValues(false)[p2] + (double) y / data.getPlotScale() * (data.getMaxValues(false)[p2] - data.getMinValues(false)[p2]) &&
                                data.getEvent(i)[p2] <= data.getMinValues(false)[p2] + (double) (y + 1) / data.getPlotScale() * (data.getMaxValues(false)[p2] - data.getMinValues(false)[p2]))
                                    ++count;
                    } else {
                        for (int i = 0; i < data.getEventCount(); ++i)
                            if (data.getEvent(i)[p1] >= data.getMinValues(false)[p1] + (double) x / data.getPlotScale() * (data.getMaxValues(false)[p1] - data.getMinValues(false)[p1]) &&
                                data.getEvent(i)[p1] <= data.getMinValues(false)[p1] + (double) (x + 1) / data.getPlotScale() * (data.getMaxValues(false)[p1] - data.getMinValues(false)[p1]) &&
                                data.getEvent(i)[p2] >= data.getMinValues(false)[p2] + (double) y / data.getPlotScale() * (data.getMaxValues(false)[p2] - data.getMinValues(false)[p2]) &&
                                data.getEvent(i)[p2] <= data.getMinValues(false)[p2] + (double) (y + 1) / data.getPlotScale() * (data.getMaxValues(false)[p2] - data.getMinValues(false)[p2]))
                                    ++count;                        
                    }

                    if (count != (int) pl.get(x, y))
                        System.out.println("testGetPlotData fail: " + x + " " + y);
                    assertEquals(count, (int) pl.get(x, y));
                }
            }
        }
    }

    /**
     * Test of getTimeParamNumber method, of class FcsData.
     */
    @Test
    public void testGetTimeParamNumber() {
        assertEquals(data.getTimeParamNumber(), 6);
        assertEquals(data1.getTimeParamNumber(), 6);
        
        try {
            FcsData fcs = new FcsData(new File("examples/fcs3_all_lin.fcs"), null);
            assertEquals(fcs.getTimeParamNumber(), 10);

            fcs = new FcsData(new File("examples/macsquant.fcs"), null);
            assertEquals(fcs.getTimeParamNumber(), 0);
        } catch (IOException ex) {
            fail("Could not read FCS file.");
        }
        
    }

    /**
     * Test of isTimeInSeconds method, of class FcsData.
     */
    @Test
    public void testIsTimeInSeconds() {
        assertEquals(data.isTimeInSeconds(), true);
        assertEquals(data1.isTimeInSeconds(), true);
    }

    /*
    @Test
    public void testGetTimeDependenceRatio() {
        assertEquals(8.25, data.getTimeDependenceRatio(6,5, false, false, 10), 0.001);
        assertEquals(9.24, data.getTimeDependenceRatio(6,1, false, false, 10), 0.001);
        assertEquals(9.24, data.getTimeDependenceRatio(6,1, false, true, 10), 0.001);
    }
    */
    
    //@author Daniel Herman
    /**
     * Test of timeUniformization method, of class FcsData.
     */
    @Test
    public void testTimeUniformization() {
        data.timeUniformization(null);
        assertEquals(0.000959, (data.getEvent(1)[6] - data.getEvent(0)[6]), 0.00001);
    }
    //--
    
    /**
     * Test of logSuggested method, of class FcsData.
     */
    @Test
    public void testLogSuggested() {
        assertEquals(data.logSuggested()[0], false);
        assertEquals(data.logSuggested()[1], false);
        assertEquals(data.logSuggested()[2], false);
        assertEquals(data.logSuggested()[3], true);
        assertEquals(data.logSuggested()[4], false);
        assertEquals(data.logSuggested()[5], false);
        assertEquals(data.logSuggested()[6], false);
    }

    /**
     * Test of append method, of class FcsData.
     */
    @Test
    public void testAppend() throws Exception {
        data.clearGates(null);
        data1.clearGates(null);

        data.append(data1);

        assertEquals(data.getEventCount(), 45375 + 13013);
    }

}
