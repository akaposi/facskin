package facskin;

public class Arrays {

    public static boolean eq(Double[] a, Double[] b) {
        boolean ret = true;

        if (a.length != b.length)
            ret = false;
        for (int i = 0; i < a.length; ++i)
            if (Math.abs((a[i] - b[i])/a[i]) > 0.0001)
                ret = false;

        if (!ret) {
            for (int i = 0; i < a.length; ++i)
                System.out.print(a[i] + " ");
            System.out.println();
            for (int i = 0; i < b.length; ++i)
                System.out.print(b[i] + " ");
            System.out.println();
        }

        return ret;
    }

    public static boolean eq(double a, double b) {
        if (Math.abs((a - b)/a) > 0.0001)
            return false;
        return true;
    }

    public static boolean eq(double[] a, double[] b) {
        boolean ret = true;

        if (a.length != b.length)
            ret = false;
        for (int i = 0; i < a.length; ++i)
            if (Math.abs((a[i] - b[i])/a[i]) > 0.0001)
                ret = false;

        if (!ret) {
            for (int i = 0; i < a.length; ++i)
                System.out.print(a[i] + " ");
            System.out.println();
            for (int i = 0; i < b.length; ++i)
                System.out.print(b[i] + " ");
            System.out.println();
        }

        return ret;
    }

    public static boolean eq(int[] a, int[] b) {
        boolean ret = true;

        if (a.length != b.length)
            ret = false;
        for (int i = 0; i < a.length; ++i)
            if (a[i] != b[i])
                ret = false;

        if (!ret) {
            for (int i = 0; i < a.length; ++i)
                System.out.print(a[i] + " ");
            System.out.println();
            for (int i = 0; i < b.length; ++i)
                System.out.print(b[i] + " ");
            System.out.println();
        }

        return ret;
    }

}
