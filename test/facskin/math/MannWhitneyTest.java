/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package facskin.math;

import facskin.Arrays;
import java.util.ArrayList;
import java.util.Random;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ambi
 */
public class MannWhitneyTest {

    public MannWhitneyTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    private static List<List<Double>> randomData(Random generator) {
        int n = 2;
        int[] ns = new int[n];
        for (int i = 0; i < n; ++i)
            ns[i] = generator.nextInt(30)+1;

        List<List<Double>> l = new ArrayList<List<Double>>();
        for (int i = 0; i < n; ++i) {
            List<Double> l1 = new ArrayList<Double>();
            for (int j = 0; j < ns[i]; ++j)
                l1.add((double) generator.nextInt(20));
            l.add(l1);
        }

        return l;
    }

    /**
     * Test of test method, of class MannWhitney.
     */
    @Test
    public void testTest() {
        Random rand = new Random(682473243);

        for (int i = 0; i < 10; ++i) {
            List<List<Double>> xss = randomData(rand);
            assertEquals(KruskalWallis.test(xss)[1], MannWhitney.test(xss.get(0), xss.get(1))[1], 0.01);
        }
    }

}