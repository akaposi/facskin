/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package facskin.math;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.File;
import java.util.Random;
import facskin.Arrays;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ambi
 */
public class KruskalWallisTest {

    public KruskalWallisTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    private static List<List<Double>> randomData(Random generator) {
        int n = generator.nextInt(5)+2;
        int[] ns = new int[n];
        for (int i = 0; i < n; ++i)
            ns[i] = generator.nextInt(30)+1;

        List<List<Double>> l = new ArrayList<List<Double>>();
        for (int i = 0; i < n; ++i) {
            List<Double> l1 = new ArrayList<Double>();
            for (int j = 0; j < ns[i]; ++j)
                l1.add((double) generator.nextInt(20));
            l.add(l1);
        }

        return l;
    }

    // I generate R code that makes the comparison between my test and
    // R's own one
    @Test
    public void longTest() {

        File f = new File("examples/kruskal_test.R");
        File f_script = new File("examples/kruskal_test.sh");
        try {
            FileWriter fw = new FileWriter(f);

            // 1. R

            Random generator = new Random(682473243);
            int n = 100;
            fw.write("rm(list=ls())\n");
            fw.write("results<-array(NA, dim=c("+n+", 4))\n");
            for (int j = 0; j < n; ++j) {
                List<List<Double>> l = randomData(generator);

                fw.write("x<-c(");
                for (int i = 0; i < l.size(); ++i) {
                    for (int k = 0; k < l.get(i).size()-1; ++k)
                        fw.write(l.get(i).get(k)+", ");
                    fw.write("" + (l.get(i).get(l.get(i).size()-1)));
                    if (i==l.size()-1) fw.write(")\n");
                    else               fw.write(", ");
                }
                fw.write("g<-c(");
                for (int i = 0; i < l.size(); ++i) {
                    for (int k = 0; k < l.get(i).size()-1; ++k)
                        fw.write((i+1)+", ");
                    fw.write("" + (i+1));
                    if (i==l.size()-1) fw.write(")\n");
                    else               fw.write(", ");
                }
                fw.write("k<-kruskal.test(x, g)\n");
                fw.write("results["+(j+1)+", 1:2]<-c(k$statistic, k$p.value)\n");
            }

            // 2. Java

            generator = new Random(682473243);
            fw.write("j.results<-array(NA, dim=c("+n+", 4))\n");
            for (int j = 0; j < n; ++j) {
                List<List<Double>> l = randomData(generator);
                double[] kwr = KruskalWallis.test(l);
                fw.write("results["+(j+1)+", 3:4]<-c("+kwr[0]+", "+kwr[1]+")\n");
            }

            // 3. Comparison

            fw.write("if (length(which(abs(results[, 2]-results[, 4]) > 0.000001))+length(which(abs(results[, 1]-results[, 3]) > 0.000001)) > 0) stop(\"The results differ\")");
            fw.close();

            // Execute script
            fw = new FileWriter(f_script);
            fw.write("#!/bin/sh\n");
            fw.write("R --no-save --no-restore <examples/kruskal_test.R\n");
            fw.close();

            Process pr0 = Runtime.getRuntime().exec("chmod +x examples/kruskal_test.sh");
            pr0.waitFor();

            Process pr = Runtime.getRuntime().exec("examples/kruskal_test.sh");
            BufferedReader in = new BufferedReader(
                                new InputStreamReader(pr.getInputStream()));
            String line = null;
            while ((line = in.readLine()) != null) {
                // System.out.println(line);
            }
            int exitVal = pr.waitFor();

            if (exitVal != 0) {
                f.delete();
                f_script.delete();
                fail("Results differ.");
            }
            f.delete();
            f_script.delete();
        } catch (IOException ex) {
            f.delete();
            f_script.delete();
            fail("IO Exception.");
        } catch (InterruptedException ex) {
            f.delete();
            f_script.delete();
            fail("R execution problem.");
        }
    }

    /**
     * Test of test method, of class KruskalWallis.
     */
    @Test
    public void testTest() {
        List<List<Double>> xss;
        List<Double> xs;

        xss = new ArrayList<List<Double>>();
        xs = new ArrayList<Double>(); for (Double d : new double[] {1,1}) xs.add(d); xss.add(xs);
        xs = new ArrayList<Double>(); for (Double d : new double[] {1,2}) xs.add(d); xss.add(xs);
        xs = new ArrayList<Double>(); for (Double d : new double[] {2,4,6}) xs.add(d); xss.add(xs);
        assertTrue(Arrays.eq(KruskalWallis.test(xss), new double[] {4.519608, 0.1043709}));

        xss = new ArrayList<List<Double>>();
        xs = new ArrayList<Double>(); for (Double d : new double[] {1,1,1,1,1,1}) xs.add(d); xss.add(xs);
        xs = new ArrayList<Double>(); for (Double d : new double[] {1,1,2,2,2,2}) xs.add(d); xss.add(xs);
        xs = new ArrayList<Double>(); for (Double d : new double[] {2,2,3,3,3,3,3,3,4,5,6,6}) xs.add(d); xss.add(xs);
        xs = new ArrayList<Double>(); for (Double d : new double[] {12,21,43,45}) xs.add(d); xss.add(xs);
        assertTrue(Arrays.eq(KruskalWallis.test(xss), new double[] {23.27294, 3.542389e-05}));
        
        xss = new ArrayList<List<Double>>();
        xs = new ArrayList<Double>(); for (Double d : new double[] {1,2,3,4,5}) xs.add(d); xss.add(xs);
        xs = new ArrayList<Double>(); for (Double d : new double[] {5,6,6,7}) xs.add(d); xss.add(xs);
        xs = new ArrayList<Double>(); for (Double d : new double[] {7,8,9}) xs.add(d); xss.add(xs);
        xs = new ArrayList<Double>(); for (Double d : new double[] {10, 10}) xs.add(d); xss.add(xs);
        assertTrue(Arrays.eq(KruskalWallis.test(xss), new double[] {11.65292, 0.008671806}));

        xss = new ArrayList<List<Double>>();
        xs = new ArrayList<Double>(); for (Double d : new double[] {1,2}) xs.add(d); xss.add(xs);
        xs = new ArrayList<Double>(); for (Double d : new double[] {2}) xs.add(d); xss.add(xs);
        assertTrue(Arrays.eq(KruskalWallis.test(xss), new double[] {0.5, 0.4795001}));
    }
}
