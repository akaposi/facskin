package facskin.math;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ambi
 */
public class StatisticsTest {

    private static double delta = 0.00001;

    public StatisticsTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of overlap method, of class Statistics.
     */
    @Test
    public void testOverlap() {
        double low1, high1, low2,high2;
        
        low1 = 0.0; high1 = 1.0;
        low2 = 3.0; high2 = 4.0;
        assertEquals(0, Statistics.overlap(high1, low1, high2, low2), delta);

        low1 = 6.0; high1 = 7.0;
        low2 = 3.0; high2 = 4.0;
        assertEquals(0, Statistics.overlap(high1, low1, high2, low2), delta);

        low1 = 2.5; high1 = 3.5;
        low2 = 3.0; high2 = 4.0;
        assertEquals(0.5, Statistics.overlap(high1, low1, high2, low2), delta);

        low1 = 3.0; high1 = 4.0;
        low2 = 2.5; high2 = 3.5;
        assertEquals(0.5, Statistics.overlap(high1, low1, high2, low2), delta);

        low1 = 3.0; high1 = 5.0;
        low2 = 2.5; high2 = 3.5;
        assertEquals(0.25, Statistics.overlap(high1, low1, high2, low2), delta);
    }

    /**
     * Test of median method, of class Statistics.
     */
    @Test
    public void testMedian() {
        List<Double> l;

        l = new ArrayList<Double>(); for (Double d : new double[] {1,1}) l.add(d);
        assertEquals(Statistics.median(l), 1, delta);

        l = new ArrayList<Double>(); for (Double d : new double[] {0,1}) l.add(d);
        assertEquals(Statistics.median(l), 0.5, delta);

        l = new ArrayList<Double>(); for (int i = 0; i < 100; i ++) l.add((double) i);
        assertEquals(Statistics.median(l), 49.5, delta);

        l = new ArrayList<Double>(); for (Double d : new double[] {0.1,0.3,1,2,12}) l.add(d);
        assertEquals(Statistics.median(l), 1, delta);
    }

    /**
     * Test of quantile method, of class Statistics.
     */
    @Test
    public void testQuantile() {
        List<Double> l;

        l = new ArrayList<Double>(); for (Double d : new double[] {1,1}) l.add(d);
        assertEquals(Statistics.quantile(l, 0), 1, delta);
        assertEquals(Statistics.quantile(l, 1), 1, delta);
        assertEquals(Statistics.quantile(l, 0.3), 1, delta);

        l = new ArrayList<Double>(); for (Double d : new double[] {0,1}) l.add(d);
        assertEquals(Statistics.quantile(l, 0), 0, delta);
        assertEquals(Statistics.quantile(l, 1), 1, delta);
        assertEquals(Statistics.quantile(l, 0.3), 0.3, delta);
        assertEquals(Statistics.quantile(l, 0.7645), 0.7645, delta);
        
        l = new ArrayList<Double>(); for (int i = 0; i < 100; i ++) l.add((double) i);
        assertEquals(Statistics.quantile(l, 0), 0, delta);
        assertEquals(Statistics.quantile(l, 1), 99, delta);
        assertEquals(Statistics.quantile(l, 0.3), 29.7, delta);
        assertEquals(Statistics.quantile(l, 0.87), 86.13, delta);
        assertEquals(Statistics.quantile(l, 0.5), 49.5, delta);

        l = new ArrayList<Double>(); for (Double d : new double[] {0.1,0.3,1,2,12}) l.add(d);
        assertEquals(Statistics.quantile(l, 0), 0.1, delta);
        assertEquals(Statistics.quantile(l, 1), 12, delta);
        assertEquals(Statistics.quantile(l, 0.3), 0.44, delta);
        assertEquals(Statistics.quantile(l, 0.87), 6.8, delta);
        assertEquals(Statistics.quantile(l, 0.5), 1, delta);
    }

}