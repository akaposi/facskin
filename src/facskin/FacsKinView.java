package facskin;

import facskin.fcs.FcsPanel;
import facskin.fcs.GatePanel;
import facskin.fcs.GateCheckBox;
import facskin.formats.FcsData;
import facskin.formats.GateData;
import facskin.formats.KineticsData;
import facskin.kinetics.KineticsPanel;
import java.awt.Color;
import java.awt.Component;
import java.io.IOException;
import org.jdesktop.application.Action;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskMonitor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Timer;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * The application's main frame.
 */
public class FacsKinView extends FrameView {

    private final Timer messageTimer;
    private final Timer busyIconTimer;
    private final Icon idleIcon;
    private final Icon[] busyIcons = new Icon[15];
    private int busyIconIndex = 0;

    private JDialog aboutBox;
    private JFileChooser fileChooser;

    private KineticsPanel kineticsPanel = null;
    
    private String fileName = null;
    //--
    
    /**
     * Initializes user interface, creates a task monitor for
     * the progress bar and a listener for the tab pane so that it gets
     * tab open and close events.
     * @param app The application.
     */
    public FacsKinView(SingleFrameApplication app) {
        super(app);

        initComponents();

        fileChooser = new JFileChooser();
        //this.getFrame().setResizable(false);

        // status bar initialization - message timeout, idle icon and busy animation, etc
        ResourceMap resourceMap = getResourceMap();
        int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
        messageTimer = new Timer(messageTimeout, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                statusMessageLabel.setText("");
            }
        });
        messageTimer.setRepeats(false);
        int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
        for (int i = 0; i < busyIcons.length; i++) {
            busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
        }
        busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
                statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
            }
        });
        idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
        statusAnimationLabel.setIcon(idleIcon);
        progressBar.setVisible(false);

        // connecting action tasks to status bar via TaskMonitor
        TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
        taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            @Override
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                String propertyName = evt.getPropertyName();
                if ("started".equals(propertyName)) {
                    if (!busyIconTimer.isRunning()) {
                        statusAnimationLabel.setIcon(busyIcons[0]);
                        busyIconIndex = 0;
                        busyIconTimer.start();
                    }
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(true);
                } else if ("done".equals(propertyName)) {
                    busyIconTimer.stop();
                    statusAnimationLabel.setIcon(idleIcon);
                    progressBar.setVisible(false);
                    progressBar.setValue(0);
                } else if ("message".equals(propertyName)) {
                    String text = (String)(evt.getNewValue());
                    statusMessageLabel.setText((text == null) ? "" : text);
                    messageTimer.restart();
                } else if ("progress".equals(propertyName)) {
                    int value = (Integer)(evt.getNewValue());
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(false);
                    progressBar.setValue(value);
                }
            }
        });

        if (FacsKinApp.getDecimalSeparator() == '.') {
            jMenuItem2.setText("Use , as decimal separator");
        } else {
            jMenuItem2.setText("Use . as decimal separator");
        }
        if (FacsKinApp.getStandardisation()) {
            jCheckBoxMenuItem1.setState(true);
        } else {
            jCheckBoxMenuItem1.setState(false);
        }

        tabbedPane.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent evt) {
                disableSaveAs();
                disableAppendFCS();
                disableSaveGates();
                disableLoadGates();
                if (tabbedPane.getTabCount() > 0) {
                    Component tab = tabbedPane.getComponentAt(tabbedPane.getSelectedIndex());
                    if (tab.getClass().getSimpleName().equals("KineticsPanel")) {
                        if (kineticsPanel != null && kineticsPanel.getDatas().size() > 0)
                            enableSaveAs();
                    } else if (tab.getClass().getSimpleName().equals("FcsPanel")) {
                        enableSaveAs();
                        enableAppendFCS();
                        enableSaveGates();
                        enableLoadGates();
                    }
                }
            }
        });
    }

    //----------------------------------------------
    // File menu actions
    //----------------------------------------------

    /**
     * Method called when the corresponding menu item is clicked.
     * @return Returns null if the action is canceled, the task otherwise.
     */
    @Action
    public Task FileOpen() {
        fileChooser.setMultiSelectionEnabled(true);
        int option = fileChooser.showOpenDialog(getFrame());
        Task task = null;
        if (JFileChooser.APPROVE_OPTION == option)
            task = new OpenTask(getApplication(), fileChooser.getSelectedFiles());
        fileChooser.setMultiSelectionEnabled(false);
        return task;
    }

    /**
     * Method called when the corresponding menu item is clicked.
     * @return Returns null if the action is canceled, the task otherwise.
     */
    @Action
    public Task FileSaveAs() {
        if (tabbedPane.getComponentAt(tabbedPane.getSelectedIndex()).getClass().getSimpleName().equals("KineticsPanel")) {
            if (fileChooser.getSelectedFile() == null || !fileChooser.getSelectedFile().getName().toLowerCase().endsWith(".zip"))
                fileChooser.setSelectedFile(new File("kinetics.zip"));
        } else { // we want to save an FCS
            if (fileChooser.getSelectedFile() == null) {
                if (tabbedPane.getComponentAt(tabbedPane.getSelectedIndex()).getClass().getSimpleName().equals("FcsPanel")) {
                    FcsPanel panel = (FcsPanel) tabbedPane.getComponentAt(tabbedPane.getSelectedIndex());
                    fileChooser.setSelectedFile(new File(panel.getData().getFilename() + "_1.fcs"));
                } else
                    fileChooser.setSelectedFile(new File("measurement.fcs"));
            }
        }
        int option = fileChooser.showSaveDialog(getFrame());
        Task task = null;
        if (JFileChooser.APPROVE_OPTION == option)
            task = new SaveAsClickedTask(getApplication(), fileChooser.getSelectedFile());
        return task;
    }

    /**
     * Method called when the corresponding menu item is clicked.
     * @return Returns null if the action is canceled, the task otherwise.
     */
    @Action
    public Task FileAppendFCS() {
        String response = null;
        String text = "Size of gap included before appended FCS data (seconds):";

        response = JOptionPane.showInputDialog(FacsKinView.this.getFrame(), text, FacsKinApp.getGapBeforeAppend());
        if (response != null) {
            try {
                double newGapBeforeAppend = Double.valueOf(response);
                if (newGapBeforeAppend < 0) {
                    text = "Bad value. Please enter new value for the size of gap included before appended FCS data (seconds):";
                    response = null;
                } else
                    FacsKinApp.setGapBeforeAppend(newGapBeforeAppend);
            } catch (NumberFormatException ex) {
                text = "Bad value. Please enter new value for the size of gap included before appended FCS data (seconds):";
                response = null;
            }
        }

        if (response != null) {
            int option = fileChooser.showOpenDialog(getFrame());
            Task task = null;
            if (JFileChooser.APPROVE_OPTION == option)
                task = new FileAppendFCSTask(getApplication(), fileChooser.getSelectedFile());
            return task;
        } else
            return null;
    }

    //----------------------------------------------
    // Actions related to file menu actions
    //----------------------------------------------

    @Action
    public Task OpenFile() {
        Task task = new OpenTask(getApplication(), new File[] { fileChooser.getSelectedFile() });
        return task;
    }

    //----------------------------------------------
    // Preferences menu actions
    //----------------------------------------------

    /**
     * Method called when the corresponding menu item is clicked.
     */
    @Action
    public void PreferencesToggleDecimalSeparator() {
        if (FacsKinApp.getDecimalSeparator() == '.') {
            FacsKinApp.setDecimalSeparator(',');
            jMenuItem2.setText("Use . as decimal separator");
        } else {
            FacsKinApp.setDecimalSeparator('.');
            jMenuItem2.setText("Use , as decimal separator");
        }
        if (kineticsPanel != null)
            kineticsPanel.redrawTableAndGraph();
    }

    /**
     * Method called when the corresponding menu item is clicked.
     */
    @Action
    public void PreferencesToggleStandardisation() {
        FacsKinApp.setStandardisation(!FacsKinApp.getStandardisation());
        jCheckBoxMenuItem1.setState(FacsKinApp.getStandardisation());
        if (kineticsPanel != null) {
            for (int i = 0; i < ((KineticsPanel) kineticsPanel).getDatas().size(); i ++)
                kineticsPanel.getDatas().get(i).setStandardisation(FacsKinApp.getStandardisation());
            kineticsPanel.redrawTableAndGraph();
            kineticsPanel.ensureShowingKineticsTable();
        }
    }

    /**
     * Method called when the corresponding menu item is clicked.
     */
    @Action
    public void toggleDrawFunctionUntilMeasurementEnds() {
        if (FacsKinApp.getDrawFunctionUntilMeasurementEnd()) {
            FacsKinApp.setDrawFunctionUntilMeasurementEnd(false);
        } else {
            FacsKinApp.setDrawFunctionUntilMeasurementEnd(true);
        }
        jCheckBoxMenuItem2.setState(FacsKinApp.getDrawFunctionUntilMeasurementEnd());
        if (kineticsPanel != null)
            kineticsPanel.redrawTableAndGraph();
    }

    /**
     * Method called when the corresponding menu item is clicked.
     */
    @Action
    public void PreferencesSetMaxTimeForAUC() {
        String response = JOptionPane.showInputDialog(FacsKinView.this.getFrame(), "Set Maximum Time for AUC calculation (seconds):", FacsKinApp.getMaxTime());
        if (response != null) {
            try {
                double newMaxTime = Double.valueOf(response);
                if (newMaxTime <= 0) {
                    JOptionPane.showMessageDialog(FacsKinView.this.getFrame(), "Bad value. Old value (" + FacsKinApp.getMaxTime() + " seconds) remains.", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    if (kineticsPanel != null)
                        kineticsPanel.setMaxTime(newMaxTime);
                }
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(FacsKinView.this.getFrame(), "Bad value. Old value (" + FacsKinApp.getMaxTime() + " seconds) remains.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    @Action
    public void PreferencesSetBaselineLength() {
        String response = JOptionPane.showInputDialog(FacsKinView.this.getFrame(), "Set Baseline Length (seconds):", FacsKinApp.getBaselineTime());
        if (response != null) {
            try {
                double newBaselineLength = Double.valueOf(response);
                if (newBaselineLength <= 0) {
                    JOptionPane.showMessageDialog(FacsKinView.this.getFrame(), "Bad value. Old value (" + FacsKinApp.getBaselineTime() + " seconds) remains.", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    if (kineticsPanel != null)
                        kineticsPanel.setBaselineTime(newBaselineLength);
                }
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(FacsKinView.this.getFrame(), "Bad value. Old value (" + FacsKinApp.getBaselineTime() + " seconds) remains.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Method called when the corresponding menu item is clicked.
     */
    @Action
    public void PreferencesSetNumberofBreaksinHistogramPlot() {
        String response = JOptionPane.showInputDialog(FacsKinView.this.getFrame(), "Set Number of Breaks in Histogram Plot (should be between 1 and 100):", FacsKinApp.getNumberofBreaksinHistogramPlot());
        if (response != null) {
            try {
                int newValue = Integer.valueOf(response);
                if (newValue < 1 || newValue > 100)
                    JOptionPane.showMessageDialog(FacsKinView.this.getFrame(), "Bad value. Old value (" + FacsKinApp.getNumberofBreaksinHistogramPlot() + ") remains.", "Error", JOptionPane.ERROR_MESSAGE);
                else
                    FacsKinApp.setNumberofBreaksinHistogramPlot(newValue);
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(FacsKinView.this.getFrame(), "Bad value. Old value (" + FacsKinApp.getNumberofBreaksinHistogramPlot() + ") remains.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }        
    }

    @Action
    public void PreferencesSetNumberofBreaksinKineticsPlot() {
        String response = JOptionPane.showInputDialog(FacsKinView.this.getFrame(), "Set Number of Breaks in Kinetics Plot (should be between 50 and 100000):", FacsKinApp.getNumberofBreaksinKineticsPlot());
        if (response != null) {
            try {
                int newValue = Integer.valueOf(response);
                if (newValue < 50 || newValue > 100000)
                    JOptionPane.showMessageDialog(FacsKinView.this.getFrame(), "Bad value. Old value (" + FacsKinApp.getNumberofBreaksinKineticsPlot() + ") remains.", "Error", JOptionPane.ERROR_MESSAGE);
                else
                    FacsKinApp.setNumberofBreaksinKineticsPlot(newValue);
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(FacsKinView.this.getFrame(), "Bad value. Old value (" + FacsKinApp.getNumberofBreaksinKineticsPlot() + ") remains.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }        
    }
    
    @Action
    public void toggleColourByGroups() {
        if (FacsKinApp.getColorByGroups()) {
            FacsKinApp.setColorByGroups(false);
        } else {
            FacsKinApp.setColorByGroups(true);
        }
        jCheckBoxMenuItem3.setState(FacsKinApp.getColorByGroups());
        if (kineticsPanel != null)
            kineticsPanel.redrawTableAndGraph();
    }

    //----------------------------------------------
    // Help menu actions
    //----------------------------------------------

    /**
     * Method called when the corresponding menu item is clicked.
     */
    @Action
    public void HelpUsersGuide() {
        JTextPane tp = new JTextPane();
        tp.setEditable(false);
        JScrollPane js = new JScrollPane();
        js.getViewport().add(tp);
        JFrame jf = new JFrame("FacsKin User's Guide");
        jf.getContentPane().add(js);
        jf.pack();
        jf.setSize(600,650);
        jf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        jf.setVisible(true);
        try {
            URL url = getClass().getResource("/facskin/resources/usersguide.html");
            tp.setPage(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method called when the corresponding menu item is clicked.
     */
    @Action
    public void HelpAbout() {
        if (aboutBox == null) {
            JFrame mainFrame = FacsKinApp.getApplication().getMainFrame();
            aboutBox = new FacsKinAboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        FacsKinApp.getApplication().show(aboutBox);
    }

    //----------------------------------------------
    // Public helper functions
    //----------------------------------------------

    /**
     * Disable File/Save As menu item (when the kinetics tab is not opened
     * or there are no kinetics files).
     */
    public void disableSaveAs() {
        jMenuItem3.setEnabled(false);
    }

    /**
     * This is a callback function for ButtonTabComponent and is called when
     * removing a tab. We only provide FacsKinView.this for KineticsPanel tabs'
     * ButtonTabComponent so this will only be called when removing a KineticsPanel.
     */
    public void tabRemoved() {
        kineticsPanel = null;
    }

    /**
     * Show the Open panel (used when launching application without parameters).
     */
    public void showFileOpenPanel() {
        jMenuItem1.doClick();
    }

    /**
     * Returns the Open/Save panel. This is used by every UI element that
     * needs to save something to file.
     * @return The Open/Save panel
     */
    public JFileChooser getFileChooser() {
        return fileChooser;
    }

    /**
     * Open a file using file path (used by {@link FacsKinApp} when launched with
     * command line arguments).
     * @param filename file path
     */
    public void openFile(String filename) {
        fileChooser.setSelectedFile(new File(filename));
        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(facskin.FacsKinApp.class).getContext().getActionMap(FacsKinView.class, this);
        actionMap.get("OpenFile").actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "OpenFile"));
    }

    //----------------------------------------------
    // Private helper functions
    //----------------------------------------------

    /**
     * Enable File/Save As menu item (when the kinetics tab is opened
     * and it contains at least one kinetics file).
     */
    private void enableSaveAs() {
        jMenuItem3.setEnabled(true);
    }

    /**
     * Disable File/Append FCS menu item (when an FCS tab is not opened)
     */
    private void disableAppendFCS() {
        jMenuItem5.setEnabled(false);
    }

    /**
     * Enable File/Append FCS menu item (when an FCS tab is opened)
     */
    private void enableAppendFCS() {
        jMenuItem5.setEnabled(true);
    }

    /**
     * @return The contact email address of FacsKin as given in the About Box.
     */
    private String getFacsKinEmail() {
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(facskin.FacsKinApp.class).getContext().getResourceMap(FacsKinAboutBox.class);
        return resourceMap.getString("emailLabel.text");
   }

    /**
     * Custom error messages if we can't find the time parameter in an FCS file.
     * @param fcsData
     * @return Returns true if we found the time parameter, otherwise false.
     */
    private boolean fcsHasTime(FcsData fcsData) {
        if (fcsData.getTimeParamNumber() == -1) {
            JOptionPane.showMessageDialog(this.getComponent(),
                    "We could not determine which is the time parameter.\n"
                    + "Please use FCS 3.0 format or an FCS 2.0 format with a\n"
                    + "parameter Time and keyword TIMETICKS.\n\n"
                    + "If you are sure that your FCS file contains this data,\n"
                    + "please contact us at " + getFacsKinEmail() + ".");
            return false;
        } 
        
// don't use this because a.lmd has a time parameter that is in seconds
// even if it is not specified in a timeticks or similar parameter
//
//        else if (!fcsData.isTimeInSeconds()) {
//            JOptionPane.showMessageDialog(this.getComponent(),
//                    "We could not convert the time parameter values into seconds.\n"
//                    + "Please use FCS 3.0 format or an FCS 2.0 format with a\n"
//                    + "parameter Time and keyword TIMETICKS.\n\n"
//                    + "If you are sure that your FCS file contains this data,\n"
//                    + "please contact us at " + getFacsKinEmail() + ".");
//            return false;
//        }
        return true;
    }

    //----------------------------------------------
    // Classes for long tasks
    //----------------------------------------------

    private class OpenTask extends TaskWithProgress {
        OpenTask(org.jdesktop.application.Application app, File[] files) {
            super(app);
            this.files = files;
        }
        @Override protected Object doInBackground() {
            JComponent panel = null;
            for (File file : files) {
                if (file.getName().toLowerCase().endsWith(".kinetics")) {
                    try {
                        FileInputStream fis = new FileInputStream(file);
                        KineticsData kineticsData = new KineticsData(fis, file.getName(), FacsKinApp.getMaxTime(), this);
                        fis.close();
                        if (kineticsPanel == null) {
                            kineticsPanel = new KineticsPanel(FacsKinView.this);
                            kineticsPanel.addData(kineticsData);
                            tabbedPane.addTab("", kineticsPanel);
                            tabbedPane.setSelectedIndex(tabbedPane.getTabCount() - 1);
                            tabbedPane.setTabComponentAt(tabbedPane.getSelectedIndex(), new ButtonTabComponent("Kinetics", tabbedPane, FacsKinView.this, Color.RED));
                        } else {
                            kineticsPanel.addData(kineticsData);
                            tabbedPane.setSelectedComponent(kineticsPanel);
                        }
                        panel = kineticsPanel;
                        if (((KineticsPanel) kineticsPanel).getDatas().size() > 0)
                            enableSaveAs();
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(FacsKinView.this.getFrame(), "Could not read file.", "Error", JOptionPane.ERROR_MESSAGE);
                        //TODO: tesztelni hibas bemenetekkel
                        ex.printStackTrace();
                        return -1;
                    }
                } else {
                    if (file.getName().toLowerCase().endsWith(".zip")) {
                        try {
                            if (kineticsPanel == null) {
                                kineticsPanel = new KineticsPanel(FacsKinView.this);
                                kineticsPanel.addDatas(file, this);
                                tabbedPane.addTab("", kineticsPanel);
                                tabbedPane.setSelectedIndex(tabbedPane.getTabCount() - 1);
                                tabbedPane.setTabComponentAt(tabbedPane.getSelectedIndex(), new ButtonTabComponent("Kinetics", tabbedPane, FacsKinView.this, Color.RED));
                            } else {
                                kineticsPanel.addDatas(file, this);
                                tabbedPane.setSelectedComponent(kineticsPanel);
                            }
                            panel = kineticsPanel;
                            if (((KineticsPanel) kineticsPanel).getDatas().size() > 0)
                                enableSaveAs();
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(FacsKinView.this.getFrame(), "Could not read file.", "Error", JOptionPane.ERROR_MESSAGE);
                            //TODO: tesztelni hibas bemenetekkel
                            ex.printStackTrace();
                        }
                    } else {
                        try {
                            // we treat the file as FCS
                            FcsData fcsData = new FcsData(file, this);
                            panel = new FcsPanel(fcsData, FacsKinView.this);
                            // fcsPanel = (FcsPanel)panel;
                            fileName = fcsData.getFilename();
                            tabbedPane.addTab("", panel);
                            tabbedPane.setSelectedIndex(tabbedPane.getTabCount() - 1);
                            tabbedPane.setTabComponentAt(tabbedPane.getSelectedIndex(), new ButtonTabComponent(fcsData.getFilename(), tabbedPane, null, Color.BLACK));
                            // we don't give a reference to FacsKinView.this because we don't want tabRemoved to be notified when removing an FCS tab
                        } catch (IOException ex) {
                            JOptionPane.showMessageDialog(FacsKinView.this.getFrame(), "Could not read file.", "Error", JOptionPane.ERROR_MESSAGE);
                            //TODO: tesztelni hibas bemenetekkel
                            ex.printStackTrace();
                            panel = null;
                        }
                    }
                }
            }
            return panel;
        }
        @Override protected void succeeded(Object result) {
            if (result != null)
                ((FacsKinPanel) result).readyWithPainting();
        }

        private File[] files;
    }

    private class FileAppendFCSTask extends TaskWithProgress {
        FileAppendFCSTask(org.jdesktop.application.Application app, File file) {
            super(app);
            this.file = file;
        }
        @Override protected Object doInBackground() {
            FcsPanel panel = null;
            if (tabbedPane.getComponentAt(tabbedPane.getSelectedIndex()).getClass().getSimpleName().equals("FcsPanel")) {
                try {
                    panel = (FcsPanel) tabbedPane.getComponentAt(tabbedPane.getSelectedIndex());
                    FcsData fcsData = new FcsData(file, this);
                    if (fcsHasTime(fcsData))
                        panel.getData().append(fcsData);
                    else
                        panel = null;
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(FacsKinView.this.getFrame(), "Could not read file.", "Error", JOptionPane.ERROR_MESSAGE);
                    //TODO: tesztelni hibas bemenetekkel
                    ex.printStackTrace();
                    panel = null;
                 }                
            }
            return panel;  // return your result
        }
        @Override protected void succeeded(Object result) {
            if (result != null)
                ((FcsPanel) result).readyWithPainting();
        }

        private File file;
    }

    private class SaveAsClickedTask extends TaskWithProgress {
        SaveAsClickedTask(org.jdesktop.application.Application app, File file) {
            super(app);
            this.file = file;
        }
        @Override protected Object doInBackground() {
            if (tabbedPane.getComponentAt(tabbedPane.getSelectedIndex()).getClass().getSimpleName().equals("KineticsPanel")) {
                if (kineticsPanel != null)
                    try {
                        kineticsPanel.writeDatas(file);
                    } catch (IOException ex) {
                        JOptionPane.showMessageDialog(FacsKinView.this.getFrame(), "Could not write file.", "Error", JOptionPane.ERROR_MESSAGE);
                        return -1;
                    }
            } else if (tabbedPane.getComponentAt(tabbedPane.getSelectedIndex()).getClass().getSimpleName().equals("FcsPanel")) {
                FcsPanel panel = (FcsPanel) tabbedPane.getComponentAt(tabbedPane.getSelectedIndex());
                try {
                    facskin.io.FcsWriter.write(file, panel.getData(), this);
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(FacsKinView.this.getFrame(), "Could not write file.", "Error", JOptionPane.ERROR_MESSAGE);
                    return -1;
                }
            }
            return null;
        }
        
        @Override protected void succeeded(Object result) {
        }
        private File file;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        tabbedPane = new javax.swing.JTabbedPane();
        menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        LoadGates = new javax.swing.JMenuItem();
        SaveGates = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JSeparator();
        javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jCheckBoxMenuItem1 = new javax.swing.JCheckBoxMenuItem();
        jCheckBoxMenuItem2 = new javax.swing.JCheckBoxMenuItem();
        jCheckBoxMenuItem3 = new javax.swing.JCheckBoxMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        jMenuItem6 = new javax.swing.JMenuItem();
        javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
        statusPanel = new javax.swing.JPanel();
        javax.swing.JSeparator statusPanelSeparator = new javax.swing.JSeparator();
        statusMessageLabel = new javax.swing.JLabel();
        statusAnimationLabel = new javax.swing.JLabel();
        progressBar = new javax.swing.JProgressBar();

        mainPanel.setName("mainPanel"); // NOI18N
        mainPanel.setPreferredSize(new java.awt.Dimension(710, 510));

        tabbedPane.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);
        tabbedPane.setName("tabbedPane"); // NOI18N

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 705, Short.MAX_VALUE)
            .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(tabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, 705, Short.MAX_VALUE))
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 509, Short.MAX_VALUE)
            .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(tabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, 509, Short.MAX_VALUE))
        );

        menuBar.setName("menuBar"); // NOI18N

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(facskin.FacsKinApp.class).getContext().getActionMap(FacsKinView.class, this);
        fileMenu.setAction(actionMap.get("OpenFile")); // NOI18N
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(facskin.FacsKinApp.class).getContext().getResourceMap(FacsKinView.class);
        fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
        fileMenu.setName("fileMenu"); // NOI18N

        jMenuItem1.setAction(actionMap.get("FileOpen")); // NOI18N
        jMenuItem1.setText(resourceMap.getString("jMenuItem1.text")); // NOI18N
        jMenuItem1.setName("jMenuItem1"); // NOI18N
        fileMenu.add(jMenuItem1);

        jMenuItem5.setAction(actionMap.get("FileAppendFCS")); // NOI18N
        jMenuItem5.setText(resourceMap.getString("jMenuItem5.text")); // NOI18N
        jMenuItem5.setName("jMenuItem5"); // NOI18N
        fileMenu.add(jMenuItem5);

        jMenuItem3.setAction(actionMap.get("FileSaveAs")); // NOI18N
        jMenuItem3.setText(resourceMap.getString("jMenuItem3.text")); // NOI18N
        jMenuItem3.setName("jMenuItem3"); // NOI18N
        fileMenu.add(jMenuItem3);

        LoadGates.setAction(actionMap.get("LoadGates")); // NOI18N
        LoadGates.setName("LoadGates"); // NOI18N
        fileMenu.add(LoadGates);

        SaveGates.setAction(actionMap.get("SaveGates")); // NOI18N
        SaveGates.setText(resourceMap.getString("SaveGates.text")); // NOI18N
        SaveGates.setName("SaveGates"); // NOI18N
        fileMenu.add(SaveGates);

        jSeparator1.setName("jSeparator1"); // NOI18N
        fileMenu.add(jSeparator1);

        exitMenuItem.setAction(actionMap.get("quit")); // NOI18N
        exitMenuItem.setName("exitMenuItem"); // NOI18N
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        jMenu1.setText(resourceMap.getString("jMenu1.text")); // NOI18N
        jMenu1.setName("jMenu1"); // NOI18N

        jCheckBoxMenuItem1.setAction(actionMap.get("PreferencesToggleStandardisation")); // NOI18N
        jCheckBoxMenuItem1.setSelected(true);
        jCheckBoxMenuItem1.setText(resourceMap.getString("jCheckBoxMenuItem1.text")); // NOI18N
        jCheckBoxMenuItem1.setName("jCheckBoxMenuItem1"); // NOI18N
        jMenu1.add(jCheckBoxMenuItem1);

        jCheckBoxMenuItem2.setAction(actionMap.get("toggleDrawFunctionUntilMeasurementEnds")); // NOI18N
        jCheckBoxMenuItem2.setSelected(true);
        jCheckBoxMenuItem2.setText(resourceMap.getString("jCheckBoxMenuItem2.text")); // NOI18N
        jCheckBoxMenuItem2.setName("jCheckBoxMenuItem2"); // NOI18N
        jMenu1.add(jCheckBoxMenuItem2);

        jCheckBoxMenuItem3.setAction(actionMap.get("toggleColourByGroups")); // NOI18N
        jCheckBoxMenuItem3.setText(resourceMap.getString("jCheckBoxMenuItem3.text")); // NOI18N
        jCheckBoxMenuItem3.setName("jCheckBoxMenuItem3"); // NOI18N
        jMenu1.add(jCheckBoxMenuItem3);

        jMenuItem2.setAction(actionMap.get("PreferencesToggleDecimalSeparator")); // NOI18N
        jMenuItem2.setText(resourceMap.getString("menuItemDecimalSeparator.text")); // NOI18N
        jMenuItem2.setName("menuItemDecimalSeparator"); // NOI18N
        jMenu1.add(jMenuItem2);

        jMenuItem4.setAction(actionMap.get("PreferencesSetMaxTimeForAUC")); // NOI18N
        jMenuItem4.setText(resourceMap.getString("jMenuItem4.text")); // NOI18N
        jMenuItem4.setName("jMenuItem4"); // NOI18N
        jMenu1.add(jMenuItem4);

        jMenuItem8.setAction(actionMap.get("PreferencesSetBaselineLength")); // NOI18N
        jMenuItem8.setName("jMenuItem8"); // NOI18N
        jMenu1.add(jMenuItem8);

        jMenuItem9.setAction(actionMap.get("PreferencesSetNumberofBreaksinKineticsPlot")); // NOI18N
        jMenuItem9.setName("jMenuItem9"); // NOI18N
        jMenu1.add(jMenuItem9);
        
        jMenuItem7.setAction(actionMap.get("PreferencesSetNumberofBreaksinHistogramPlot")); // NOI18N
        jMenuItem7.setText(resourceMap.getString("jMenuItem7.text")); // NOI18N
        jMenuItem7.setName("jMenuItem7"); // NOI18N
        jMenu1.add(jMenuItem7);

        menuBar.add(jMenu1);

        helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
        helpMenu.setName("helpMenu"); // NOI18N

        jMenuItem6.setAction(actionMap.get("HelpUsersGuide")); // NOI18N
        jMenuItem6.setText(resourceMap.getString("jMenuItem6.text")); // NOI18N
        jMenuItem6.setName("jMenuItem6"); // NOI18N
        helpMenu.add(jMenuItem6);

        aboutMenuItem.setAction(actionMap.get("HelpAbout")); // NOI18N
        aboutMenuItem.setName("aboutMenuItem"); // NOI18N
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        statusPanel.setName("statusPanel"); // NOI18N

        statusPanelSeparator.setName("statusPanelSeparator"); // NOI18N

        statusMessageLabel.setName("statusMessageLabel"); // NOI18N

        statusAnimationLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        statusAnimationLabel.setName("statusAnimationLabel"); // NOI18N

        progressBar.setName("progressBar"); // NOI18N

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusPanelSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, 705, Short.MAX_VALUE)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(statusMessageLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 521, Short.MAX_VALUE)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusAnimationLabel)
                .addContainerGap())
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addComponent(statusPanelSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(statusMessageLabel)
                    .addComponent(statusAnimationLabel)
                    .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3))
        );

        setComponent(mainPanel);
        setMenuBar(menuBar);
        setStatusBar(statusPanel);
    }// </editor-fold>//GEN-END:initComponents
    
    //@author Daniel Herman
    public void disableSaveGates() {
        SaveGates.setEnabled(false);
    }
    
    private void enableSaveGates() {
        SaveGates.setEnabled(true);
    }
    
    public void disableLoadGates() {
        LoadGates.setEnabled(false);
    }
    
    private void enableLoadGates() {
        LoadGates.setEnabled(true);
    }
    
    @Action
    public Task SaveGates() {
        if (fileChooser.getSelectedFile() == null || !fileChooser.getSelectedFile().getName().toLowerCase().endsWith(".gate"))
            fileChooser.setSelectedFile(new File(this.fileName + ".gate"));
        int option = fileChooser.showSaveDialog(getFrame());
        Task task = null;
        if (JFileChooser.APPROVE_OPTION == option)
            task = new SaveGatesClickedTask(getApplication(), fileChooser.getSelectedFile());
        return task;
    }

    private class SaveGatesTask extends org.jdesktop.application.Task<Object, Void> {
        SaveGatesTask(org.jdesktop.application.Application app) {
            // Runs on the EDT.  Copy GUI state that
            // doInBackground() depends on from parameters
            // to SaveGatesTask fields, here.
            super(app);
        }
        @Override protected Object doInBackground() {
            // Your Task's code here.  This method runs
            // on a background thread, so don't reference
            // the Swing GUI from here.
            return null;  // return your result
        }
        @Override protected void succeeded(Object result) {
            // Runs on the EDT.  Update the GUI based on
            // the result computed by doInBackground().
        }
    }
    
    private class SaveGatesClickedTask extends org.jdesktop.application.Task<Object, Void> {
        SaveGatesClickedTask(org.jdesktop.application.Application app, File file) {
            super(app);
            this.file = file;
        }
        @Override protected Object doInBackground() {
            if (tabbedPane.getComponentAt(tabbedPane.getSelectedIndex()).getClass().getSimpleName().equals("FcsPanel")) {
                FcsPanel panel = (FcsPanel) tabbedPane.getComponentAt(tabbedPane.getSelectedIndex());
                GatePanel gatePanel = panel.getGatePanel();
                FileOutputStream outFile = null;
                ObjectOutputStream outStream = null; 
                try {
                    outFile = new FileOutputStream(file);
                    outStream = new ObjectOutputStream(outFile);
                    for(GateCheckBox e : gatePanel.getCheckBoxesGates()) {
                         outStream.writeObject(e.getText());
                         outStream.writeObject(e.data());
                    }    
                    outStream.writeObject(new String("$END$"));

                } catch(Exception e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                } finally {
                    if ( outStream != null ) {
                        try {
                            outStream.close();
                        } catch (IOException ex) {
                            Logger.getLogger(FacsKinView.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } 
                    
                    if ( outFile != null ) {
                        try {
                            outFile.close();
                        } catch (IOException ex) {
                            Logger.getLogger(FacsKinView.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
            return null;
        }
        @Override protected void succeeded(Object result) {
        }

        private File file;
    }

    @Action
    public Task LoadGates() {
        int option = fileChooser.showOpenDialog(getFrame());
        Task task = null;
        if (JFileChooser.APPROVE_OPTION == option)
            task = new LoadGatesClickedTask(getApplication(), fileChooser.getSelectedFile());
        return task;
    }
    
    private class LoadGatesClickedTask extends org.jdesktop.application.Task<Object, Void> {
        LoadGatesClickedTask(org.jdesktop.application.Application app, File file) {
            super(app);
            this.file = file;
        }
        @Override protected Object doInBackground() {
            if (tabbedPane.getComponentAt(tabbedPane.getSelectedIndex()).getClass().getSimpleName().equals("FcsPanel")) {
                FcsPanel panel = (FcsPanel) tabbedPane.getComponentAt(tabbedPane.getSelectedIndex());
                 GatePanel gatePanel = panel.getGatePanel();
                 FileInputStream inFile = null;
                 ObjectInputStream inStream = null;
                try {
                    gatePanel.getCheckBoxesGates().clear();
                    inFile = new FileInputStream(file);
                    inStream = new ObjectInputStream(inFile);

                    int i = 1;
                    String text = null;
                        GateData data;
                    while(true) {
                        Object o = inStream.readObject();

                        if ( i == 1 ) {
                            text = (String)o;
                            if ( text.equals("$END$") )
                                break;
                            
                            i++;
                            continue;
                        } else if ( i == 2 ) {
                            data = (GateData)o;
                            gatePanel.addNewGateCheckBox(text, data);
                            i=1;
                            continue;
                        }
                    }
                } catch(Exception e) {
                    System.out.println(e.getMessage());
                } finally {
                    if ( inStream != null ) {
                        try {
                            inStream.close();
                        } catch (IOException ex) {
                            Logger.getLogger(FacsKinView.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } 
                    
                    if ( inFile != null ) {
                        try {
                            inFile.close();
                        } catch (IOException ex) {
                            Logger.getLogger(FacsKinView.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }                    
                }
            }
            return null;
        }
        @Override protected void succeeded(Object result) {
        }

        private File file;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem ExportFCS;
    private javax.swing.JMenuItem ImportFCS;
    private javax.swing.JMenuItem LoadGates;
    private javax.swing.JMenuItem SaveGates;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem1;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem2;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JLabel statusAnimationLabel;
    private javax.swing.JLabel statusMessageLabel;
    private javax.swing.JPanel statusPanel;
    private javax.swing.JTabbedPane tabbedPane;
    // End of variables declaration//GEN-END:variables
}
