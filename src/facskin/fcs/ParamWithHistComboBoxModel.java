package facskin.fcs;

import facskin.formats.FcsData;

/**
 * Combo box model for parameters in an FCS file + one more item in the end, text HISTOGRAM_STR
 * @author ambi
 */
public class ParamWithHistComboBoxModel extends ParamComboBoxModel {
    public final String HISTOGRAM_STR="Histogram";

    public ParamWithHistComboBoxModel(FcsData data) {
        super(data);
    }

    @Override
    public int getSize() {
        return super.getSize() + 1;
    }

    @Override
    public Object getElementAt(int i) {
        if (i == getSize() - 1)
            return HISTOGRAM_STR;
        else
            return super.getElementAt(i);
    }
}
