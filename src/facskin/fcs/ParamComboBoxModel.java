package facskin.fcs;

import facskin.formats.FcsData;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 * Combo box model for parameters in an FCS file.
 * @author ambi
 */
public class ParamComboBoxModel implements ComboBoxModel {
    protected String selected = null;
    private FcsData data = null;

    public ParamComboBoxModel(FcsData data) {
        this.data = data;
    }

    @Override
    public void setSelectedItem(Object object) {
        if (object instanceof String) {
            selected = (String) object;
        } else selected = null;
    }

    @Override
    public Object getSelectedItem() {
        return selected;
    }

    @Override
    public int getSize() {
        if (data != null)
            return data.getParameterCount();
        else
            return 0;
    }

    @Override
    public Object getElementAt(int i) {
        if (data != null)
            return data.getParameterName(i);
        else
            return "";
    }

    @Override
    public void addListDataListener(ListDataListener listDataListener) {
    }

    @Override
    public void removeListDataListener(ListDataListener listDataListener) {
    }
}
