package facskin.fcs;

import facskin.formats.FcsData;

/**
 * Combo box model for parameters in an FCS file + one more item in the front, text CHOOSE_STR
 * @author ambi
 */
public class ParamWithChooseComboBoxModel extends ParamComboBoxModel {
    private final String CHOOSE_STR="Choose One";

    public ParamWithChooseComboBoxModel(FcsData data) {
        super(data);
    }

    @Override
    public int getSize() {
        return super.getSize() + 1;
    }

    @Override
    public Object getElementAt(int i) {
        if (i == 0)
            return CHOOSE_STR;
        return super.getElementAt(i - 1);
    }

    @Override
    public Object getSelectedItem() {
        Object retValue;
        if (selected == null) retValue = CHOOSE_STR;
        else retValue = super.getSelectedItem();
        return retValue;
    }
}
