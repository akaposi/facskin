package facskin.fcs;

import facskin.formats.FcsData;

/**
 * Combo box model for parameters in an FCS file + one more item in the front, text ONE_STR
 * @author ambi
 */
public class ParamWith1ComboBoxModel extends ParamComboBoxModel {
    private final String ONE_STR="1.0";

    public ParamWith1ComboBoxModel(FcsData data) {
        super(data);
    }

    @Override
    public int getSize() {
        return super.getSize() + 1;
    }

    @Override
    public Object getElementAt(int i) {
        if (i == 0)
            return ONE_STR;
        return super.getElementAt(i - 1);
    }

    @Override
    public Object getSelectedItem() {
        Object retValue;
        if (selected == null) retValue = ONE_STR;
        else retValue = super.getSelectedItem();
        return retValue;
    }
}
