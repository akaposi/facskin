package facskin;

/**
 * Interface providing a function that can be called if new instance
 * of an application are launched. See {@link ApplicationInstanceManager}.
 * @author ambi
 */
public interface ApplicationInstanceListener {
    /**
     * This method is called when a new instance is created.
     * @param args These are the parameters that were passed as command line
     * arguments to the new instance.
     */
    public void newInstanceCreated(String[] args);
}
