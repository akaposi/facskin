package facskin.math;

import java.util.Collections;
import java.util.List;

/**
 * Basic statistical functions.
 * @author ambi
 */
public class Statistics {
    public static double overlap(double high1, double low1, double high2, double low2) {
        double[] highs = new double[2];
        double[] lows = new double[2];
        highs[0] = high1;
        highs[1] = high2;
        lows[0] = low1;
        lows[1] = low2;
        return overlap(highs, lows);
    }

    private static double overlap(double[] highs, double[] lows) {
        double high = highs[0], low = lows[0];
        int i = 1;
        while (i < highs.length && high > low) {
            if (highs[i] < high)
                high = highs[i];
            if (lows[i] > low)
                low = lows[i];
            i ++;
        }
        if (high <= low)
            return 0.0;
        double x = 1, diff = high - low;
        for (i = 0; i < highs.length; i ++)
            if (diff / (highs[i] - lows[i])  < x)
                x = diff / (highs[i] - lows[i]);
        return x;
    }

    public static double median(List<Double> l) {
        Collections.sort(l);
        if (l.size() % 2 == 1)
            return l.get((l.size() - 1) / 2);
        return (l.get(l.size() / 2) + l.get(l.size() / 2 - 1)) / 2;
    }

    public static double quantile(List<Double> l, double q) {
        Collections.sort(l);
        q = q * (l.size() - 1);
        if (q != 0.0 && Math.abs((Math.round(q) - q)/q) < 0.00001)
            return l.get((int) Math.round(q));
        if (q == 0.0)
            return l.get(0);
        return l.get((int) Math.floor(q)) + (q-Math.floor(q)) * (l.get((int) Math.floor(q) + 1) - l.get((int) Math.floor(q)));
    }
}
