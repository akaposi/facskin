package facskin.math;

import facskin.formats.Data;
import facskin.formats.KineticsData;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.math.MathException;
import org.apache.commons.math.distribution.NormalDistributionImpl;

/**
 * Probability Binning comparison with functions suggesting bin counts.
 * @author ambi
 */
public class ProbabilityBinning {
    private double[] limits; // binCount-1 limits (leftmost and rightmost bins are open to the left and right, correspondingly)
                             // intervals are right closed, that is, if limits[0] == 1.0, value 1.0 goes into the first bin (with bin index 0)
    private int[]    control_counts;
    private double[] control_relcounts;
    private int      binCount, control_size;

    public static int defaultBinCount(List<Data> datas) {
        int minLength = datas.get(0).getDataLength();
        for (int i = 1; i < datas.size(); ++i)
            if (minLength > datas.get(i).getDataLength())
                minLength = datas.get(i).getDataLength();

        int defaultBinCount = (int) Math.round((double)minLength / 20.0);
        if (defaultBinCount < 2) defaultBinCount = 2;

        return defaultBinCount;
    }

    public static int defaultBinCount(List<KineticsData> datas, int fct, int p) {
        int minLength = datas.get(0).getFunction(fct).getParam(p).getDataLength();
        for (int i = 1; i < datas.size(); ++i)
            if (minLength > datas.get(i).getFunction(fct).getParam(p).getDataLength())
                minLength = datas.get(i).getFunction(fct).getParam(p).getDataLength();

        int defaultBinCount = (int) Math.round((double)minLength / 20.0);
        if (defaultBinCount < 2) defaultBinCount = 2;

        return defaultBinCount;
    }

    public ProbabilityBinning(Data control, int binCount) {
        this.binCount = binCount;

        control_size = control.getDataLength();

        double[] c = new double[control_size];
        for (int i = 0; i < control_size; ++i)
            c[i] = control.getData(i);
        Arrays.sort(c);

        limits = new double[binCount - 1]; // if a value is the same as the limit it belongs to bin to the left
        control_counts = new int[binCount];

        int i = 0; // index in control (c)
        for (int b = 0; b < binCount - 1; ++b) { // current bin
            control_counts[b] = 0;
            int stop_here = (int)Math.round(((double)b+1.0)*((double)control_size / (double)binCount) - 1.0);

            while (i < control_size && (i <= stop_here || (i > stop_here && c[i] == c[i-1]))) {
                ++control_counts[b];
                ++i;
            }
            limits[b] = c[i-1];
        }
        control_counts[binCount - 1] = control_size - i;

        control_relcounts = new double[binCount];
        for (int b = 0; b < binCount; ++b)
            control_relcounts[b] = (double)control_counts[b] / (double)control_size;
    }

    public double[] compare(Data test) {
        PBResult pbres = compareFull(test);
        double[] res = {pbres.t_value, pbres.p_value};
        return res;
    }

    public PBResult compareFull(Data test) {
        int test_size = test.getDataLength();

        double[] t = new double[test_size];
        for (int i = 0; i < test_size; ++i)
            t[i] = test.getData(i);
        Arrays.sort(t);

        int[] test_counts = new int[binCount];
        for (int i = 0; i < test_counts.length; ++i) test_counts[i] = 0;
        int i = 0; // index in test (t)
        for (int b = 0; b < binCount - 1; ++b) { // current bin
            while (i < test_size && t[i] <= limits[b]) {
                ++test_counts[b];
                ++i;
            }
        }
        test_counts[binCount - 1] = test_size - i;

        double[] test_relcounts = new double[binCount];
        for (int b = 0; b < binCount; ++b)
            test_relcounts[b] = (double)test_counts[b] / (double)test_size;

        double chisq = 0.0;
        for (int b = 0; b < binCount; ++b)
            if (control_relcounts[b] + test_relcounts[b] > 0)
                chisq += (control_relcounts[b]-test_relcounts[b])*(control_relcounts[b]-test_relcounts[b])
                        / (control_relcounts[b] + test_relcounts[b]);

        double chisq_mean = (double)binCount / (double)Math.min(control_size, test_size);
        double chisq_sd = Math.sqrt((double)binCount) / (double)Math.min(control_size, test_size);

        double t_value = Math.max(0.0, (chisq-chisq_mean)/chisq_sd);

        PBResult res = new PBResult();
        res.t_value = t_value;
        res.p_value = getP(t_value);
        res.control_counts = control_counts;
        res.limits = limits;
        res.test_counts = test_counts;

        return res;
    }

    private double getP(double t_value) {
        NormalDistributionImpl nd = new NormalDistributionImpl();
        double p_value = -1.0;
        try {
            p_value = 1.0 - nd.cumulativeProbability(t_value);
        } catch (MathException ex) {}
        
        return p_value;
    }

    public double[] getLimits() { return limits; }

    public int[] getControlCounts() { return control_counts; }

}
