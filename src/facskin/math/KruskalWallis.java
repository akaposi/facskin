package facskin.math;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.math.MathException;
import org.apache.commons.math.distribution.ChiSquaredDistributionImpl;

/**
 * Class for performing Kruskal-Wallis rank sum test. The implementation
 * follows R's kruskal.test function.
 * @author ambi
 */
public class KruskalWallis {
    public static double[] test(List<List<Double>> xx) {
        if (xx.size() == 1)
            return new double[] {0.0, 1.0};

        List<ValueWithGroup> x = new ArrayList<ValueWithGroup>();

        // ordered data
        for (int i = 0; i < xx.size(); ++i)
            for (int j = 0; j < xx.get(i).size(); ++j)
                x.add(new ValueWithGroup(xx.get(i).get(j), i));
        Collections.sort(x);

        // calculation of ranks
        List<Double> r = new ArrayList<Double>();
        List<Double> ties = new ArrayList<Double>();
        for (double i = 0; i < x.size(); ++i) r.add(i+1);
        int i = 0; // we inspect him
        int j = 0; // the last having the same value as i
        while (i < r.size()) {
//            System.out.println(i+" "+x.get(i).v+" ("+x.get(i).g+")"); //DEBUG

            if (i == r.size() - 1 || x.get(i).v != x.get(i+1).v) {
                if (j < i)
                    for (int k = j; k <= i; ++k) r.set(k, ((double) (j+i+2))/2);

                ties.add(0.0);
                for (int k = j; k <= i; ++k)
                    ties.set(ties.size()-1, ties.get(ties.size()-1) + 1.0);
                j = i + 1;
            }
            ++i;
        }

        //DEBUG
//        System.out.print("ties: ");
//        for (i = 0; i < ties.size(); ++i)
//            System.out.print(ties.get(i)+" ");
//        System.out.println();
//        System.out.print("r: ");
//        for (i = 0; i < r.size(); ++i)
//            System.out.print(r.get(i)+" ");
//        System.out.println();
//        System.out.print("v: ");
//        for (i = 0; i < x.size(); ++i)
//            System.out.print(x.get(i).v+" ");
//        System.out.println();
//        System.out.print("g: ");
//        for (i = 0; i < x.size(); ++i)
//            System.out.print(x.get(i).g+" ");
//        System.out.println();

        double STATISTIC = 0.0;

        // R: STATISTIC <- sum(tapply(r, g, "sum")^2 / tapply(r, g, "length"))
        for (int g = 0; g < xx.size(); ++g) { // we go through all the groups
            double groupsize = 0.0;
            double ranksum = 0.0;
            for (i = 0; i < r.size(); ++i)
                if (x.get(i).g == g) {
                    groupsize++;
                    ranksum += r.get(i);
                }
            //System.out.println(g+" "+groupsize+" "+ranksum+" "); //DEBUG
            STATISTIC += ranksum * ranksum / groupsize;
        }

        //System.out.println("STATISTIC0: " + STATISTIC); //DEBUG

        // R: STATISTIC <- ((12 * STATISTIC / (n * (n + 1)) - 3 * (n + 1)) /
        //              (1 - sum(TIES^3 - TIES) / (n^3 - n)))
        int n = x.size();

        double tiediffsum = 0.0;
        for (i = 0; i < ties.size(); ++i)
            tiediffsum += ties.get(i)*ties.get(i)*ties.get(i) - ties.get(i);

        STATISTIC = (12 * STATISTIC / (n * (n + 1)) - 3 * (n + 1))
                  / (1 - tiediffsum / (n*n*n - n));

        //System.out.println("STATISTIC1: " + STATISTIC); //DEBUG

        ChiSquaredDistributionImpl chisq = new ChiSquaredDistributionImpl(xx.size() - 1);
        double p_value;
        try {
            p_value = 1 - chisq.cumulativeProbability(STATISTIC);
        } catch (MathException ex) {
            p_value = -1;
        }
        double[] result = {STATISTIC, p_value};
        return result;
    }
}

class ValueWithGroup implements Comparable {
    public final double v;
    public final int g;
    public ValueWithGroup(double value, int group) { v = value; g = group; }

    @Override
    public int compareTo(Object t) {
        return Double.compare(v, ((ValueWithGroup) t).v);
    }
}
