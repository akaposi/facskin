package facskin.math;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class implementing Mann-Whitney U test (or Wilcoxon test).
 * @author ambi
 */
public class MannWhitney {
    private static Map<int[], Integer> _choose = new HashMap<int[], Integer>();
    private static Map<int[], Integer> _cwilcox = new HashMap<int[], Integer>();

    public static double[] test(List<Double> p1, List<Double> p2) {
        double u = 0.0;
        for (int i = 0; i < p1.size(); i ++) {
            for (int j = 0; j < p2.size(); j ++)
                if (p1.get(i) > p2.get(j)) {
                    u += 1;
                } else {
                    if (p1.get(i).equals(p2.get(j)))
                        u += 0.5;
                }
        }

        u = Math.max(u, p1.size() * p2.size() - u);
        double p_value;
        if (Math.max(p1.size(), p2.size()) < 10) { // TODO: this limit used to be 30 but it dies sometimes
            p_value = pwilcox(p1.size() * p2.size() - u, Math.min(p1.size(), p2.size()), Math.max(p1.size(), p2.size())) * 2;
        } else {
            // we approximate the u distribution with normal distribution and standardize
            // mu=n1*n2/2
            // sigma=sqrt(n1*n2*(n1+n2+1)/12)
            double z = (u - p1.size() * p2.size() / 2) /
                    Math.sqrt(p1.size() * p2.size() * (p1.size() + p2.size() + 1) / 12);
            p_value = pstnorm(z) * 2.0; // 2-sided p-value
        }
        if (p_value > 1) p_value = 1;
        double[] result = {u, p_value};
        return result;
    }

    private static double pstnorm(double z) {
        // we have a look how much area is under the standard normal distribution
        // curve after z. density function: dnorm(x) = 1/sqrt(2*pi) * exp(-x^2/2)
        // 1/sqrt(2*pi) = 0.3989423
        // numerical integration:
        double p = 0.0, step = 0.00001;
        for (double d = z; d < Math.max(z + 1, 10); d += step) {
            p += (Math.exp(-0.5 * d * d) +
                            Math.exp(-0.5 * (d + step) * (d + step))) / 2 * step;
        }
        return p * 0.3989422804014327;
    }

    private static double pwilcox(double q, int m, int n) {
        int c = choose(m + n, n);
        double p = 0;
        for (int i = 0; i <= q; i ++) {
            p += ((double) cwilcox(i, m, n)) / ((double) c);
        }
        return p;
    }

    private static int cwilcox(int k, int m, int n) {
        int[] p = {k, m, n};
        if (!_cwilcox.containsKey(p)) {
            int u = m*n;
            if (k < 0 || k > u) {
                _cwilcox.put(p, 0);
            } else {
                if (m == 0 && n == 0) {
                    if (k == 0) _cwilcox.put(p, 1); else _cwilcox.put(p, 0);
                } else {
                    _cwilcox.put(p, cwilcox(k-n, m-1, n) + cwilcox(k, m, n-1));
                }
            }
        }
        return _cwilcox.get(p);
    }

    private static int choose(int m, int n) {
        if (n == 0 || m == n)
            return 1;
        int[] p = {m, n};
        if (!_choose.containsKey(p))
            _choose.put(p, choose(m - 1, n - 1) + choose(m - 1, n));
        return _choose.get(p);
    }
}
