package facskin.math;

/**
 * Class representing mathematical functions.
 * @author ambi
 */
public class Functions {
    public static double getAUC(String name, double[] p, double tmax) {
        double auc = 0;
        Double[] params = new Double[p.length];
        for (int i = 0; i < p.length; i ++)
            params[i] = p[i];
        double starting = fct(name, 0, params);
        double prev = starting;
        double step = tmax / 100;
        tmax = tmax + step / 2;
        for (double x = step; x <= tmax; x += step) {
            double y = fct(name, x, params);
            //double diff = (y - starting + prev - starting) / 2 * step;
            double diff = (y + prev) / 2 * step;
            if (diff > 0) auc += diff;
            prev = y;
        }
        return auc;
    }

    public static double fct(String name, double x, Double[] p) {
        if (name.equals("constant")) return(constant(x, p));
        if (name.equals("logistp"))  return(logistp(x, p));
        if (name.equals("logistn"))  return(logistn(x, p));
        if (name.equals("dlogistp")) return(dlogistp(x, p));
        if (name.equals("dlogistn")) return(dlogistn(x, p));
        
        // This cannot happen
        // TODO: replace this with an exception and test
        System.out.println("I don't know about this function.");
        return Math.PI;
    }

    public static String getFunctionName(int fct) {
        if (fct == 0) return "constant";
        if (fct == 1) return "logist+";
        if (fct == 2) return "logist-";
        if (fct == 3) return "dlogist+";
        if (fct == 4) return "dlogist-";
        return "";
    }

    public static int parameterCount(int fct) {
        // AUC is counted as well:
        if (fct == 0) return 2;
        if (fct == 1 || fct == 2) return 5;
        return 9;
    }

    public static double constant(double x, Double[] p) {
        return Math.abs(p[0]);
    }

    public static double logistp(double x, Double[] p) {
        double y0 = Math.abs(p[0]);
        double y2 = Math.abs(p[1]);
        if (y0 > y2) { double temp = y0; y0 = y2; y2 = temp; }
        double x1 = Math.abs(p[2]);
        double m1 = Math.abs(p[3]);
        if (y0 == y2)
            return y0;
        else
            return y0 + (y2-y0) / (1 + Math.exp( (-x+x1) * 4*m1/(y2-y0) ));
    }

    public static double logistn(double x, Double[] p) {
        double y0 = Math.abs(p[0]);
        double y2 = Math.abs(p[1]);
        if (y2 > y0) { double temp = y0; y0 = y2; y2 = temp; }
        double x1 = Math.abs(p[2]);
        double m1 = -Math.abs(p[3]);
        if (y0 == y2)
            return y0;
        else
            return y0 + (y2-y0) / (1 + Math.exp( (-x+x1) * 4*m1/(y2-y0) ));
    }

    public static double dlogistp(double x, Double[] p) {
        double y0  =  Math.abs(p[0]);
        double y1  =  Math.abs(p[1]);
        double y2  =  Math.abs(p[2]);
        double x1  =  Math.abs(p[3]);
        double xd0 =  Math.abs(p[4]);
        double xd2 =  Math.abs(p[5]);
        double m0  =  Math.abs(p[6]);
        double m2  = -Math.abs(p[7]);
        if (y0 > y1 && y0 >= y2) {
            double temp = y1; y1 = y0; y0 = temp;
        } else {
            if (y2 > y1) { double temp = y1; y1 = y2; y2 = temp; }
        }
        if (x < x1) {
            if (xd0 > x1) xd0 = x1;
            if (y1 == y0 || xd0 == 0)
                return y0;
            else
                return y0 + (y1 - y0) / (1 + Math.pow((x1 - x) / xd0, 4 * xd0 * m0 / (y1 - y0)));
        } else {
            if (x == x1) {
                return y1;
            } else {
                if (y1 == y2 || xd2 == 0)
                    return y2;
                else
                    return y2 + (y1 - y2) / (1 + Math.pow((x - x1) / xd2, 4 * xd2 * m2 / (y2 - y1)));
            }
        }
    }

    public static double dlogistn(double x, Double[] p) {
        double y0  =  Math.abs(p[0]);
        double y1  =  Math.abs(p[1]);
        double y2  =  Math.abs(p[2]);
        double x1  =  Math.abs(p[3]);
        double xd0 =  Math.abs(p[4]);
        double xd2 =  Math.abs(p[5]);
        double m0  = -Math.abs(p[6]);
        double m2  =  Math.abs(p[7]);
        if (y0 < y1 && y0 <= y2) {
            double temp = y1; y1 = y0; y0 = temp;
        } else {
            if (y2 < y1) { double temp = y1; y1 = y2; y2 = temp; }
        }
        if (x < x1) {
            if (xd0 > x1) xd0 = x1;
            if (y1 == y0 || xd0 == 0)
                return y0;
            else
                return y0 + (y1 - y0) / (1 + Math.pow((x1 - x) / xd0, 4 * xd0 * m0 / (y1 - y0)));
        } else {
            if (x == x1) {
                return y1;
            } else {
                if (y1 == y2 || xd2 == 0)
                    return y2;
                else
                    return y2 + (y1 - y2) / (1 + Math.pow((x - x1) / xd2, 4 * xd2 * m2 / (y2 - y1)));
            }
        }
    }

    public static String[] logistParamNames() {
        String[] paramNames = new String[5]; // with AUC
        paramNames[0] = "Starting value";
        paramNames[1] = "Ending value";
        paramNames[2] = "Time to reach 50% value";
        paramNames[3] = "Slope at 50% value";
        paramNames[4] = "AUC"; // with AUC
        return paramNames;
    }

    public static String[] dlogistpParamNames() {
        String[] paramNames = new String[9]; // with AUC
        paramNames[0] = "Starting value";
        paramNames[1] = "Maximum value";
        paramNames[2] = "Ending value";
        paramNames[3] = "Time to reach maximum";
        paramNames[4] = "Time from first 50% value to maximum";
        paramNames[5] = "Time from maximum to second 50% value";
        paramNames[6] = "Slope at first 50% value";
        paramNames[7] = "Slope at second 50% value";
        paramNames[8] = "AUC"; // with AUC
        // TODO: calculated parameters
        return paramNames;
    }

    public static String[] dlogistnParamNames() {
        String[] paramNames = new String[9]; // with AUC
        paramNames[0] = "Starting value";
        paramNames[1] = "Minimum value";
        paramNames[2] = "Ending value";
        paramNames[3] = "Time to reach Minimum";
        paramNames[4] = "Time from first 50% value to minimum";
        paramNames[5] = "Time from minimum to second 50% value";
        paramNames[6] = "Slope at first 50% value";
        paramNames[7] = "Slope at second 50% value";
        paramNames[8] = "AUC"; // with AUC
        // TODO: calculated parameters
        return paramNames;
    }

    public static String getParameterName(String fct, int param) {
        if (fct.startsWith("logist"))   return logistParamNames()[param];
        if (fct.startsWith("dlogistp")) return dlogistpParamNames()[param];
        if (fct.startsWith("dlogistn")) return dlogistnParamNames()[param];
        if (param == 0)                 return "Constant value"; // with AUC
        return "AUC"; // with AUC
    }

    public static String getParameterName(int fct, int param) {
        if (fct == 1 || fct == 2) return logistParamNames()[param];
        if (fct == 3)             return dlogistpParamNames()[param];
        if (fct == 4)             return dlogistnParamNames()[param];
        if (param == 0)           return "Constant value"; // with AUC
        return "AUC"; // with AUC
    }

    public static int[] paramsToStandardize(String name) {
        // TODO: AUC is not among these parameters, however it also needs standardization
        int[] p;
        if (name.startsWith("logist")) {
            p = new int[3];
            p[0] = 0;
            p[1] = 1;
            p[2] = 3;
            return p;
        } else {
            if (name.startsWith("dlogist")) {
                p = new int[5];
                p[0] = 0;
                p[1] = 1;
                p[2] = 2;
                p[3] = 6;
                p[4] = 7;
                return p;
            } else { // constant
                p = new int[1];
                p[0] = 0;
                return p;
            }
        }
    }

    public static boolean isParameterOutside(int fct, int i, Double[] pars, double minX, double maxX, double minY, double maxY, double startY, double endY) {
        if ((fct == 1 || fct == 2) && (i == 2 || i == 3) && (pars[2] > maxX || pars[2] < minX)) return true;
         // (x < minX) is only possible if the beginning of the measurement is cut (the first value is not at time point zero)
        if (fct == 3 || fct == 4) {
          if ((i == 1 || i == 3) && (pars[3] > maxX || pars[3] < minX)) return true;
          if (i == 4 || i == 6) { double x = pars[3]-pars[4]; if (x > maxX || x < minX) return true; }
          if (i == 5 || i == 7) { double x = pars[3]+pars[5]; if (x > maxX || x < minX) return true; }
        }
        if (fct == 1 || fct == 2) {
            if (i == 0 || i == 2 || i == 3)
                if (pars[0] < startY*0.5 || pars[0] > startY*2) return true;
            if (i == 1 || i == 2 || i == 3)
                if (pars[1] < endY*0.5 || pars[1] > endY*2) return true;
        }
        if (fct == 3 || fct == 4) {
            if (i == 0 || i == 4 || i == 6)
                if (pars[0] < startY*0.5 || pars[0] > startY*2) return true;
            if (i == 1 || i == 3)
                if (pars[1] < minY*0.5 || pars[1] > maxY*2) return true;
            if (i == 2 || i == 5 || i == 7)
                if (pars[2] < endY*0.5 || pars[2] > endY*2) return true;
        }
        return false;
    }
}
