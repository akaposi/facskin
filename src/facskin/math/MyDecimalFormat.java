package facskin.math;

import facskin.FacsKinApp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * My own number formatting class.
 * @author ambi
 */
public class MyDecimalFormat {
    public static String format(double x) {
        DecimalFormatSymbols dfs;
        if (FacsKinApp.getDecimalSeparator() == '.')
            dfs = new DecimalFormatSymbols(Locale.ENGLISH);
        else
            dfs = new DecimalFormatSymbols(Locale.GERMAN);

        String fs = "0.0##";
        if (Math.abs(x) < 0.001 && x != 0.0)
            fs = "0.0##E0";

        DecimalFormat df = new DecimalFormat(fs, dfs);
        return df.format(x);
    }

    public static String formatShort(double x) {
        DecimalFormatSymbols dfs;
        if (FacsKinApp.getDecimalSeparator() == '.')
            dfs = new DecimalFormatSymbols(Locale.ENGLISH);
        else
            dfs = new DecimalFormatSymbols(Locale.GERMAN);

        String fs = "0.0##";

        //max. 5 hosszú lehet a string, ha a .-ot nem számítjuk
        // (de a - jeleket igen, sőt, az E utáni 0 is lehet 2-jegyű)
        double absx = Math.abs(x);
        if (x >= 1000 && fs.equals("0.0##"))
            fs = "0.0E0";
        if (x <= -1000 && fs.equals("0.0##"))
            fs = "0E0";
        if (absx >= 100 && fs.equals("0.0##")) //. előtt 3 számjegy
            fs = "0.0";
        if (absx >= 10 && fs.equals("0.0##")) //. előtt 2 számjegy
            fs = "0.0#";
        if (x > 0 && x <= 0.001 && fs.equals("0.0##"))
            fs = "0E0";
        if (x < 0 && x >= -0.001 && fs.equals("0.0##"))
            fs = "0E0";
        if (absx == 0.0)
            return "0";

        DecimalFormat df = new DecimalFormat(fs, dfs);
        return df.format(x);
    }

    public static String formatLong(double x) {
        DecimalFormatSymbols dfs;
        if (FacsKinApp.getDecimalSeparator() == '.')
            dfs = new DecimalFormatSymbols(Locale.ENGLISH);
        else
            dfs = new DecimalFormatSymbols(Locale.GERMAN);

        String fs = "0.####################";
        if (Math.abs(x) < 0.0001 && x != 0.0) {
            fs = "0.####################E00";
            DecimalFormat df = new DecimalFormat(fs, dfs);
            String str = df.format(x);
            return str.replace('E', 'e');
        }

        DecimalFormat df = new DecimalFormat(fs, dfs);
        return df.format(x);
    }

    public static String formatLongPoint(double x) {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.ENGLISH);

        String fs = "0.####################";
        if (Math.abs(x) < 0.0001 && x != 0.0) {
            fs = "0.####################E00";
            DecimalFormat df = new DecimalFormat(fs, dfs);
            String str = df.format(x);
            return str.replace('E', 'e');
        }

        DecimalFormat df = new DecimalFormat(fs, dfs);
        return df.format(x);
    }
}
