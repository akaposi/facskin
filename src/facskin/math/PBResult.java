package facskin.math;

/**
 *
 * @author ambi
 */
public class PBResult {
    public double t_value;
    public double p_value;
    public int[] control_counts;
    public int[] test_counts;
    public double[] limits;

    public PBResult() {}
}
