package facskin;

import org.jdesktop.application.Application;
import org.jdesktop.application.Task;

/**
 * A class for representing long-lasting tasks that provide progress information
 * which can be showed by the progress bar of {@link FacsKinView}
 * @author ambi
 */
public class TaskWithProgress extends Task<Object, Void> {

    public TaskWithProgress(Application application) {
        super(application);
    }

    public void setProgressValue(float value) {
        this.setProgress(value);
    }

    @Override
    protected Object doInBackground() throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

