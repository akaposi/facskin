package facskin.formats;

/**
 * Class representing a list for a scatter plot with some statistical data (max, min).
 * Used by {@link FcsData}.
 * @author ambi
 */
import java.awt.Point;
import java.util.HashMap;

public class PlotList extends HashMap<Point, Integer> {
    public PlotList() {
    }

    public void put(int x, int y, Integer value) {
        if (value.compareTo(maxValue) == 1) {
            maxValue = value;
        }
        if (value.compareTo(minValue) == -1) {
            minValue = value;
        }
        Point p = new Point(x, y);
        put(p, value);
    }

    public Integer get(int x, int y) {
        return get(new Point(x, y));
    }

    public double getNormalized(int x, int y) {
        int scale = maxValue - minValue;
        Point p = new Point(x, y);
        double d = (get(p) - minValue) / (double) scale;
        return d;
    }

    private Integer maxValue = Integer.MIN_VALUE;
    private Integer minValue = Integer.MAX_VALUE;
}
