package facskin.formats;

/**
 * Class representing a list for a histogram with some statistical data (max, min).
 * Used by {@link FcsData}.
 * @author ambi
 */
import java.util.ArrayList;
import java.util.Iterator;

public class HistogramList extends ArrayList<Integer> {
    public HistogramList(int scale, Double from, Double to) {
        super();
        setMinX(from);
        setMaxX(to);
        for (int i=0;i<scale;i++){
            add(i,0);
        }
    }

    public Integer getMaxY() {
        Iterator<Integer> it = iterator();
        Integer max = Integer.MIN_VALUE;
        while (it.hasNext()){
            Integer c = it.next();
            if (c.compareTo(max) == 1) max = c;
        }
        return max;
    }

    public Integer getMinY() {
        return 0;
    }

    public Double getMinX() {
        return minX;
    }

    public void setMinX(Double minX) {
        this.minX = minX;
    }

    public Double getMaxX() {
        return maxX;
    }

    public void setMaxX(Double maxX) {
        this.maxX = maxX;
    }

    private Double minX;
    private Double maxX;
}
