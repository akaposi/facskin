package facskin.formats;

import facskin.FacsKinApp;
import facskin.TaskWithProgress;
import facskin.io.FcsOpener;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Class representing FCS data. It has built in support for
 * gating, canceling all gates and for creating histograms and
 * scatter plots. It has also caches for these latter.
 * @author ambi
 */
public class FcsData implements Serializable {

    // file from which the FCS was read
    private File file;

    // data containers
    private ArrayList<double[]> data;
    private ArrayList<double[]> firstData;
    private int paramCount;
    
    //@author Daniel Herman: data containers
    private ArrayList<double[]> gatedData;
    //--
    
    // caches for creating histograms, scatter plots
    private double[] minValues;
    private double[] minValuesGreaterThan0;
    private double[] maxValues;
    private double[] minLogValues;
    private double[] maxLogValues;
    private HistogramList[][] histoCache; // dimensions: number of parameter, (0:linearis scale, 1:logarithmic scale)
    private PlotList[][][][] plotCache;   // dimensions: number of parameter on axis x
                                          //             number of parameter on axis y
                                          //             (0:linearis scale, 1:logarithmic scale) on axis x
    // @author Daniel Herman                                     //             (0:linearis scale, 1:logarithmic scale) on axis y
    private PlotList[][][][] activatedPlotCache;
    //--
    // @author Daniel Herman: caches of gated data for creating histograms, scatter plots
    private double[] minValuesGated;
    private double[] minValuesGreaterThan0Gated;
    private double[] maxValuesGated;
    private double[] minLogValuesGated;
    private double[] maxLogValuesGated;
    private HistogramList[][] gatedHistoCache; // dimensions: number of parameter, (0:linearis scale, 1:logarithmic scale)
    private PlotList[][][][] gatedPlotCache;
    //--
    
    // metadata
    private String[] paramNames;
    private String fcsVersion;
    private String measurementTime = "";
    private String gateDescription;
    private String appendDescription = "";
    private boolean[] logSuggested;
    private int timeParamNumber;
    private boolean isTimeInSeconds = false;
    // @author Daniel Herman
    private double timeSlice;
    //--
    public FcsData(File file, TaskWithProgress task) throws IOException {
        openFile(file, task);
    }

    //------------------------------------------------
    // Private helper functions
    //------------------------------------------------

    private void openFile(File file, TaskWithProgress task) throws IOException {
        // We save file (clearGates could re-read it if we hadn't stored it in firstData)
        this.file = file;

        // read FCS
        FcsOpener fo = new FcsOpener(new FileInputStream(file), (int) file.length() / 3, task);
        data = fo.getData();

        // getting parameter names and some other metadata from keywords in FCS
        if (fo.getKeywordValue("$DATE") != null)
            measurementTime = fo.getKeywordValue("$DATE");
        if (fo.getKeywordValue("$BTIM") != null)
            if (measurementTime.equals(""))
                measurementTime += fo.getKeywordValue("$BTIM");
            else
                measurementTime += " " + fo.getKeywordValue("$BTIM");
        paramCount = fo.getParameterCount();
        paramNames = new String[paramCount];
        for (int i = 0; i < paramCount; i ++)
            paramNames[i] = fo.getParamName(i);
        fcsVersion = fo.getFCSVersion();

        // looking for a time parameter
        timeParamNumber = -1;
        for (int i = 0; i < paramCount && timeParamNumber == -1; i ++)
            if (paramNames[i].equalsIgnoreCase("Time"))
                timeParamNumber = i;

        if (timeParamNumber == -1) {
            for (int i = 0; i < paramCount && timeParamNumber == -1; i ++)
                if (paramNames[i].equalsIgnoreCase("HDR-T"))
                    timeParamNumber = i;
            if (timeParamNumber != -1)
                isTimeInSeconds = true;
        }

        if (timeParamNumber != -1) {
            // setting time multiplication
            double timeSlice;
	    System.out.println("version: " + fcsVersion);
            if (fcsVersion == "3.0" || fcsVersion == "3.1") {
                if (fo.getKeywordValue("$TIMESTEP") != null) {
                    timeSlice = Double.parseDouble(fo.getKeywordValue("$TIMESTEP"));
                    isTimeInSeconds = true;
		    System.out.println("OK");
                } else {
                    timeSlice = Double.parseDouble(fo.getKeywordValue("TIMETICKS")) / 1000;
                    isTimeInSeconds = true;
                }

		if ((! (fo.getKeywordValue("$BTIM") == null || fo.getKeywordValue("$ETIM") == null)) && 
                        ! fo.getKeywordValue("$BTIM").equals(fo.getKeywordValue("$ETIM"))) {
		    String[] btims = fo.getKeywordValue("$BTIM").split(":");
		    String[] etims = fo.getKeywordValue("$ETIM").split(":");
                    double btim = Integer.parseInt(btims[0])*60*60 + Integer.parseInt(btims[1]) * 60 + Double.parseDouble(btims[2]);
                    double etim = Integer.parseInt(etims[0])*60*60 + Integer.parseInt(etims[1]) * 60 + Double.parseDouble(etims[2]);
		    double secs = etim - btim;
		    if (etim < btim) secs = etim + 24*60*60-btim;
		    double lastEvent[] = data.get(data.size()-1);
		    timeSlice = secs / lastEvent[timeParamNumber];
		}
		
            } else {
                if (fo.getKeywordValue("TIMETICKS") == null) {
                    if (fo.getKeywordValue("ACQTIME") == null
                     && fo.getKeywordValue("P$ACQTIMES") == null) {
                        timeSlice = 1.0;
                    } else {
                        double maxTime = 0;
                        for (int i = 0; i < data.size(); i ++) {
                            if (data.get(i)[timeParamNumber] > maxTime)
                                maxTime = data.get(i)[timeParamNumber];
                        }
                        if (maxTime > 0) {
                            if (fo.getKeywordValue("ACQTIME") == null)
                                timeSlice = Double.parseDouble(fo.getKeywordValue("P$ACQTIMES")) / maxTime;
                            else
                                timeSlice = Double.parseDouble(fo.getKeywordValue("ACQTIME")) / maxTime;                            
                        } else
                            timeSlice = 1.0;
                        //System.out.println("Maxtime: " + String.valueOf(maxTime) + ", timeslice: " + String.valueOf(timeSlice));
                    }
                } else {
                    timeSlice = Double.parseDouble(fo.getKeywordValue("TIMETICKS")) / 1000;
                    isTimeInSeconds = true;
                }
            }
            // converting time to seconds
            if (timeSlice != 1.0)
                for (int i = 0; i < data.size(); i ++) {
                    double event[] = data.get(i);
                    event[timeParamNumber] = event[timeParamNumber] * timeSlice;
                    data.set(i, event);
                }
            this.timeSlice = timeSlice;
                    //System.out.println("Timeslice: " + String.valueOf(timeSlice));
        } else
            isTimeInSeconds = false;

        // setting log suggested values
        logSuggested = new boolean[getParameterCount()];
        for (int i = 0; i < getParameterCount(); i ++) {
            logSuggested[i] = false;
            if (fo.getKeywordValue("$P" + (i + 1) + "E") != null)
                if (Double.parseDouble(fo.getKeywordValue(
                    "$P" + (i + 1) + "E").split(",")[0]) != 0)
                    logSuggested[i] = true;
            if (fo.getKeywordValue("P" + (i + 1) + "DISPLAY") != null)
                if (fo.getKeywordValue("P" + (i + 1) + "DISPLAY").equals("LOG")) {
                    logSuggested[i] = true;
                } else {
                    if (fo.getKeywordValue("P" + (i + 1) + "DISPLAY").equals("LIN"))
                        logSuggested[i] = false;
                }
        }
        logSuggested[timeParamNumber] = false;

        gateDescription = "";
        processStatistic();

        //@author Daniel Herman
        gatedData = null;
        //gateDescription = gateDescription.concat("Actual: " + getEventCount() + " events\n");
        //--
        
        firstData = data;
    }

    /**
     * After gating/clearing gates we have to recreate statistical data.
     * Histograms are recalculated, but we don't recalculate all scatter plots,
     * because it is possible that we won't need them.
     */
    // @author Daniel Herman: change to public from private
    public void processStatistic() {
        // max-min
        minValues = new double[getParameterCount()];
        minValuesGreaterThan0 = new double[getParameterCount()];
        maxValues = new double[getParameterCount()];
        for (int i = 0; i < getParameterCount(); i ++) {
            minValues[i] = Double.MAX_VALUE;
            minValuesGreaterThan0[i] = Double.MAX_VALUE;
            maxValues[i] = Double.MIN_VALUE;
        }
        for (int i = 0; i < getEventCount(); i ++) {
            for (int j = 0; j < getParameterCount(); j ++) {
                if (data.get(i)[j] < minValues[j]) minValues[j] = data.get(i)[j];
                if (data.get(i)[j] > 0.0 && data.get(i)[j] < minValuesGreaterThan0[j])
                    minValuesGreaterThan0[j] = data.get(i)[j];
                if (data.get(i)[j] > maxValues[j]) maxValues[j] = data.get(i)[j];
            }
        }
        minLogValues = new double[getParameterCount()];
        maxLogValues = new double[getParameterCount()];
        for (int i = 0; i < getParameterCount(); i ++) {
            minLogValues[i] = logarithmize(minValuesGreaterThan0[i], 1, i);
            maxLogValues[i] = logarithmize(maxValues[i], 1, i);
        }

        // histoCache
        int scale = getHistogramScale();
        double[][] segmentSize = new double[getParameterCount()][2];
        histoCache = new HistogramList[getParameterCount()][2];
        for (int i = 0; i < getParameterCount(); i ++)
            for (int j = 0; j < 2; j ++) {
                boolean log;
                if (j == 0) log = false; else log = true;
                segmentSize[i][j] = (getMaxValues(log)[i] - getMinValues(log)[i]) / scale;
                histoCache[i][j] = new HistogramList(scale, getMinValues(log)[i], getMaxValues(log)[i]);
            }
        for (int i = 0; i < getEventCount(); i ++) {
            for (int j = 0; j < getParameterCount(); j ++) {
                int key;

                // (1) histocache[*][0]: non-logarithmized data
                key = (int) Math.floor((logarithmize(data.get(i)[j], 0, j) - getMinValues(false)[j]) / segmentSize[j][0]);
                if (key == scale) key --;
                int value = histoCache[j][0].get(key);
                histoCache[j][0].set(key, ++ value);
                
                // (2) histocache[*][1]: logarithmized data
                key = (int) Math.floor((logarithmize(data.get(i)[j], 1, j) - getMinValues(true)[j]) / segmentSize[j][1]);
                if (key == scale) key --;
                value = histoCache[j][1].get(key);
                histoCache[j][1].set(key, ++ value);
            }
        }

        // plotCache
        plotCache = new PlotList[getParameterCount()][getParameterCount()][2][2];
        //@author Daniel Herman
        activatedPlotCache = new PlotList[getParameterCount()][getParameterCount()][2][2];
        setGateDescription();
        //gateDescription = "Actual: " + getEventCount() + " events\n";
        //--
    }
    
    /**
     * @author Daniel Herman
     * Statistical data of gated data in order to plot the gated data within the all data.  
     */
    private void processGatedStatistic() {
        // max-min
        minValuesGated = new double[getParameterCount()];
        minValuesGreaterThan0Gated = new double[getParameterCount()];
        maxValuesGated = new double[getParameterCount()];
        for (int i = 0; i < getParameterCount(); i ++) {
            minValuesGated[i] = Double.MAX_VALUE;
            minValuesGreaterThan0Gated[i] = Double.MAX_VALUE;
            maxValuesGated[i] = Double.MIN_VALUE;
        }
        for (int i = 0; i < getGatedEventCount(); i ++) {
            for (int j = 0; j < getParameterCount(); j ++) {
                if (gatedData.get(i)[j] < minValuesGated[j]) minValuesGated[j] = gatedData.get(i)[j];
                if (gatedData.get(i)[j] > 0.0 && gatedData.get(i)[j] < minValuesGreaterThan0Gated[j])
                    minValuesGreaterThan0[j] = data.get(i)[j];
                if (gatedData.get(i)[j] > maxValuesGated[j]) maxValuesGated[j] = gatedData.get(i)[j];
            }
        }
        minLogValuesGated = new double[getParameterCount()];
        maxLogValuesGated = new double[getParameterCount()];
        for (int i = 0; i < getParameterCount(); i ++) {
            minLogValuesGated[i] = logarithmize(minValuesGreaterThan0Gated[i], 1, i);
            maxLogValuesGated[i] = logarithmize(maxValuesGated[i], 1, i);
        }

        // histoCache
        /*
        int scale = getHistogramScale();
        double[][] segmentSize = new double[getParameterCount()][2];
        gatedHistoCache = new HistogramList[getParameterCount()][2];
        for (int i = 0; i < getParameterCount(); i ++)
            for (int j = 0; j < 2; j ++) {
                boolean log;
                if (j == 0) log = false; else log = true;
                segmentSize[i][j] = (getMaxValues(log)[i] - getMinValues(log)[i]) / scale;
                gatedHistoCache[i][j] = new HistogramList(scale, getMinValues(log)[i], getMaxValues(log)[i]);
            }
        for (int i = 0; i < getEventCount(); i ++) {
            for (int j = 0; j < getParameterCount(); j ++) {
                int key;

                // (1) histocache[*][0]: non-logarithmized data
                key = (int) Math.floor((logarithmize(data.get(i)[j], 0, j) - getMinValues(false)[j]) / segmentSize[j][0]);
                if (key == scale) key --;
                int value = histoCache[j][0].get(key);
                histoCache[j][0].set(key, ++ value);

                // (2) histocache[*][1]: logarithmized data
                key = (int) Math.floor((logarithmize(data.get(i)[j], 1, j) - getMinValues(true)[j]) / segmentSize[j][1]);
                if (key == scale) key --;
                value = histoCache[j][1].get(key);
                histoCache[j][1].set(key, ++ value);
            }
        }
        */
        // plotCache
        gatedPlotCache = new PlotList[getParameterCount()][getParameterCount()][2][2];
    }

    private boolean inside_poly(double point1, double point2, double[] points1, double[] points2) {
        boolean ret = false;
        int j = 0;
        int n = points1.length;
        for (int i = 0; i < n; i ++) {
            j ++;
            if (j == n)
                j = 0;
            if ((points2[i] < point2 && points2[j] >= point2) || (points2[j] < point2 && points2[i] >= point2)) {
                if (points1[i]+(point2-points2[i])/(points2[j]-points2[i])*(points1[j]-points1[i])<point1) {
                    ret=!ret;
                }
            }
        }
        return ret;
    }

    private double logarithmize(double x, int log, int param) { //log= 0:nincs, 1:log, 2:hyperlog
        if (log == 0)
            return x;
        else if (log == 1)
            return x > 0.0 ? Math.log10(x) : minLogValues[param];
        else { // log == 2
            double a = 1;
            double b = 1;
            double c = 0;
            x = a + b * x;
            //inverse of (generalized) sinh
            return Math.log(x + Math.sqrt(x*x + 1)) + c;
        }
    }

    //------------------------------------------------
    // Data query functions
    //------------------------------------------------

    public int getEventCount() {
        return data.size();
    }
    
    //@author Daniel Herman
    public int getGatedEventCount() {
        return gatedData.size();
    }
    public void setGateDescription() {
        gateDescription = "Actual: " + getEventCount() + " events\n";
    }
    //--
    
    public double[] getEvent(int i) {
        return data.get(i);
    }

    //------------------------------------------------
    // Metadata query functions
    //------------------------------------------------

    public String getFilename() {
            return file.getName();
    }

    public String getMeasurementTime() {
        return measurementTime;
    }

    public String getGateDescription() {
        return gateDescription;
    }

    public String getDescription() {
        String description = "Filename: " + getFilename() + "\n" +
                "FCS version " + fcsVersion + "\n" +
                "Measurement date:\n  " + measurementTime +
                "\n\n" + appendDescription + "\n" + gateDescription;
        return description;
    }

    public String getParameterName(int i) {
        return paramNames[i];
    }

    public int getParameterCount() {
        return paramCount;
    }
    
    
    //------------------------------------------------
    // Statistical query functions
    //------------------------------------------------

    public int getHistogramScale() {
        return 75;
    }

    public HistogramList getHistogramData(int p, boolean log) {
        if (p > getParameterCount())
            return null;
        if (log)
            return histoCache[p][1];
        else
            return histoCache[p][0];
    }

    public int getPlotScale() {
        return 100;
    }

    public PlotList getPlotData(int p1, int p2, boolean log1, boolean log2) {
        if (p1 >= getParameterCount() || p2 >= getParameterCount())
            return null;
        int l1, l2;
        if (log1) l1 = 1; else l1 = 0;
        if (log2) l2 = 1; else l2 = 0;
        PlotList plotList = plotCache[p1][p2][l1][l2];
        if (plotList == null) {
            plotList = new PlotList();
            int scaleX = getPlotScale();
            int scaleY = getPlotScale();
            for (int x = 0; x < scaleX; x ++)
                for (int y = 0; y < scaleY; y ++)
                    plotList.put(x, y, 0);

            double[] segmentSize = new double[getParameterCount()];

            segmentSize[p1] = (getMaxValues(log1)[p1] - getMinValues(log1)[p1]) / scaleX;
            segmentSize[p2] = (getMaxValues(log2)[p2] - getMinValues(log2)[p2]) / scaleY;

            for (int i = 0; i < getEventCount(); i ++) {
                int keyX, keyY;

                //X tengelyen levo adat
                keyX = (int) Math.floor((logarithmize(data.get(i)[p1], log1 ? 1 : 0, p1) - getMinValues(log1)[p1]) / segmentSize[p1]);
                if (keyX == scaleX)
                    keyX --;

                //Y tengelyen levo adat
                keyY = (int) Math.floor((logarithmize(data.get(i)[p2], log2 ? 1 : 0, p2) - getMinValues(log2)[p2]) / segmentSize[p2]);
                if (keyY == scaleY)
                    keyY --;
                int value = plotList.get(keyX, keyY);
                plotList.put(keyX, keyY, value + 1);
            }
            plotCache[p1][p2][l1][l2] = plotList;
        }
        return plotList;
    }

    public double[] getMaxValues(boolean log) {
        if (log)
            return maxLogValues;
        else
            return maxValues;
    }

    public double[] getMinValues(boolean log) {
        if (log)
            return minLogValues;
        else
            return minValues;
    }

    public int getTimeParamNumber() {
        return timeParamNumber; //this may be -1
    }

    public boolean isTimeInSeconds() {
        return isTimeInSeconds;
    }

    public boolean[] logSuggested() {
        return logSuggested;
    }
    
    //@author Daniel Herman: calculate activated ratio
    public double getTimeDependenceRatio(int p1,int p2, boolean log1, boolean log2, int baselineLength) {
        double activatedRatio = 0.0;

        //search max time & value
        double maxTime = (double)baselineLength;
        double maxValue = 0.0;
        for (int i = 0; i < getEventCount(); i ++) {
            if(data.get(i)[timeParamNumber] > maxTime) {
                maxTime = data.get(i)[timeParamNumber];
            }
            if(data.get(i)[p2] > maxValue) {
                maxValue = data.get(i)[p2];
            }
        }
        
        //search min time & value
        double minTime = maxTime;
        double minValue = maxValue;
        for (int i = 0; i < getEventCount(); i ++) {
            if(data.get(i)[timeParamNumber] < minTime) {
                minTime = data.get(i)[timeParamNumber];
            }
            if(data.get(i)[p2] < minValue) {
                minValue = data.get(i)[p2];
            }
        }

        if(!isTimeInSeconds()) {
            System.out.println("Time is not in seconds!");
        }
        if(getTimeParamNumber() == -1) {
            System.out.println("There is no time parameter!");
            return activatedRatio;
        }
        
        int l1, l2;
        if (log1) l1 = 1; else l1 = 0;
        if (log2) l2 = 1; else l2 = 0;
        
        PlotList plotList = new PlotList();
        
        //baseline distribution
        int[] baselineDistribution = new int[getPlotScale()];
        double gap = (maxValue - minValue) / (double)(getPlotScale()-1);      
        
        for(int i = 0; data.get(i)[timeParamNumber] <= minTime + (double)baselineLength; i++) {
            baselineDistribution[(int)( (data.get(i)[p2]-minValue)/gap )]++;
        }
        int eventCountBaseline = 0;
        for (int i=0; i < getPlotScale(); i++) {
            eventCountBaseline += baselineDistribution[i];
        }
        
        //data distribution
        int[][] dataDistribution = new int[getPlotScale()][getPlotScale()];
        double timeGap = (maxTime - minTime) / (double)(getPlotScale()-1);
        
        for (int i=0; i < data.size(); i++) {
            dataDistribution[(int)( (data.get(i)[timeParamNumber]-minTime)/timeGap )][(int)( (data.get(i)[p2]-minValue)/gap )]++;
        }
        
        int[] eventCountDataDistribution = new int[getPlotScale()];
        for (int i=0; i < getPlotScale(); i++) {
            int eventCountTimeslot = 0;
            for (int j=0; j < getPlotScale(); j++) {
                eventCountTimeslot += dataDistribution[i][j];
            }
            eventCountDataDistribution[i] = eventCountTimeslot;
        }
        
        //extrapolated distribution
        int[][] extrapolatedDistribution = new int[getPlotScale()][getPlotScale()];
        for (int i=0; i < getPlotScale(); i++) {
            for (int j=0; j < getPlotScale(); j++) {
                extrapolatedDistribution[i][j] = (int) (baselineDistribution[j] * ((double)eventCountDataDistribution[i]/(double)eventCountBaseline));
            }
        }
        
        //substraction of data and extrapolated distributions
        int sumActivatedCells = 0;
        int events = 0;
        double maxActivatedRatio = 0.0;
            
        for (int i=0; i < getPlotScale(); i++) {
            int activatedCellsInRange = 0;
            int cellsInRange = 0;
            for (int j=0; j < getPlotScale(); j++) {
                int activatedCells = Math.max(0,dataDistribution[i][j] - extrapolatedDistribution[i][j]);
                activatedCellsInRange += activatedCells;
                sumActivatedCells += activatedCells;
                plotList.put(i, j, activatedCells);
                events += dataDistribution[i][j];
                cellsInRange += dataDistribution[i][j];
            }
            if(maxActivatedRatio < Math.round(((double)activatedCellsInRange/(double)cellsInRange)*10000.0)/100.0) {
                maxActivatedRatio = Math.round(((double)activatedCellsInRange/(double)cellsInRange)*10000.0)/100.0;
            }
        }
        
        activatedPlotCache[p1][p2][l1][l2] = plotList;
        activatedRatio = Math.round(((double)sumActivatedCells/(double)events)*10000.0)/100.0;
        //return activatedRatio;
        return maxActivatedRatio;
    }
    
    public void timeUniformization(TaskWithProgress task) {
        double maxTime = 0.0;
        double minTime = data.get(0)[timeParamNumber];
        for (int i = 0; i < getEventCount(); i ++) {
            if(data.get(i)[timeParamNumber] > maxTime) {
                maxTime = data.get(i)[timeParamNumber];
            }
            if(data.get(i)[timeParamNumber] < minTime) {
                minTime = data.get(i)[timeParamNumber];
            }
        }
        System.out.println("min: " + String.valueOf(maxTime));
        System.out.println("max: " + String.valueOf(maxTime));
        double timeUnit = (maxTime-minTime)/data.size();
        for (int i = 0; i < getEventCount(); i ++) {
            data.get(i)[timeParamNumber] = minTime + i*timeUnit;
            if (i % Math.floor(data.size() / 100) == 0 && task != null)
                task.setProgressValue((float) i / data.size());
        }
    }
    //--

    //------------------------------------------------
    // Data change (gate, append) functions
    //------------------------------------------------

    public GateData histGate(int param, double min, double max, boolean log, TaskWithProgress task) {
        //@author Daniel Herman
        double[] points1 = new double[1];
        points1[0] = min;
        double[] points2 = new double[1];
        points2[0] = max;
        //--
        ArrayList<double[]> newData = new ArrayList<double[]>();
        for (int i = 0; i < data.size(); i ++) {
            if (logarithmize(data.get(i)[param], log ? 1 : 0, param) > min &&
                    logarithmize(data.get(i)[param], log ? 1 : 0, param) < max)
                newData.add(data.get(i));
            if (i % Math.floor(data.size() / 100) == 0 && task != null)
                task.setProgressValue((float) i / data.size());
        }
        /*data = newData;
        gateDescription = gateDescription.concat("  " + paramNames[param] + " gate\n");
        processStatistic();*/
        //@author Daniel Herman
        String newGateDescription = "<html>Histogram " + (paramNames[param].length()>11 ? paramNames[param].substring(0,11) : paramNames[param]) +
                "<br/>";   
        //processStatistic();
        //newGateDescription = newGateDescription.concat(newData.size() + " events</html>");
        
        return new GateData(param, 0, points1, points2, log, false, newGateDescription);
        //--
    }

    public GateData inverseHistGate(int param, double min, double max, boolean log, TaskWithProgress task) {
        //@author Daniel Herman
        double[] points1 = new double[1];
        points1[0] = min;
        double[] points2 = new double[1];
        points2[0] = max;
        //--
        ArrayList<double[]> newData = new ArrayList<double[]>();
        for (int i = 0; i < data.size(); i ++) {
            if (!(logarithmize(data.get(i)[param], log ? 1 : 0, param) > min &&
                    logarithmize(data.get(i)[param], log ? 1 : 0, param) < max))
                newData.add(data.get(i));
            if (i % Math.floor(data.size() / 100) == 0 && task != null)
                task.setProgressValue((float) i / data.size());
        }
        /*data = newData;
        gateDescription = gateDescription.concat("  " + paramNames[param] + " gate\n");
        processStatistic();*/
        //@author Daniel Herman
        String newGateDescription = "<html>Histogram Inv " + (paramNames[param].length()>11 ? paramNames[param].substring(0,11) : paramNames[param]) +
                "<br/>";   
        //processStatistic();
        //newGateDescription = newGateDescription.concat(newData.size() + " events</html>");
        
        return new GateData(param, 0, points1, points2, log, false, newGateDescription);
        //--
    }

    //@author Daniel Herman
    public void plotGates(int param1, int param2, double[] points1, double[] points2, boolean log1, boolean log2, TaskWithProgress task) {
        ArrayList<double[]> newData = new ArrayList<double[]>();
        
        for (int i = 0; i < data.size(); i ++) {
            if (inside_poly(
                    logarithmize(data.get(i)[param1], log1 ? 1 : 0, param1),
                    logarithmize(data.get(i)[param2], log2 ? 1 : 0, param2),
                    points1, points2))
                newData.add(data.get(i));
            if (i % Math.floor(data.size() / 100) == 0 && task != null)
                task.setProgressValue((float) i / data.size());
        }
        data = newData;
    }
    public void inversePlotGates(int param1, int param2, double[] points1, double[] points2, boolean log1, boolean log2, TaskWithProgress task) {
        ArrayList<double[]> newData = new ArrayList<double[]>();
        for (int i = 0; i < data.size(); i ++) {
            if (!inside_poly(
                    logarithmize(data.get(i)[param1], log1 ? 1 : 0, param1),
                    logarithmize(data.get(i)[param2], log2 ? 1 : 0, param2),
                    points1, points2))
                newData.add(data.get(i));
            if (i % Math.floor(data.size() / 100) == 0 && task != null)
                task.setProgressValue((float) i / data.size());
        }
        data = newData;
    }
    
    public void histGates(int param1, int param2, double[] points1, double[] points2, boolean log1, boolean log2, TaskWithProgress task) {
        double min = points1[0];
        double max = points2[0];
        ArrayList<double[]> newData = new ArrayList<double[]>();
        for (int i = 0; i < data.size(); i ++) {
            if ((logarithmize(data.get(i)[param1], log1 ? 1 : 0, param1) > min &&
                    logarithmize(data.get(i)[param1], log1 ? 1 : 0, param1) < max))
                newData.add(data.get(i));
            if (i % Math.floor(data.size() / 100) == 0 && task != null)
                task.setProgressValue((float) i / data.size());
        }
        data = newData;
    }
    
    public void inverseHistGates(int param1, int param2, double[] points1, double[] points2, boolean log1, boolean log2, TaskWithProgress task) {
        double min = points1[0];
        double max = points2[0];
        ArrayList<double[]> newData = new ArrayList<double[]>();
        for (int i = 0; i < data.size(); i ++) {
            if (!(logarithmize(data.get(i)[param1], log1 ? 1 : 0, param1) > min &&
                    logarithmize(data.get(i)[param1], log1 ? 1 : 0, param1) < max))
                newData.add(data.get(i));
            if (i % Math.floor(data.size() / 100) == 0 && task != null)
                task.setProgressValue((float) i / data.size());
        }
        data = newData;
    }

    public void paintGate(boolean inverse, int param1, int param2, double[] points1, double[] points2, boolean log1, boolean log2) {
        
        ArrayList<double[]> newData = new ArrayList<double[]>();
        
        for (int i = 0; i < data.size(); i ++) {
            if(inverse) {
                if (!inside_poly(
                        logarithmize(data.get(i)[param1], log1 ? 1 : 0, param1),
                        logarithmize(data.get(i)[param2], log2 ? 1 : 0, param2),
                        points1, points2))
                    newData.add(data.get(i));
            }
            else {
                if (inside_poly(
                    logarithmize(data.get(i)[param1], log1 ? 1 : 0, param1),
                    logarithmize(data.get(i)[param2], log2 ? 1 : 0, param2),
                    points1, points2))
                newData.add(data.get(i));
            }
        }
        gatedData = newData;
        processGatedStatistic();
    }
    
    public boolean isNullgatedData() {
        if(gatedData == null) {
            return true;
        }
        return false;
    }
    
    public void deleteGatedData() {
        gatedData = null;
    }
    
    public PlotList getActivatedPlotData(int p1, int p2, boolean log1, boolean log2) {
        int l1, l2;
        if (log1) l1 = 1; else l1 = 0;
        if (log2) l2 = 1; else l2 = 0;
        return activatedPlotCache[p1][p2][l1][l2];
    }
    
    public PlotList getGatedPlotData(int p1, int p2, boolean log1, boolean log2) {
        if(gatedData == null)
            return null;
        
        if (p1 >= getParameterCount() || p2 >= getParameterCount())
            return null;
        int l1, l2;
        if (log1) l1 = 1; else l1 = 0;
        if (log2) l2 = 1; else l2 = 0;
        PlotList plotList = gatedPlotCache[p1][p2][l1][l2];
        if (plotList == null) {
            plotList = new PlotList();
            int scaleX = getPlotScale();
            int scaleY = getPlotScale();
            for (int x = 0; x < scaleX; x ++)
                for (int y = 0; y < scaleY; y ++)
                    plotList.put(x, y, 0);

            double[] segmentSize = new double[getParameterCount()];

            segmentSize[p1] = (getMaxValues(log1)[p1] - getMinValues(log1)[p1]) / scaleX;
            segmentSize[p2] = (getMaxValues(log2)[p2] - getMinValues(log2)[p2]) / scaleY;

            for (int i = 0; i < getGatedEventCount(); i ++) {
                int keyX, keyY;

                //X tengelyen levo adat
                keyX = (int) Math.floor((logarithmize(gatedData.get(i)[p1], log1 ? 1 : 0, p1) - getMinValues(log1)[p1]) / segmentSize[p1]);
                if (keyX == scaleX)
                    keyX --;

                //Y tengelyen levo adat
                keyY = (int) Math.floor((logarithmize(gatedData.get(i)[p2], log2 ? 1 : 0, p2) - getMinValues(log2)[p2]) / segmentSize[p2]);
                if (keyY == scaleY)
                    keyY --;
                int value = plotList.get(keyX, keyY);
                plotList.put(keyX, keyY, value + 1);
            }
            gatedPlotCache[p1][p2][l1][l2] = plotList;
        }
        return plotList;
    }
    //--
    
    public GateData plotGate(int param1, int param2, double[] points1, double[] points2, boolean log1, boolean log2, TaskWithProgress task) {
        ArrayList<double[]> newData = new ArrayList<double[]>();
        
        for (int i = 0; i < data.size(); i ++) {
            if (inside_poly(
                    logarithmize(data.get(i)[param1], log1 ? 1 : 0, param1),
                    logarithmize(data.get(i)[param2], log2 ? 1 : 0, param2),
                    points1, points2))
                newData.add(data.get(i));
            if (i % Math.floor(data.size() / 100) == 0 && task != null)
                task.setProgressValue((float) i / data.size());
        }
        
        //@author Daniel Herman
        String newGateDescription = "<html>" + (paramNames[param1].length()>11 ? paramNames[param1].substring(0,11) : paramNames[param1]) +
                " and " + (paramNames[param2].length()>11 ? paramNames[param2].substring(0,11) : paramNames[param2]) + "<br/>";   
        //processStatistic();
        //newGateDescription = newGateDescription.concat(newData.size() + " events</html>");
        return new GateData(param1, param2, points1, points2, log1, log2, newGateDescription);
        //--
    }

    public GateData inversePlotGate(int param1, int param2, double[] points1, double[] points2, boolean log1, boolean log2, TaskWithProgress task) {
        ArrayList<double[]> newData = new ArrayList<double[]>();
        
        for (int i = 0; i < data.size(); i ++) {
            if (!inside_poly(
                    logarithmize(data.get(i)[param1], log1 ? 1 : 0, param1),
                    logarithmize(data.get(i)[param2], log2 ? 1 : 0, param2),
                    points1, points2))
                newData.add(data.get(i));
            if (i % Math.floor(data.size() / 100) == 0 && task != null)
                task.setProgressValue((float) i / data.size());
        }

        //@author: Daniel Herman
        String newGateDescription = "<html>Inv " + (paramNames[param1].length()>11 ? paramNames[param1].substring(0,11) : paramNames[param1]) +
                " and " + (paramNames[param2].length()>11 ? paramNames[param2].substring(0,11) : paramNames[param2]) + "<br/>";   
        //processStatistic();
        //newGateDescription = newGateDescription.concat(newData.size() + " events</html>");
        return new GateData(param1, param2, points1, points2, log1, log2, newGateDescription);
        //--
    }

    public void clearGates(TaskWithProgress task) {
        data = firstData;  
        //processStatistic();
    }

    public void append(FcsData plusData) throws Exception {
        if (plusData.paramCount != paramCount)
            throw new IOException("parameter count differs");
	int[] indices = new int[paramCount]; // indices[i] is the index where we find parameter i in plusData
        for (int i = 0; i < paramCount; ++i) {
	    // i: parameter in data (paramNames)
	    indices[i] = 0;
	    while (indices[i] < paramCount && !plusData.paramNames[indices[i]].equals(paramNames[i]))
		indices[i]++;
	    if (indices[i] == paramCount)
                throw new IOException("parameter " + paramNames[i] + " can't be found in the appended FCS file");
	}
        double timeToAdd = getMaxValues(false)[timeParamNumber] + FacsKinApp.getGapBeforeAppend();
        for (int i = 0; i < plusData.data.size(); i++) {
            double[] event_orig = plusData.data.get(i);
	    double[] event = new double[paramCount];
	    for (int j = 0; j < paramCount; j++)
		event[j] = event_orig[indices[j]];
            event[timeParamNumber] += timeToAdd;
            data.add(event);
        }

        appendDescription = appendDescription.concat("  "+plusData.getFilename()+" file appended with "+timeToAdd+" s shift\n");
        processStatistic();
    }
}
