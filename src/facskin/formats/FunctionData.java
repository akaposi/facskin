package facskin.formats;

import facskin.math.Functions;
import java.util.ArrayList;

/**
 * Class representing one function and it's distributions of parameters. It
 * also has methods for standardizing it's parameters.
 * @author ambi
 */
public class FunctionData {
    private String name;
    private double cv_value;
    private double sad;
    private ArrayList<ParameterData> parameterDatas = new ArrayList<ParameterData>();
    private Double[] medianParams = null;

    private double maxTime;

    public FunctionData(String name, double cv_value, double sad, double[][] par, double maxTime) {
        this.name = name;
        this.cv_value = cv_value;
        this.sad = sad;

        for (int param = 0; param < par[0].length; param ++) {
            ParameterData pd = new ParameterData(Functions.getParameterName(name, param), par.length);
            for (int i = 0; i < par.length; i ++)
                pd.setData(i, par[i][param]);
            parameterDatas.add(pd);
        }
        int param = par[0].length;
        ParameterData pd = new ParameterData(Functions.getParameterName(name, param), par.length);
        for (int i = 0; i < par.length; i ++)
            pd.setData(i, Functions.getAUC(name, par[i], maxTime));
        parameterDatas.add(pd);
    }

    public String getName() {
        return name;
    }

    public double get_cv_value() {
        return cv_value;
    }

    public double get_sad() {
        return sad;
    }

    /**
     * This calls {@link facskin.math.Functions}'s paramsToStandardize() function
     * to know which parameters to standardize.
     * @param standardizer
     */
    public void setStandardizer(double standardizer) {
        int[] paramsToStandardize = Functions.paramsToStandardize(name);
        for (int i = 0; i < paramsToStandardize.length; i ++) {
            parameterDatas.get(paramsToStandardize[i]).setStandardizer(standardizer);
        }
        parameterDatas.get(parameterDatas.size() - 1).setStandardizer(standardizer); //ez az AUC
        medianParams = null;
    }

    public void setMaxTime(double maxTime) {
        if (this.maxTime != maxTime) {
            this.maxTime = maxTime;
            ParameterData pd = parameterDatas.get(parameterDatas.size() - 1);
            //(kihagyjuk az utolsot, AUC-t)
            for (int i = 0; i < pd.getDataLength(); i ++) {
                double par[] = new double[parameterDatas.size() - 1];
                for (int j = 0; j < par.length; j ++)
                    par[j] = parameterDatas.get(j).getDataWithoutStandardization(i);
                pd.setData(i, Functions.getAUC(name, par, maxTime));
            }
        }
    }

    public Double[] getMedianParams() {
        if (medianParams == null) {
            medianParams = new Double[parameterDatas.size()];
            for (int i = 0; i < parameterDatas.size(); i++)
                medianParams[i] = parameterDatas.get(i).getQuantile(0.5);        
        }
        return medianParams;
    }

    public int getParamCount() {
        return parameterDatas.size();
    }

    /**
     * @return The parameter count without AUC.
     */
    public int getRealParamCount() {
        return parameterDatas.size() - 1;
    }

    public ParameterData getParam(int i) {
        return parameterDatas.get(i);
    }
}
