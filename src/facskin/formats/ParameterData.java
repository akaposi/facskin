package facskin.formats;

import facskin.math.Statistics;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Class for representing a distribution for one parameter. In addition to
 * {@link Data} methods it implements setter and standardization methods.
 * @author ambi
 */
public class ParameterData implements Data {
    private String name;
    private double[] data;
    private Map<Double, Double> quantiles = null;
    private double standardizer = 1;

    public ParameterData(String name, int length) {
        this.name = name;
        data = new double[length];
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String newName) {
        name = newName;
    }

    @Override
    public int getDataLength() {
       return data.length;
    }

    @Override
    public double getData(int i) {
        return data[i] / standardizer;
    }

    @Override
    public double getMidData() {
        return data[(int) Math.round(getDataLength() * 0.5 - 0.5)] / standardizer;
    }

    public double getDataWithoutStandardization(int i) {
        return data[i];
    }

    public void setData(int i, double value) {
        //standardizalasra vigyazni kell! azt nem veszi figyelembe data beallitasanal
        data[i] = value;
        quantiles = null;
    }

    public void setStandardizer(double standardizer) {
        this.standardizer = standardizer;
        quantiles = null;
    }

    @Override
    public double getQuantile(Double q) {
        if (quantiles == null)
            quantiles = new HashMap<Double, Double>();
        if (!quantiles.containsKey(q)) {
            ArrayList<Double> params = new ArrayList<Double>();
            for (int i = 0; i < data.length; i ++)
                params.add(getData(i));
            double quant = Statistics.quantile(params, q);
            quantiles.put(q, quant);
        }
        return quantiles.get(q);
    }
}
