package facskin.formats;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class representing data for comparison. It implements {@link Data} (that is,
 * query methods) and it has methods for adding more data.
 * @author ambi
 */
public class ComparisonData implements Data {
    private String name;
    private List<Double> data = new ArrayList<Double>();
    private Map<Double, Double> quantiles;

    public ComparisonData(String name) {
        this.name = name;
        quantiles = new HashMap<Double, Double>();
    }

    public void addData(Data d) {
        for (int i = 0; i < d.getDataLength(); i ++)
            data.add(d.getData(i));
        Collections.sort(data);
        quantiles = new HashMap<Double, Double>();
    }

    public void addData(double d) {
        data.add(d);
        Collections.sort(data);
        quantiles = new HashMap<Double, Double>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String newName) {
        name = newName;
    }

    @Override
    public int getDataLength() {
        return data.size();
    }

    /**
     * This not necessarily returns the data that was added the i^th time
     * (data could be rearranged).
     */
    @Override
    public double getData(int i) {
        return data.get(i);
    }

    @Override
    public double getMidData() {
        return data.get((int) Math.round(getDataLength() * 0.5 - 0.5));
    }

    @Override
    public double getQuantile(Double q) {
        // TODO: replace this with the quantile calculation in facskin.math.Statistics

        if (!quantiles.containsKey(q))
            quantiles.put(q, data.get((int) Math.floor((data.size() - 0.000000001) * q)));
        return quantiles.get(q);
    }

    public List<Double> getDatas() {
        return data;
    }
}
