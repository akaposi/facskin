/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facskin.formats;

import java.awt.Color;
import java.io.Serializable;
/**
 *
 * @author Daniel Herman
 */
public class GateData implements  Serializable {
    private int param1;
    private int param2;
    private double[] points1;
    private double[] points2;
    private boolean log1;
    private boolean log2;
    private String gateDescription; 
    
    public GateData(int param1, int param2, double[] points1, double[] points2, boolean log1, boolean log2, String gateDesc) {
        this.param1 = param1;
        this.param2 = param2;
        this.points1 = points1;
        this.points2 = points2;
        this.log1 = log1;
        this.log2 = log2;
        this.gateDescription = gateDesc;
    }
    
    public int getParam1() {
        return param1;
    }
    
    public int getParam2() {
        return param2;
    }
    
    public double[] getPoints1() {
        return points1;
    }
    
    public double[] getPoints2() {
        return points2;
    }
    
    public boolean getLog1() {
        return log1;
    }
    
    public boolean getLog2() {
        return log2;
    }
    
    public String getGateDescription() {
        return gateDescription;
    }
}
