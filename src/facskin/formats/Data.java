package facskin.formats;

/**
 * Interface for representing a named collection of data with some statistical
 * information query functions.
 * @author ambi
 */
public interface Data {
    public String getName();
    public void setName(String newName);

    public int getDataLength();

    /**
     * This not necessarily returns the data that was added the i^th time
     * (data could be rearranged).
     */
    public double getData(int i);

    public double getMidData();

    public double getQuantile(Double q);
}
