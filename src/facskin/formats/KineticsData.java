package facskin.formats;

import facskin.FacsKinApp;
import facskin.TaskWithProgress;
import facskin.math.Statistics;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for representing a kinetics file, i.e. some metadata and a collection
 * of functions {@link FunctionData}. It can turn on/off standardization as well.
 * @author ambi
 */
public class KineticsData {
    private String[] metadata;
    private String filename;
    private ArrayList<FunctionData> functionDatas = new ArrayList<FunctionData>();
    private double[][] medians;
    private Double minMedianY = null, maxMedianY = null;
    private int best_function;
    private double standard = 1;
    private int fileLength;

    public KineticsData(InputStream inputstream, String filename, double maxTime, TaskWithProgress task) throws FileNotFoundException, IOException, NumberFormatException {
        InputStreamReader fr = new InputStreamReader(inputstream);
        BufferedReader br = new BufferedReader(fr);
        this.fileLength = Integer.parseInt(br.readLine().trim());

        int metadataLength = Integer.parseInt(br.readLine().trim());
        metadata = new String[metadataLength];
        for (int i = 0; i < metadataLength; i ++)
            metadata[i] = deescape(br.readLine());

        int kineticsDataLength = Integer.parseInt(br.readLine().trim());
        int lineCount = 0;
        while (lineCount < kineticsDataLength) {
            int functionMetadataLength = Integer.parseInt(br.readLine().trim());
            String name = deescape(br.readLine());
            double cv_value = Double.valueOf(br.readLine().trim());
            double sad = Double.valueOf(br.readLine().trim());
            //for (int i = 0; i < functionMetadataLength - 3; i ++)
            //    br.readLine();
            int parLength = Integer.parseInt(br.readLine().trim());
            String line = br.readLine();
            String numbers[] = line.split(" ");
            int paramCount = numbers.length;
            double[][] par = new double[parLength][paramCount];
            for (int j = 0; j < paramCount; j ++)
                par[0][j] = Double.valueOf(numbers[j]);
            for (int i = 1; i < parLength; i ++) {
                line = br.readLine();
                numbers = line.split(" ");
                for (int j = 0; j < paramCount; j ++)
                    par[i][j] = Double.valueOf(numbers[j]);
            }

            functionDatas.add(new FunctionData(name, cv_value, sad, par, maxTime));

            lineCount += functionMetadataLength + 2 + parLength;
        }

        int mediansLength = Integer.parseInt(br.readLine().trim());
        medians = new double[mediansLength][2];
        for (int i = 0; i < mediansLength; i ++) {
            String line = br.readLine();
            String numbers[] = line.split(" ");
            medians[i][0] = Double.valueOf(numbers[0]);
            medians[i][1] = Double.valueOf(numbers[1]);
        }

        // these would close the input stream which is a problem when reading
        // kinetics files in a zip
        // br.close();
        // fr.close();

        this.filename = filename;

        best_function = 0;
        for (int i = 1; i < functionDatas.size(); i ++)
            if (functionDatas.get(i).get_cv_value() < functionDatas.get(best_function).get_cv_value())
                best_function = i;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public int getFileLength() {
        return fileLength;
    }

    public String[] getMetadata() {
        return metadata;
    }

    public int getFunctionCount() {
        return functionDatas.size();
    }

    public FunctionData getFunction(int fct) {
        return functionDatas.get(fct);
    }

    public int getBestFunction() {
        return best_function;
    }

    public double[][] getMedians() {
        return medians;
    }

    public double getMinX() {
        return medians[0][0];
    }

    public double getMaxX() {
        return medians[medians.length - 1][0];
    }

    public double getMinY() {
        if (minMedianY == null) {
            minMedianY = medians[0][1];
            for (int i = 1; i < medians.length; i ++)
                if (minMedianY > medians[i][1])
                    minMedianY = medians[i][1];
        }
        return minMedianY;
    }

    public double getMaxY() {
        if (maxMedianY == null) {
            maxMedianY = medians[0][1];
            for (int i = 1; i < medians.length; i ++)
                if (maxMedianY < medians[i][1])
                    maxMedianY = medians[i][1];
        }
        return maxMedianY;
    }

    public double getFirstY() {
        return medians[0][1];
    }

    public double getLastY() {
        return medians[medians.length-1][1];
    }

    public double getStandardizer() {
        return standard;
    }

    /**
     * Remark: medians are not standardized by KineticsData, but by the
     * plotting method.
     * @param x
     */
    public void setStandardisation(boolean x) {
        if (x) {
            List<Double> values = new ArrayList<Double>();
            int i = 0;
            while (medians[i][0] <= FacsKinApp.getBaselineTime() &&
                   i < medians.length) {
                values.add(medians[i][1]);
                ++i;
            }

            double newStandard;

            // if there is no baseline, use the first value
            if (values.isEmpty()) {
                newStandard = medians[0][1];
                FacsKinApp.setBaselineTime(medians[0][0]);
            } else
                newStandard = Statistics.median(values);
            
            if (standard != newStandard)
                standard = newStandard;
        } else {
            if (standard != 1.0)
                standard = 1.0;
        }
        for (int i = 0; i < functionDatas.size(); i ++)
            functionDatas.get(i).setStandardizer(standard);
    }

    public void setMaxTime(double maxTime) {
        for (int i = 0; i < functionDatas.size(); i ++)
            functionDatas.get(i).setMaxTime(maxTime);
    }

    private static String deescape(String s) {
        s = s.replace("\\\\", "\\");
        s = s.replace("\\n", "\n");
        return s;
    }
}
