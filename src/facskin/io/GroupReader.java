package facskin.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Reader for a simple text file containing data in each row.
 * @author ambi
 */
public class GroupReader {
    public static List<String> readGroup(InputStream inputstream) throws IOException {
        List<String> list = new ArrayList<String>();

        InputStreamReader fr = new InputStreamReader(inputstream);
        BufferedReader br = new BufferedReader(fr);
        String line;
        while ((line = br.readLine()) != null) list.add(line);

        return list;
    }
}
