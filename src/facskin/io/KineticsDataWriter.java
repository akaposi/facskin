package facskin.io;

import facskin.formats.KineticsData;
import facskin.math.MyDecimalFormat;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

/**
 * Writing out kinetics data to an output stream.
 * @author ambi
 */
public class KineticsDataWriter {
    private static String escape(String s) {
        s = s.replace("\\", "\\\\");
        s = s.replace("\n", "\\n");
        return s;
    }

    public static void write(OutputStream stream, KineticsData data) throws IOException {
        Writer out = new BufferedWriter(new OutputStreamWriter(stream));
        out.write(data.getFileLength() + "\n");

        String[] metadata = data.getMetadata();
        out.write(metadata.length + "\n");
        for (int i = 0; i < metadata.length; i ++)
            out.write(escape(metadata[i]) + "\n");

        out.write(data.getFunctionCount() * (1+3+1+data.getFunction(0).getParam(0).getDataLength()) + "\n");

        for (int i = 0; i < data.getFunctionCount(); i ++) {
            out.write("3\n");
            out.write(escape(data.getFunction(i).getName()) + "\n");
            out.write(MyDecimalFormat.formatLongPoint(data.getFunction(i).get_cv_value()) + "\n");
            out.write(MyDecimalFormat.formatLongPoint(data.getFunction(i).get_sad()) + "\n");
            out.write(data.getFunction(i).getParam(0).getDataLength() + "\n");
            int paramCount = data.getFunction(i).getRealParamCount();
            for (int j = 0; j < data.getFunction(i).getParam(0).getDataLength(); j ++) {
                for (int k = 0; k < paramCount - 1; k ++)
                    out.write(MyDecimalFormat.formatLongPoint(data.getFunction(i).getParam(k).getDataWithoutStandardization(j)) + " ");
                out.write(MyDecimalFormat.formatLongPoint(data.getFunction(i).getParam(paramCount - 1).getDataWithoutStandardization(j)) + "\n");
            }
        }

        double[][] medians = data.getMedians();
        out.write(medians.length + "\n");
        for (int i = 0; i < medians.length; i ++)
            out.write(MyDecimalFormat.formatLongPoint(medians[i][0]) + " " + MyDecimalFormat.formatLongPoint(medians[i][1]) + "\n");
        
        out.flush();
    }
}
