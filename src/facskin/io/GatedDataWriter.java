package facskin.io;

import facskin.TaskWithProgress;
import facskin.formats.FcsData;
import facskin.math.MyDecimalFormat;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

/**
 * Writer for gated data. It outputs in text format to an OutputStream.
 * @author ambi
 */
public class GatedDataWriter {
    private static String escape(String s) {
        s = s.replace("\\", "\\\\");
        s = s.replace("\n", "\\n");
        return s;
    }

    public static void write(
            OutputStream stream,
            FcsData data,
            int timeParamNum,
            int p1,
            int p2,
            String measurementName,
            String email,
            int numOfQuantiles,
            Double baselineLength,
            TaskWithProgress task) throws IOException {
        Writer out = new BufferedWriter(new OutputStreamWriter(stream));

        int metacount = 8;
        if (baselineLength == null)
            metacount = 7;
        
        out.write((data.getEventCount() + 2 + metacount) + "\n");
        out.write(metacount + "\n");
        out.write(escape(data.getMeasurementTime()) + "\n");
        out.write(escape(measurementName) + "\n");
        out.write(escape(data.getFilename()) + "\n");
        out.write(escape(email) + "\n");
        out.write(escape(data.getGateDescription()) + "\n");
        if (p2 != -1)
            out.write(escape(data.getParameterName(p1)) + "/" + escape(data.getParameterName(p2)) + "\n");
        else
            out.write(escape(data.getParameterName(p1)) + "\n");
        out.write(numOfQuantiles + "\n");
        if (baselineLength != null)
            out.write(MyDecimalFormat.formatLongPoint(baselineLength) + "\n");

        int length = data.getEventCount();
        out.write(length + "\n");
        for (int i = 0; i < length; i ++) {
            double[] event = data.getEvent(i);
            double value;
            if (p2 != -1) {
                if (event[p2] != 0)
                    value = event[p1] / event[p2];
                else
                    value = Double.NaN;
            } else
                value = event[p1];
            out.write(MyDecimalFormat.formatLongPoint(event[timeParamNum]) + " " + MyDecimalFormat.formatLongPoint(value) + "\n");
            if (i % Math.floor(length / 100) == 0)
                task.setProgressValue((float) i / length);
        }
        out.flush();
    }
}
