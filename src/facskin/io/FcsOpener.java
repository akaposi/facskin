package facskin.io;

import facskin.TaskWithProgress;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.math.linear.Array2DRowRealMatrix;
import org.apache.commons.math.linear.LUDecompositionImpl;


/**
 * Class for reading FCS. The only conversion it does is linearizing logarithmized
 * values and applying a compensation matrix, if available.
 * @author ambi
 */
public class FcsOpener {
    private DataInputStream dis;

    private String version;
    private int text_offset_start;
    private int text_offset_end;
    private int data_offset_start;
    private int data_offset_end;

    private Map<String, String> keywords = new HashMap<String, String>();
    private int parameterCount;

    private ArrayList<double[]> data = new ArrayList<double[]>();

    private TaskWithProgress task;

    private boolean compensationNeeded;
    private double[][] comp_mat = null;

    public FcsOpener(InputStream is, int bufferSize, TaskWithProgress task) throws IOException {
        this.task = task;
        this.compensationNeeded = true;
        open(is, bufferSize);
    }

    public FcsOpener(InputStream is, int bufferSize, boolean compensate, TaskWithProgress task) throws IOException {
        this.task = task;
        this.compensationNeeded = compensate;
        open(is, bufferSize);
    }

    private static float byteArrayToFloat(byte test[]) {
        // from http://dbaktiar.wordpress.com/2009/06/26/float-and-byte-array-conversion/
        // modified variable i to go from 3 to 0 instead of 0 to 3
        int bits = 0;
	int i = 3;
        for (int shifter = 3; shifter >= 0; shifter--) {
            bits |= ((int) test[i] & 0xff) << (shifter * 8);
	    i--;
        }
        return Float.intBitsToFloat(bits);
    }

    private static int byteArrayToInt2(byte[] b) {
        // from http://snippets.dzone.com/posts/show/94
        // modified so that b is indexed from 1 to 0 instead of 0 to 1
        int value = 0;
        for (int i = 0; i < 2; i++) {
            int shift = (2 - 1 - i) * 8;
            value += (b[1-i] & 0x000000FF) << shift;
        }
        return value;
    }

    private static int byteArrayToInt4(byte[] b) {
        // from http://snippets.dzone.com/posts/show/94
        // modified so that b is indexed from 3 to 0 instead of 0 to 3
        int value = 0;
        for (int i = 0; i < 4; i++) {
            int shift = (4 - 1 - i) * 8;
            value += (b[3-i] & 0x000000FF) << shift;
        }
        return value;
    }

    private void open(InputStream is, int bufferSize) throws IOException {
        dis = new DataInputStream(new BufferedInputStream(is, bufferSize));

        int a = readHeader();

        dis.skip(text_offset_start - a);
        readText(text_offset_end - text_offset_start + 1);
        if (data_offset_start == 0)
            data_offset_start = Integer.parseInt(keywords.get("$BEGINDATA").replace(" ", ""));
        if (data_offset_end == 0)
            data_offset_end = Integer.parseInt(keywords.get("$ENDDATA").replace(" ", ""));
        dis.skip(data_offset_start - text_offset_end - 1);
        parameterCount = Integer.parseInt(keywords.get("$PAR"));
        readData(data_offset_end - data_offset_start + 1);

        linearize();

        if (compensationNeeded)
            compensate();

        // ArrayList keywords_sorted = new ArrayList(keywords.keySet());
        // Collections.sort(keywords_sorted);
	// for (int i = 0; i < keywords_sorted.size(); i ++)
	//     System.out.println(keywords_sorted.get(i) + ": " + keywords.get(keywords_sorted.get(i)));

	//	try {
	//	PrintWriter writer = new PrintWriter("raw_data_par7", "UTF-8");
	//	for (int i = 0; i < data.size(); i++) {
	//	    double[] event = data.get(i);
	//	    writer.println(event[6]);
	//	}
	//	writer.close(); } catch (IOException e) {
	//	}
    }

    private int readHeader() throws IOException {
        char[] ver_ = new char[6];
        for (int j = 0; j < 6; j ++) ver_[j] = (char) dis.readUnsignedByte();
        String ver = String.valueOf(ver_);
	//	System.out.println("FCS version: " + ver);
        if (ver.equals("FCS2.0"))
            version = "2.0";
        else {
            if (ver.equals("FCS3.0")) {
                version = "3.0";
            } else {
		if (ver.equals("FCS3.1"))
		    version = "3.1";
		else
		    throw new IOException("Don't know how to deal with FCS version " + ver);
	    }
        }

        dis.skip(4);

        char[] text_offset_start_ = new char[8];
        for (int j = 0; j < 8; j ++) text_offset_start_[j] = (char) dis.readUnsignedByte();
        text_offset_start = Integer.parseInt((
                String.valueOf(text_offset_start_)).replace(" ", ""));

        char[] text_offset_end_ = new char[8];
        for (int j = 0; j < 8; j ++) text_offset_end_[j] = (char) dis.readUnsignedByte();
        text_offset_end = Integer.parseInt((
                String.valueOf(text_offset_end_)).replace(" ", ""));

        char[] data_offset_start_ = new char[8];
        for (int j = 0; j < 8; j ++) data_offset_start_[j] = (char) dis.readUnsignedByte();
        data_offset_start = Integer.parseInt((
                String.valueOf(data_offset_start_)).replace(" ", ""));

        char[] data_offset_end_ = new char[8];
        for (int j = 0; j < 8; j ++) data_offset_end_[j] = (char) dis.readUnsignedByte();
        data_offset_end = Integer.parseInt((
                String.valueOf(data_offset_end_)).replace(" ", ""));

        return 42;
    }

    private void readText(int length) throws IOException {
        char delimiter = (char) dis.readUnsignedByte();

        int i = 1; // we have already read the delimiter!
        boolean stop = false;
        char temp = ' ';
        do {
            String keyword = "";
            do {
                if (!stop) {
                    temp = (char) dis.readUnsignedByte(); i ++;
                } else
                    stop = false;
                if (temp == delimiter) {
                    temp = (char) dis.readUnsignedByte(); i ++;
                    if (temp == delimiter)
                        keyword += temp;
                    else
                        stop = true;
                } else
                    keyword += temp;
            } while (!stop);

            String value = "";
            do {
                if (!stop) {
                    temp = (char) dis.readUnsignedByte(); i ++;
                } else
                    stop = false;
                if (temp == delimiter) {
                    if (i < length) {
                        temp = (char) dis.readUnsignedByte(); i ++;
                        if (temp == delimiter)
                            value += temp;
                        else
                            stop = true;
                    }
                } else
                    value += temp;
            } while (!stop && i < length);
            keywords.put(keyword, value);
        } while (i < length);
    }

    private void readData(int length) throws IOException {
        if (keywords.containsKey("$MODE") && !keywords.get("$MODE").equals("L"))
            throw new IOException("Don't know how to deal with $MODE " + keywords.get("$MODE"));
        
        if (keywords.containsKey("$INST/$MODE") && !keywords.get("$INST/$MODE").equals("L"))
            throw new IOException("Don't know how to deal with $MODE " + keywords.get("$MODE"));

        if (!keywords.get("$BYTEORD").equals("4,3,2,1") && !keywords.get("$BYTEORD").equals("1,2,3,4") && !keywords.get("$BYTEORD").equals("1,2"))
            throw new IOException("Don't know how to deal with $BYTEORD " + keywords.get("$BYTEORD"));

        if (!keywords.get("$DATATYPE").equals("F") &&
                !keywords.get("$DATATYPE").equals("D") &&
                !keywords.get("$DATATYPE").equals("I"))
            throw new IOException("Don't know how to deal with $DATATYPE " + keywords.get("$DATATYPE"));


        int[] parameterSizes = new int[parameterCount];
        int step = 0;
        for (int j = 0; j < parameterCount; j ++) {
            parameterSizes[j] = Integer.parseInt(keywords.get("$P" + (j+1) + "B"));
            step += parameterSizes[j] / 8;
        }

        if (keywords.get("$DATATYPE").equals("F")) {
            for (int j = 0; j < parameterCount; j ++)
                if (parameterSizes[j] != 32)
                    throw new IOException("Float parameter type must be of 32 bits size.");
            int i = 0;

            if (keywords.get("$BYTEORD").equals("4,3,2,1")) {
                while (i < length) {
                    double[] p = new double[parameterCount];
                    for (int j = 0; j < parameterCount; j ++)
                        p[j] = dis.readFloat();
                    i += step;
                    data.add(p);
                    if (task != null && i % Math.floor(length / 100) == 0)
                        task.setProgressValue((float) i / length);
                }
            } else { //1, 2, 3, 4
                while (i < length) {
                    double[] p = new double[parameterCount];
                    byte[] buf = new byte[4];
                    for (int j = 0; j < parameterCount; j ++) {
                        dis.read(buf, 0, 4);
                        p[j] = byteArrayToFloat(buf);
                    }
                    i += step;
                    data.add(p);
                    if (task != null && i % Math.floor(length / 100) == 0)
                        task.setProgressValue((float) i / length);
                }
            }
        }

        if (keywords.get("$DATATYPE").equals("D")) {
            for (int j = 0; j < parameterCount; j ++)
                if (parameterSizes[j] != 64)
                    throw new IOException("Double parameter type must be of 64 bits size.");
            int i = 0;
            while (i < length) {
                double[] p = new double[parameterCount];
                for (int j = 0; j < parameterCount; j ++)
                    p[j] = dis.readDouble();
                i += step;
                data.add(p);
                if (task != null && i % Math.floor(length / 100) == 0)
                    task.setProgressValue((float) i / length);
            }
        }

        if (keywords.get("$DATATYPE").equals("I") && keywords.get("$BYTEORD").equals("4,3,2,1")) {
            int i = 0;
            byte[] buf = new byte[4];
            byte[] buf1 = new byte[4];
            while (i < length) {
                double[] p = new double[parameterCount];
                for (int j = 0; j < parameterCount; j ++) {
                    if (parameterSizes[j] == 8) {
                        p[j] = dis.readUnsignedByte();
                    } else if (parameterSizes[j] == 16) {
                        p[j] = dis.readUnsignedShort();
                    } else if (parameterSizes[j] == 32) {
                        dis.read(buf, 0, 4);
                        buf1[0] = buf[3];
                        buf1[1] = buf[2];
                        buf1[2] = buf[1];
                        buf1[3] = buf[0];
                        p[j] = byteArrayToInt4(buf1);
                    } else {
                        throw new IOException("Not implemented");
                    }
                }
                i += step;
                data.add(p);
                if (task != null && i % Math.floor(length / 100) == 0)
                    task.setProgressValue((float) i / length);
            }
        }

        if (keywords.get("$DATATYPE").equals("I") && (keywords.get("$BYTEORD").equals("1,2,3,4") || keywords.get("$BYTEORD").equals("1,2"))) {
            int i = 0;
            byte[] buf = new byte[4];
            while (i < length) {
                double[] p = new double[parameterCount];
                for (int j = 0; j < parameterCount; j ++) {
                    if (parameterSizes[j] == 8) {
                        p[j] = dis.readUnsignedByte();
                    } else if (parameterSizes[j] == 16) {
                        dis.read(buf, 0, 2);
                        p[j] = byteArrayToInt2(buf);
                    } else if (parameterSizes[j] == 32) {
                        dis.read(buf, 0, 4);
                        p[j] = byteArrayToInt4(buf);
                    } else
                        throw new IOException("Not implemented");
                }
                i += step;
                data.add(p);
                if (task != null && i % Math.floor(length / 100) == 0)
                    task.setProgressValue((float) i / length);
            }
        }

    }

    private void linearize() {
        ArrayList<Integer> paramNumbers = new ArrayList<Integer>();
        double[] logDecades = new double[parameterCount];
        double[] ranges = new double[parameterCount];
        double[] multiplier = new double[parameterCount];
        for (int i = 0; i < parameterCount; i ++) {
            logDecades[i] = Double.parseDouble(
                    keywords.get("$P" + (i + 1) + "E").split(",")[0]); //TODO: mi van a masodik elemevel?
            ranges[i] = Double.parseDouble(keywords.get("$P" + (i + 1) + "R"));
            multiplier[i] = logDecades[i] / (ranges[i] - 1);
            if (logDecades[i] != 0.0) {
                paramNumbers.add(i);
		//		System.out.println("parameter " + getParamName(i) + " linearized");
	    }
        }

        if (paramNumbers.size() > 0)
            for (int i = 0; i < data.size(); i ++) {
                double[] event = data.get(i);
                for (int j : paramNumbers) {
                    event[j] = Math.pow(10, event[j] * multiplier[j]);
                }
                data.set(i, event);
            }
    }

    private void compensate() {
        if (keywords.get("APPLY COMPENSATION") != null)
            if (keywords.get("APPLY COMPENSATION").equals("TRUE")) {
		String spilling = "";
		if (keywords.get("SPILL") != null)
		    spilling = "SPILL";
		if (spilling.equals("")) {
		    ArrayList<String> keywords_set = new ArrayList<String>(keywords.keySet());
		    for (int i = 0; i < keywords_set.size(); i ++)
			if (keywords_set.get(i).endsWith("SPILLOVER"))
			    spilling = keywords_set.get(i);
		}
		if (spilling.equals("")) {
		    System.out.println("Parameters not compensated because I couldn't find spillover matrix.");
		} else {
                    String[] s = keywords.get(spilling).split(",");

		    
                    int n = Integer.parseInt(s[0]); // number of params to compensate
                    comp_mat = new double[n][n];

                    // filling the comp_mat
                    for (int i = 0; i < n; i ++) {
                        for (int j = 0; j < n; j ++)
                            comp_mat[i][j] = Double.parseDouble(s[i * n + n + 1 + j]);
                    }

                    Array2DRowRealMatrix m = new Array2DRowRealMatrix(comp_mat);
                    double[][] comp_mat_inverse = new LUDecompositionImpl(m).getSolver().getInverse().getData();

                    // where can we find which parameter in the compensation matrix
                    int[] comp_mat_place = new int[getParameterCount()];
                    int[] real_place = new int[n];
                    for (int i = 0; i < getParameterCount(); i ++) {
                        comp_mat_place[i] = -1;
                        for (int j = 0; j < n; j ++)
                            if (s[j + 1].equals(getParamName(i))) {
                                comp_mat_place[i] = j;
                                real_place[j] = i;
                            }
                    }

                    // the compensation itself (is simply a matrix multiplication)
                    for (int i = 0; i < data.size(); ++i) {
                        double[] event = new double[getParameterCount()];
                        for (int j = 0; j < getParameterCount(); ++j) {
                            if (comp_mat_place[j] != -1) {
                                event[j] = 0;
                                for (int k = 0; k < n; ++k)
                                    event[j] += data.get(i)[real_place[k]] * comp_mat_inverse[k][comp_mat_place[j]];
                            } else
                                event[j] = data.get(i)[j];
                        }
                        data.set(i, event);
                    }

                    System.out.println("Parameters compensated.");
                }
	    }
    }

    public int getParameterCount() {
        return parameterCount;
    }

    public String getFCSVersion() {
        return version;
    }

    public ArrayList<double[]> getData() {
        return data;
    }

    public String getKeywordValue(String keyword) {
        return keywords.get(keyword);
    }

    public String getParamName(int j) {
        j = j + 1;
        return keywords.get("$P" + j + "N");
    }

    public double[][] getCompensationMatrix() {
        return comp_mat;
    }
}
