package facskin.io;

import facskin.TaskWithProgress;
import facskin.formats.KineticsData;
import facskin.math.MyDecimalFormat;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Reading and writing collections of kinetics files together with some metadata.
 * @author ambi
 */
public class KineticsZip {

    // reading kinetics zip files

    private List<KineticsData> datas = new ArrayList<KineticsData>();
    private List<String> groups = null;
    private List<KineticsData> pairs = null;
    private Integer selectedFunction = null;
    private Double maxTime = null;
    private Double baselineTime = null;

    public KineticsZip(File file, TaskWithProgress task) throws FileNotFoundException, IOException {
        ZipInputStream zis = new ZipInputStream(new FileInputStream(file));
        ZipEntry entry;

        double tmpMaxTime = 100;
        boolean tmpStandarisation = true;

        List<String> groupList = null;
        List<String> pairsList = null;
        while ((entry = zis.getNextEntry()) != null) {
            if (entry.getName().toLowerCase().endsWith(".kinetics")) {
                KineticsData kineticsData = new KineticsData(zis, entry.getName(), tmpMaxTime, task);
                kineticsData.setStandardisation(tmpStandarisation);
                datas.add(kineticsData);
            } else {
                if (entry.getName().toLowerCase().equals("__groups")) {
                    groupList = GroupReader.readGroup(zis);
                }
                if (entry.getName().toLowerCase().equals("__pairs")) {
                    pairsList = GroupReader.readGroup(zis);
                }
                if (entry.getName().toLowerCase().equals("__maxtimeforauc")) {
                    maxTime = MaxTimeForAUCReader.readMaxTimeForAUC(zis);
                }
                if (entry.getName().toLowerCase().equals("__selectedfunction")) {
                    selectedFunction = SelectedFunctionReader.readSelectedFunction(zis);
                }
                if (entry.getName().toLowerCase().equals("__baselinetime")) {
                    baselineTime = MaxTimeForAUCReader.readMaxTimeForAUC(zis);
                }
            }
        }
        zis.close();

        if (groupList != null) {
            groups = new ArrayList<String>();
            for (int i = 0; i < datas.size(); ++i)
                groups.add("");
            for (int i = 0; i < groupList.size(); i +=2)
                setGroup(groupList.get(i), groupList.get(i+1));
        }
        if (pairsList != null) {
            pairs = new ArrayList<KineticsData>();
            for (int i = 0; i < datas.size(); ++i)
                pairs.add(null);
            for (int i = 0; i < pairsList.size(); i +=2)
                setPairs(pairsList.get(i), pairsList.get(i+1));
        }

        if (maxTime != null) {
            for (int i = 0; i < datas.size(); ++i)
                datas.get(i).setMaxTime(maxTime);
        }
    }

    private void setGroup(String filename, String group) {
        int i = 0;
        while (i < datas.size() && !datas.get(i).getFilename().equalsIgnoreCase(filename))
            i ++;
        if (i < datas.size())
            groups.set(i, group);
    }

    private void setPairs(String filename, String pair) {
        int i = 0;
        while (i < datas.size() && !datas.get(i).getFilename().equalsIgnoreCase(filename))
            i ++;
        int j = 0;
        while (j < datas.size() && !datas.get(j).getFilename().equalsIgnoreCase(pair))
            j ++;
        if (i < datas.size() && j < datas.size())
            pairs.set(i, datas.get(j));
    }

    public List<KineticsData> getDatas()            { return datas; }
    public List<String>       getGroups()           { return groups; };
    public List<KineticsData> getPairs()            { return pairs; }
    public Integer            getSelectedFunction() { return selectedFunction; }
    public Double             getMaxTime()          { return maxTime; }
    public Double             getBaselineTime()     { return baselineTime; }

    // writing kinetics zip files

    public KineticsZip(List<KineticsData> datas, 
                       List<String>       groups,
                       List<KineticsData> pairs,
                       int selectedFunction,
                       double maxTime,
                       double baselineTime) {
        this.datas = datas;
        this.groups = groups;
        this.pairs = pairs;
        this.selectedFunction = selectedFunction;
        this.maxTime = maxTime;
        this.baselineTime = baselineTime;
    }

    public void writeDatas(File file) throws FileNotFoundException, IOException {
        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(file));
        for (int i = 0; i < datas.size(); i ++) {
            out.putNextEntry(new ZipEntry(datas.get(i).getFilename()));
            KineticsDataWriter.write(out, datas.get(i));
        }
        out.putNextEntry(new ZipEntry("__groups"));
        Writer writer = new BufferedWriter(new OutputStreamWriter(out));
        for (int i = 0; i < datas.size(); i ++)
            writer.write(datas.get(i).getFilename() + "\n" + groups.get(i) + "\n");
        writer.flush();
        out.putNextEntry(new ZipEntry("__maxTimeForAUC"));
        writer = new BufferedWriter(new OutputStreamWriter(out));
        writer.write(MyDecimalFormat.formatLongPoint(maxTime));
        writer.flush();
        out.putNextEntry(new ZipEntry("__selectedFunction"));
        writer = new BufferedWriter(new OutputStreamWriter(out));
        writer.write(String.valueOf(selectedFunction));
        writer.flush();
        out.putNextEntry(new ZipEntry("__pairs"));
        for (int i = 0; i < pairs.size(); i ++) {
            writer.write(datas.get(i).getFilename() + "\n");
            if (pairs.get(i) != null)
                writer.write(pairs.get(i).getFilename());
            writer.write("\n");
        }
        writer.flush();
        if (baselineTime != null) {
          out.putNextEntry(new ZipEntry("__baselinetime"));
          writer = new BufferedWriter(new OutputStreamWriter(out));
          writer.write(MyDecimalFormat.formatLongPoint(baselineTime));
          writer.flush();
        }
        out.close();
    }
}
