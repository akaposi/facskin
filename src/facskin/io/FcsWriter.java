/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facskin.io;

import facskin.TaskWithProgress;
import facskin.formats.FcsData;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author ambi
 */
public class FcsWriter {
    private static String int2str(int i) {
        String s = String.valueOf(i);
        if (s.length() <= 8)  {
            return " ".repeat(8-s.length()) + s;
        } else {
            return "       0";
        }
    }    
    
    public static void write(File file, FcsData data, TaskWithProgress task) throws IOException {
        DataOutputStream os = new DataOutputStream(new FileOutputStream(file));
        final char sep = 12;
        final int text_offset_start = 58;
        int text_offset_end = 0;
        int data_offset_start = 0;
        int data_offset_end = 0;
        boolean changed = true;
        String text = "";
        while (changed) {
            text = sep +
                "$BEGINANALYSIS" + sep + "0" + sep +
                "$BEGINDATA" + sep + String.valueOf(data_offset_start) + sep +
                "$BEGINSTEXT" + sep + String.valueOf(text_offset_start) + sep +
                "$BYTEORD" + sep + "4,3,2,1" + sep +
                "$DATATYPE" + sep + "D" + sep +
                "$ENDANALYSIS" + sep + "0" + sep +
                "$ENDDATA" + sep + String.valueOf(data_offset_end) + sep +
                "$ENDSTEXT"	+ sep + String.valueOf(text_offset_end) + sep +
                "$MODE" + sep + "L" + sep +
                "$NEXTDATA" + sep + "0" + sep +
                "$PAR" + sep + data.getParameterCount() + sep +
                "$TOT" + sep + data.getEventCount() + sep +
                "$NEXTDATA" + sep + "0" + sep +
                "$TIMESTEP" + sep + "1" + sep +
                "$DATE" + sep + data.getMeasurementTime() + sep;
            for (int i = 0; i < data.getParameterCount(); ++i) {
                text = text +
                        "$P" + (i+1) + "N" + sep + data.getParameterName(i) + sep +
                        "$P" + (i+1) + "E" + sep + "0,0" + sep +
                        "$P" + (i+1) + "B" + sep + "64" + sep +
                        "$P" + (i+1) + "R" + sep + "0" + sep;
            }
            int new_text_offset_end = text_offset_start + text.length() - 1;
            if (new_text_offset_end != text_offset_end) {
                changed = true;
                text_offset_end = new_text_offset_end;
                data_offset_start = text_offset_end + 1;
                data_offset_end = data_offset_start + data.getParameterCount() * data.getEventCount() * 8 - 1;
            } else
                changed = false;
        }
        String header = "FCS3.0    " + int2str(text_offset_start) + int2str(text_offset_end) + int2str(data_offset_start) + int2str(data_offset_end) + int2str(0) + int2str(0);
        if (header.length() != text_offset_start) throw new IOException("header length problem");
        os.write(header.getBytes());
        os.write(text.getBytes());
        for (int i = 0; i < data.getEventCount(); i++) {
            if (task != null)
                if (i % Math.floor(data.getEventCount() / 100) == 0)
                    task.setProgressValue((float) i / data.getEventCount());
            for (int j = 0; j < data.getParameterCount(); j++)
                os.writeDouble(data.getEvent(i)[j]);
        }
        os.close();        
    }   
}
