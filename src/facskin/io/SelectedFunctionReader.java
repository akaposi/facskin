package facskin.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Reading in one integer from a text file.
 * @author ambi
 */
public class SelectedFunctionReader {
    public static int readSelectedFunction(InputStream inputstream) throws IOException {
        InputStreamReader fr = new InputStreamReader(inputstream);
        BufferedReader br = new BufferedReader(fr);
        String line = br.readLine();
        return Integer.valueOf(line);
    }
}
