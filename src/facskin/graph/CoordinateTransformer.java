package facskin.graph;

import java.awt.Point;

/**
 * The coordinate system of the screen in Java is different from that of the plots that
 * we want to create. Distances on the screen are measured in pixels, not in arbitrary
 * units and the y coordinates of the pixels increase from top to bottom. To allow the
 * Graph classes plotting seamlessly using real mathematical values and also enable
 * drawing strings or bullets on the plot using pixel values (so that a bullet would
 * be always recognizable regardless of the pixels pro real values ratio) we implement
 * class CoordinateTransformer for the conversion between the following units:
 * <ul>
 * <li>real values (conventional mathematical coordinate system)
 * <li>virtual values (pixels, y coordinates increase from bottom to top)
 * <li>screen values (pixels, y coordinates increase from top to bottom)
 * </ul>
 * The size of frame around the plot is fixed in pixels (offset[Right|Left|Top|Bottom])
 * and the origo of the real coordinate system can be placed anywhere by setting
 * real[Right|Left|Top|Bottom] values in the constructor.
 * @author ambi
 */
public class CoordinateTransformer {
    // size of plot
    private int width;
    private int height;

    // real offsets
    private double left;
    private double bottom;
    private double right;
    private double top;

    // screen values
    private int offsetTop = 20;
    private int offsetRight = 20;
    private int offsetLeft = 50;
    private int offsetBottom = 30;

    public CoordinateTransformer(int width, int height,
            double left, double bottom, double right, double top, int offsetLeft) {
        this.offsetLeft = offsetLeft;
        this.setWidth(width);
        this.setHeight(height);

        this.top = top;
        this.bottom = bottom;
        this.right = right;
        this.left = left;
    }

    public CoordinateTransformer(int width, int height,
            double left, double bottom, double right, double top) {
        this.setWidth(width);
        this.setHeight(height);

        this.top = top;
        this.bottom = bottom;
        this.right = right;
        this.left = left;
    }

    public Point realToVirtual(double x, double y) {
        double scaleX = getWidth() / (getRealRight() - getRealLeft());
        double scaleY = getHeight() / (getRealTop() - getRealBottom());
        Point p = new Point();
        p.x = (int) Math.round((x - getRealLeft()) * scaleX);
        p.y = (int) Math.round((y - getRealBottom()) * scaleY);
        return p;
    }

    public Point virtualToScreen(int x, int y) {
        Point p = new Point();
        p.x = x + offsetLeft;
        p.y = getHeight() - y + offsetTop;
        return p;
    }

    public Point realToScreen(double x, double y) {
        Point p = realToVirtual(x, y);
        return virtualToScreen(p.x, p.y);
    }

    public Point screenToVirtual(int x, int y) {
        Point p = new Point();
        p.x = x - offsetLeft;
        p.y = getHeight() - y + offsetTop;
        return p;
    }

    public double[] virtualToReal(int x, int y) {
        double scaleX = getWidth() / (getRealRight() - getRealLeft());
        double scaleY = getHeight() / (getRealTop() - getRealBottom());
        double[] p = new double[2];
        p[0] = x / scaleX + getRealLeft();
        p[1] = y / scaleY + getRealBottom();
        return p;
    }

    public double[] screenToReal(int x, int y) {
        Point p = screenToVirtual(x, y);
        return virtualToReal(p.x, p.y);
    }

    public int realToVirtualDistX(double x) {
        double scaleX = getWidth() / (getRealRight()-getRealLeft());
        return (int) Math.round(x*scaleX);
    }

    public int realToVirtualDistY(double y) {
        double scaleY = getHeight() / (getRealTop()-getRealBottom());
        return (int) Math.round(y*scaleY);

    }

    public int getLeft() {
        return offsetLeft;
    }

    public int getRight() {
        return  offsetLeft + width;
    }

    public double getRealLeft() {
        return left;
    }

    public double getRealBottom() {
        return bottom;
    }

    public double getRealRight() {
        return right;
    }

    public double getRealTop() {
        return top;
    }

    public double getRealWidth() {
        return getRealRight()-getRealLeft();
    }

    public double getRealHeight() {
        return getRealTop()-getRealBottom();
    }

    public int getWidth() {
        return width;
    }

    public final void setWidth(int width) {
        this.width = width - offsetRight - offsetLeft;
    }

    public int getHeight() {
        return height;
    }

    public final void setHeight(int height) {
        this.height = height - offsetTop - offsetBottom;
    }
}
