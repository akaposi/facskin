package facskin.graph;

import facskin.FacsKinApp;
import facskin.formats.FunctionData;
import facskin.formats.KineticsData;
import facskin.math.Functions;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;

/**
 * Renderer for a {@link GraphKinetics}.
 * @author ambi
 */
public class GraphKineticsRenderer extends GraphRenderer {
    public GraphKineticsRenderer() {
    }

    void render(Graphics g, KineticsData data, FunctionData fct, CoordinateTransformer transformer, Color color, boolean wideLine) {
        Graphics2D g2d = (Graphics2D) g;

        Object saved = g2d.getPaint();
        g2d.setPaint(color);

        Stroke savedStroke = g2d.getStroke();
        if (wideLine)
            g2d.setStroke(new BasicStroke(3));

        for (int i = 0; i < data.getMedians().length; i ++) {
            Point p = transformer.realToScreen(data.getMedians()[i][0], data.getMedians()[i][1] / data.getStandardizer());
            g2d.drawOval(p.x, p.y, 2, 2);
        }

        Point last = null;

        Double[] params = new Double[fct.getParamCount()];
        int mid = (int) Math.round(fct.getParam(0).getDataLength() * 0.5 - 0.5);
        for (int i = 0; i < fct.getParamCount(); i ++) {
            params[i] = fct.getParam(i).getData(mid);
        }

        double start, end, delta;
        if (FacsKinApp.getDrawFunctionUntilMeasurementEnd()) {
            start = data.getMedians()[0][0];
            end   = data.getMedians()[data.getMedians().length-1][0];
        } else {
            start = transformer.getRealLeft();
            end   = transformer.getRealRight();
        }
        delta = (end - start) / FacsKinApp.getNumberofBreaksinKineticsPlot();
        for (double x = start; x <= end; x += delta) {
            Point p = transformer.realToScreen(x, Functions.fct(fct.getName(), x, params));
            if (last != null)
                g2d.drawLine(last.x, last.y, p.x, p.y);
            last = p;
        }

        g2d.setPaint((Paint) saved);
        g2d.setStroke(savedStroke);

        //x tengely elnevezes kiirasa
        Rectangle2D rec = g2d.getFontMetrics().getStringBounds("time (s)",g2d);
        int x = (transformer.virtualToScreen(0,0).x +
                transformer.virtualToScreen(transformer.getWidth(), 0).x) / 2 -
                (int) Math.round(rec.getWidth())/2;
        g2d.drawString("time (s)", x, transformer.virtualToScreen(0,-20).y);

        //jon az y tengely
        rec = g2d.getFontMetrics().getStringBounds("ratio", g2d);
        String y_label = "relative parameter value";
        if (!FacsKinApp.getStandardisation())
            y_label = "absolute parameter value";

        g2d.drawString(y_label, transformer.virtualToScreen(-45,0).x, (int)Math.round(rec.getHeight()));

        super.render(g2d,transformer);
    }

}
