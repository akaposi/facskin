package facskin.graph;

import facskin.math.MyDecimalFormat;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.geom.Rectangle2D;

/**
 * General renderer for a {@link Graph}.
 * @author ambi
 */
public class GraphRenderer {
    protected CoordinateTransformer transformer;
    private int max_width = 0;

    public GraphRenderer() {
    }

    public void render(Graphics graphics, CoordinateTransformer transformer) {
        this.transformer = transformer;
        renderYAxis(graphics);
        renderXAxis(graphics);

        Graphics2D g2d = (Graphics2D) graphics;
        printXLabel(0, g2d);
        printXLabel(transformer.getWidth(), g2d);
        printYLabel(0, g2d);
        printYLabel(transformer.getHeight(), g2d);
    }

    protected void renderXAxis(Graphics g2d) {
        Point start = transformer.virtualToScreen(0, 0);
        Point stop = transformer.virtualToScreen(transformer.getWidth(), 0);
        g2d.drawLine(start.x, start.y, stop.x, stop.y);
    }

    protected void renderYAxis(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        Point start = transformer.virtualToScreen(0, 0);
        Point stop = transformer.virtualToScreen(0, transformer.getHeight());
        g2d.drawLine(start.x - 1, start.y, stop.x - 1, stop.y);
    }

    protected void printXLabel(int x, Graphics2D g2d) {
        Point textPos = transformer.virtualToScreen(x, -10);
        double[] real = transformer.virtualToReal(x, -10);
        Double d = real[0];
        Point start = transformer.virtualToScreen(x, 0);
        Point stop = transformer.virtualToScreen(x, -5);
        g2d.drawLine(start.x, start.y, stop.x, stop.y);
        drawString(MyDecimalFormat.formatShort(d), false, g2d, textPos);
    }

    protected void printYLabel(int x, Graphics2D g2d) {
        Point textPos = transformer.virtualToScreen(-7, x);
        double[] real = transformer.virtualToReal(-7, x);
        Double d = real[1];
        Point start = transformer.virtualToScreen(-5, x);
        Point stop = transformer.virtualToScreen(0, x);
        g2d.drawLine(start.x, start.y, stop.x, stop.y);
        drawString(MyDecimalFormat.formatShort(d), true, g2d, textPos);
    }

    private void drawString(String text, boolean vertical, Graphics2D g2d, Point p) {
        Rectangle2D rec = g2d.getFontMetrics().getStringBounds(text, g2d);
        if (vertical) {
            p.x = p.x - (int)Math.round(rec.getWidth());
            p.y = p.y + (int)Math.round(rec.getHeight() / 2);
        } else {
            p.x = p.x - (int)Math.round(rec.getWidth() / 2);
            p.y = p.y + (int)Math.round(rec.getHeight() * 0.7);
        }
        g2d.drawString(text, p.x, p.y);
    }

    public void drawCoordinates(Graphics g, double x, double y) {
        if (transformer != null) {
            Graphics2D g2d = (Graphics2D) g;
            String s = MyDecimalFormat.format(x) + " " + MyDecimalFormat.format(y);
            Object saved = g2d.getPaint();
            g2d.setPaint(Color.WHITE);
            Rectangle2D rec = g2d.getFontMetrics().getStringBounds(s, g2d);
            if ((int) Math.round(rec.getWidth()) > max_width)
                max_width = (int) Math.round(rec.getWidth());
            g2d.fillRect(
                    (int) (transformer.getWidth() + transformer.getLeft() - max_width),
                    1,
                    max_width,
                    (int) Math.round(rec.getHeight())+1);
            g2d.setPaint((Paint) saved);
            g2d.drawString(s, transformer.getWidth() + transformer.getLeft() - Math.round(rec.getWidth()),
                    (int) Math.round(rec.getHeight()));
        }
    }
}

