package facskin.graph;

import facskin.FacsKinApp;
import facskin.formats.Data;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 * Graph for a histogram. Used by {@link facskin.compare.ComparePanel}.
 * @author ambi
 */
public class GraphHistogramPlot extends Graph {
    private int n = FacsKinApp.getNumberofBreaksinHistogramPlot(); // count of breakpoints
    private List<double[]> counts = new ArrayList<double[]>();
    private List<Color> colors;
    private String[] filenames;
    private String paramName;
    private double minX, minY, maxX, maxY;
    private List<Double> yLines;

    public GraphHistogramPlot(List<Data> datas, List<Color> colors, String paramName) {
        super();
        init(datas, colors, paramName, new ArrayList<Double>());
    }

    public GraphHistogramPlot(List<Data> datas, List<Color> colors, String paramName, List<Double> yLines) {
        super();
        init(datas, colors, paramName, yLines);
    }

    private void init(List<Data> datas, List<Color> colors, String paramName, List<Double> yLines) {
        this.colors = colors;
        this.yLines = yLines;
        renderer = new GraphHistogramPlotRenderer();

        minX = Double.MAX_VALUE;
        maxX = Double.MIN_VALUE;

        for (int i = 0; i < datas.size(); i ++) {
            Data data = datas.get(i);
            double newMinX = Math.max(data.getQuantile(0.0), data.getQuantile(0.25)-4.5*(data.getQuantile(0.75)-data.getQuantile(0.25)));
            double newMaxX = Math.min(data.getQuantile(1.0), data.getQuantile(0.75)+4.5*(data.getQuantile(0.75)-data.getQuantile(0.25)));
            if (newMinX < minX) minX = newMinX;
            if (newMaxX > maxX) maxX = newMaxX;
        }

        minY = 0.0;
        maxY = 0.0;

        for (int i = 0; i < datas.size(); ++i) {
            Data data = datas.get(i);
            double[] count = new double[n];
            for (int j = 0; j < n; ++j)
                count[j] = 0.0;
            for (int j = 0; j < data.getDataLength(); ++j)
                ++count[Math.max(0, Math.min((int) Math.floor((data.getData(j)-minX)/(maxX-minX)*n), n-1))];
            counts.add(count);
            for (int j = 0; j < n; ++j)
                if ((double) count[j] > maxY) maxY = (double) count[j];
        }

        filenames = new String[datas.size()];
        for (int i = 0; i < datas.size(); i ++)
            filenames[i] = datas.get(i).getName();

        this.paramName = paramName;
    }

    @Override
    public void reloadTransformer() {
        transformer = new CoordinateTransformer(getWidth(), getHeight(),
            minX, minY, maxX, maxY);
        this.repaint();
    }

    @Override
    public void reset() {
        reloadTransformer();
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        if (transformer == null)
            reloadTransformer();

        ((GraphHistogramPlotRenderer) renderer).render(graphics, transformer, paramName);
        for (int i = 0; i < counts.size(); i ++) {
            ((GraphHistogramPlotRenderer) renderer).renderSingleHistogramPlot(graphics, transformer, colors.get(i), counts.get(i), minX, maxX, n, filenames[i]);
        }

        if (yLines.size() > 0) {
            Graphics2D g2d = (Graphics2D) graphics;
            Paint oldPaint = g2d.getPaint();
            g2d.setPaint(Color.GRAY);
            for (int i = 0; i < yLines.size(); ++i) {
                Point p1 = transformer.realToScreen(yLines.get(i), 0);
                //Point p2 = transformer.realToScreen(yLines.get(i), maxY);
                g2d.drawLine(p1.x, p1.y, p1.x, p1.y+5);
            }

            g2d.setPaint(oldPaint);
        }
    }

    public void setYLines(List<Double> yLines) {
        this.yLines = yLines;
    }

    @Override
    public String name() {
        return paramName;
    }
}
