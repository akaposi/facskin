package facskin.graph;

import facskin.formats.Data;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.util.List;

/**
 * Graph for a box plot. Used by {@link facskin.compare.ComparePanel}
 * @author ambi
 */
public class GraphDotPlot extends Graph {
    int n;
    private List<Data> datas;
    private List<Color> colors;
    private String paramName;

    public GraphDotPlot(List<Data> datas, List<Color> colors, String paramName) {
        super();
        init(datas, colors, paramName);
    }

    private void init(List<Data> datas, List<Color> colors, String paramName) {
        this.colors = colors;
        this.datas = datas;
        this.paramName = paramName;

        n = datas.size();

        renderer = new GraphDotPlotRenderer();
    }

    @Override
    public void reloadTransformer() {
        if (n > 0) {
            double minX = Double.MAX_VALUE;
            double maxX = Double.MIN_VALUE;

            double minY = -0.5;
            double maxY = n - 0.5;

            for (int i = 0; i < n; i ++) {
                double newMinX = Math.max(datas.get(i).getQuantile(0.0), datas.get(i).getQuantile(0.25)-4.5*(datas.get(i).getQuantile(0.75)-datas.get(i).getQuantile(0.25)));
                double newMaxX = Math.min(datas.get(i).getQuantile(1.0), datas.get(i).getQuantile(0.75)+4.5*(datas.get(i).getQuantile(0.75)-datas.get(i).getQuantile(0.25)));
                if (newMinX < minX) minX = newMinX;
                if (newMaxX > maxX) maxX = newMaxX;
            }

            transformer = new CoordinateTransformer(getWidth(), getHeight(),
                minX, minY, maxX, maxY, 100);
            this.repaint();
        }
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        if (transformer == null)
            reloadTransformer();
        ((GraphDotPlotRenderer) renderer).render(graphics, transformer);
        for (int i = 0; i < n; i ++) {
            ((GraphDotPlotRenderer) renderer).renderSingleDotPlot(graphics, transformer, n-i-1, colors.get(i), datas.get(i));
        }

        // label of x axis
        Rectangle2D rec = graphics.getFontMetrics().getStringBounds(paramName, graphics);
        int x = (transformer.virtualToScreen(0,0).x +
                transformer.virtualToScreen(transformer.getWidth(), 0).x) / 2 -
                (int) Math.round(rec.getWidth())/2;
        graphics.drawString(paramName, x, transformer.virtualToScreen(0,-20).y);
    }

    @Override
    public String name() {
        return paramName;
    }
}
