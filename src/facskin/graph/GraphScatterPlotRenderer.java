package facskin.graph;

import facskin.formats.PlotList;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;

/**
 * Renderer for {@link GraphScatterPlot}.
 * @author ambi
 */
public class GraphScatterPlotRenderer extends GraphRenderer {
    GraphScatterPlot father;
    
    public void render(Graphics g2d, CoordinateTransformer transformer, int p1, int p2,  boolean log1, boolean log2, boolean showMedians) {
        super.render(g2d, transformer);
        renderValues(g2d, transformer, p1, p2, log1, log2, showMedians);
   }

    public GraphScatterPlotRenderer(GraphScatterPlot father) {
        this.father = father;
    }

    private void renderValues(Graphics g, CoordinateTransformer transformer, int p1, int p2, boolean log1, boolean log2, boolean showMedians) {
//        // p1, p2 validation
//        int min = Math.min(p1, p2);
//        int max = Math.max(p1, p2);
//        if (min < 0 || max >= father.getFather().getApp().getData().getParameterCount())
//            return;

        Graphics2D g2d = (Graphics2D) g;
        PlotList list = father.getFather().getData().getPlotData(p1, p2, log1, log2);
        int scaleX = father.getFather().getData().getPlotScale();
        int scaleY = father.getFather().getData().getPlotScale();
        double minX = transformer.getRealLeft();
        double maxX = transformer.getRealRight();
        double minY = transformer.getRealBottom();
        double maxY = transformer.getRealTop();
        double swidth = (maxX - minX) / scaleX;
        double sheight = (maxY - minY) / scaleY;
        int width = transformer.realToVirtualDistX(swidth);
        int height = transformer.realToVirtualDistY(sheight);
        
        //@author Daniel Herman
        PlotList gatedList = father.getFather().getData().getGatedPlotData(p1, p2, log1, log2);
        PlotList activatedList = father.getFather().getData().getActivatedPlotData(p1, p2, log1, log2);
        //--
     
        Object o = g2d.getPaint();
        for (int x = 0; x < scaleX; x ++) {
            double sumValue = 0; // drawing medians
            for (int y = 0; y < scaleY; y ++) {
                Point start = transformer.realToScreen(minX + x * swidth, minY + y * sheight);
                int value = (int) Math.round(Math.pow(1 - list.getNormalized(x, y), 5) * 255);
                //@author Daniel Herman
                if(gatedList != null) {
                    double valueGated = Math.pow(gatedList.getNormalized(x,y),0.5);
                    g2d.setPaint(new Color((int)(valueGated*(255-value))+value, value, value));
                } else {
                    if(activatedList != null) {
                        double valueActivated = Math.pow(activatedList.getNormalized(x,y),0.5);
                        g2d.setPaint(new Color(value,value,(int)(valueActivated*(255-value))+value));
                    } else {
                        g2d.setPaint(new Color(value, value, value));
                    }
                }
                //--
                g2d.fillRect(start.x, start.y - height, width, height);
                if (showMedians)
                    sumValue += list.getNormalized(x, y); // drawing medians
            }
            
            if (showMedians) {
                // drawing medians
                int y = 0;
                double value = 0;
                while (value < sumValue / 2 && y < scaleY) {
                    value += list.getNormalized(x, y);
                    y ++;
                }
                g2d.setPaint(new Color(0, 255, 0));
                Point start = transformer.realToScreen(minX + x * swidth, minY + y * sheight);
                g2d.drawLine(start.x, start.y, start.x + width, start.y);
            }
        }
     
        g2d.setPaint((Paint) o);
        
//        // printing the name of axis x
//        Rectangle2D rec = g2d.getFontMetrics().getStringBounds(p1.toString(),g2d);
//        int x = (transformer.virtualToScreen(0,0).x+transformer.virtualToScreen(transformer.getWidth(),0).x)/2 - (int)Math.round(rec.getWidth())/2;
//        g2d.drawString(p1.toString(),x,transformer.virtualToScreen(0,-20).y);
//
//        // printing the name of axis y
//        rec = g2d.getFontMetrics().getStringBounds(p2.toString(),g2d);
//        g2d.drawString(p2.toString(),transformer.virtualToScreen(-45,0).x,(int)Math.round(rec.getHeight()));
    }
}
