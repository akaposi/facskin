package facskin.graph;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 * Class for representing plots.
 * @author ambi
 */
public class Graph extends JPanel implements ComponentListener {
    protected GraphRenderer renderer = null;
    protected CoordinateTransformer transformer = null;

    public Graph() {
        setBackground(Color.WHITE);
        addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent mouseEvent) {
                mouseMovement(mouseEvent);
            }
            @Override
            public void mouseDragged(MouseEvent mouseEvent) {
                mouseDrag(mouseEvent);
            }
        });
        addComponentListener(this);
    }

    @Override
    public void componentResized(ComponentEvent e) {
        reloadTransformer();
    }

    protected void mouseMovement(MouseEvent mouseEvent) {
        if (transformer != null) {
            double[] coords = transformer.screenToReal(mouseEvent.getPoint().x, mouseEvent.getPoint().y);
            renderer.drawCoordinates(this.getGraphics(), coords[0], coords[1]);
        }
    }

    protected void mouseDrag(MouseEvent mouseEvent) {
        if (transformer != null) {
            double[] coords = transformer.screenToReal(mouseEvent.getPoint().x, mouseEvent.getPoint().y);
            renderer.drawCoordinates(this.getGraphics(), coords[0], coords[1]);
        }
    }

    public CoordinateTransformer getTransformer() {
        return transformer;
    }

    public void setTransformer(CoordinateTransformer transformer) {
        this.transformer = transformer;
    }

    public void reset() {}

    public void reloadTransformer(int p1, int p2, boolean log1, boolean log2) {}

    public void reloadTransformer() {}

    public String name() { return "NA"; }

    @Override
    public void componentMoved(ComponentEvent ce) {}

    @Override
    public void componentShown(ComponentEvent ce) {}

    @Override
    public void componentHidden(ComponentEvent ce) {}

    public void saveImage(File file) throws IOException {
        int width = getWidth();
        int height = getHeight();

        // Create a buffered image in which to draw
        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        // Create a graphics contents on the buffered image
        Graphics2D g2d = bufferedImage.createGraphics();

        g2d.setBackground(Color.WHITE);
        g2d.setPaint(Color.WHITE);
        g2d.fillRect(0,0,width,height);
        g2d.setPaint(Color.BLACK);

        paintComponent(g2d);

        // Graphics context no longer needed so dispose it
        g2d.dispose();

        ImageIO.write(bufferedImage, "png", file);
    }

}
