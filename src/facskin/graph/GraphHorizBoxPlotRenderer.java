package facskin.graph;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.util.List;

/**
 * Graph renderer of a box plot. Used by {@link facskin.compare.ComparePanel}
 * @author ambi
 */
public class GraphHorizBoxPlotRenderer extends GraphRenderer {

    public GraphHorizBoxPlotRenderer() {
    }

    public void renderSingleBoxPlot(Graphics g, CoordinateTransformer transformer, int y, Color color, double[] quartiles, String filename, List<Double> highOutliers, List<Double> lowOutliers, List<Double> highExtremeOutliers, List<Double> lowExtremeOutliers, double maxIQ, double minIQ) {
        Graphics2D g2d = (Graphics2D) g;

        Paint oldPaint = g2d.getPaint();
        g2d.setPaint(color);

        double width = 0.4;

        Point p = transformer.realToScreen(quartiles[0], y + width / 2);
        int w = transformer.realToScreen(quartiles[2], 0).x - p.x;
        int h = transformer.realToScreen(0, y - width / 2).y - p.y;
        g2d.drawRect(p.x, p.y, w, h);

        // draw median line
        Point p1 = transformer.realToScreen(quartiles[1], y - width / 2);
        Point p2 = transformer.realToScreen(quartiles[1], y + width / 2);
        g2d.drawLine(p1.x, p1.y, p2.x, p2.y);

        p1 = transformer.realToScreen(quartiles[2], y);
        p2 = transformer.realToScreen(maxIQ, y);
        g2d.drawLine(p1.x, p1.y, p2.x, p2.y);

        p1 = transformer.realToScreen(maxIQ, y - width / 4);
        p2 = transformer.realToScreen(maxIQ, y + width / 4);
        g2d.drawLine(p1.x, p1.y, p2.x, p2.y);

        p1 = transformer.realToScreen(quartiles[0], y);
        p2 = transformer.realToScreen(minIQ, y);
        g2d.drawLine(p1.x, p1.y, p2.x, p2.y);

        p1 = transformer.realToScreen(minIQ, y - width / 4);
        p2 = transformer.realToScreen(minIQ, y + width / 4);
        g2d.drawLine(p1.x, p1.y, p2.x, p2.y);

        for (Double x : highExtremeOutliers) {
            p = transformer.realToScreen(x, y);
            g2d.drawLine(p.x - 1, p.y - 1, p.x + 1, p.y + 1);
            g2d.drawLine(p.x - 1, p.y + 1, p.x + 1, p.y - 1);
        }

        for (Double x : lowExtremeOutliers) {
            p = transformer.realToScreen(x, y);
            g2d.drawLine(p.x - 1, p.y - 1, p.x + 1, p.y + 1);
            g2d.drawLine(p.x - 1, p.y + 1, p.x + 1, p.y - 1);
        }

        for (Double x : highOutliers) {
            p = transformer.realToScreen(x, y);
            g2d.drawOval(p.x, p.y, 2, 2);
        }

        for (Double x : lowOutliers) {
            p = transformer.realToScreen(x, y);
            g2d.drawOval(p.x, p.y, 2, 2);
        }

        Rectangle2D rec = g2d.getFontMetrics().getStringBounds(filename, g2d);
        int nameLen = filename.length();
        while (rec.getWidth() > 90 && nameLen > 1) {
            --nameLen;
            rec = g2d.getFontMetrics().getStringBounds(filename.substring(0, nameLen), g2d);
        }

        int ypos = transformer.realToScreen(0, y).y + (int) Math.round(rec.getHeight() / 2);
        g2d.drawString(filename.substring(0, nameLen), transformer.virtualToScreen(-90, 0).x, ypos);

        g2d.setPaint(oldPaint);
    }

    @Override
    public void render(Graphics g, CoordinateTransformer transformer) {
        this.transformer = transformer;

        renderYAxis(g);
        renderXAxis(g);

        Graphics2D g2d = (Graphics2D) g;
        printXLabel(0, g2d);
        printXLabel(transformer.getWidth(), g2d);
    }

}
