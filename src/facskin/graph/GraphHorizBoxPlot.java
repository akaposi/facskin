package facskin.graph;

import facskin.formats.Data;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Graph for a box plot. Used by {@link facskin.compare.ComparePanel}
 * @author ambi
 */
public class GraphHorizBoxPlot extends Graph {
    private int n;
    private List<double[]> quartiles;
    private List<Color> colors;
    private String[] filenames;
    private String paramName;
    private List<List<Double>> highOutliers;
    private List<List<Double>> lowOutliers;
    private List<List<Double>> highExtremeOutliers;
    private List<List<Double>> lowExtremeOutliers;
    private double[] max; //legnagyobb ertek (eddig rajzolunk)
    private double[] min; //legkisebb ertek (eddig rajzolunk)
    private double[] maxIQ; //legnagyobb ertek a kozepso 50%-bol
    private double[] minIQ; //legkisebb ertek a kozepso 50%-bol

    public GraphHorizBoxPlot(List<Data> datas, List<Color> colors, String paramName) {
        super();
        init(datas, colors, paramName);
    }

    private void init(List<Data> datas, List<Color> colors, String paramName) {
        this.colors = colors;
        renderer = new GraphHorizBoxPlotRenderer();

        n = datas.size();

        quartiles = new ArrayList<double[]>();
        for (int i = 0; i < n; i ++) {
            double[] e = new double[3];
            e[0] = datas.get(i).getQuantile(0.25);
            e[1] = datas.get(i).getQuantile(0.50);
            e[2] = datas.get(i).getQuantile(0.75);
            quartiles.add(e);
        }

        highOutliers = new ArrayList<List<Double>>();
        lowOutliers = new ArrayList<List<Double>>();
        highExtremeOutliers = new ArrayList<List<Double>>();
        lowExtremeOutliers = new ArrayList<List<Double>>();
        max = new double[n];
        min = new double[n];
        maxIQ = new double[n];
        minIQ = new double[n];
        for (int i = 0; i < n; i ++) {
            max[i] = Double.MIN_VALUE;
            min[i] = Double.MAX_VALUE;
            maxIQ[i] = quartiles.get(i)[2];
            minIQ[i] = quartiles.get(i)[0];
            double iqr = quartiles.get(i)[2] - quartiles.get(i)[0];
            List<Double> highLOutliers = new ArrayList<Double>();
            List<Double> lowLOutliers = new ArrayList<Double>();
            List<Double> highExtremeLOutliers = new ArrayList<Double>();
            List<Double> lowExtremeLOutliers = new ArrayList<Double>();
            for (int j = 0; j < datas.get(i).getDataLength(); j ++) {
                double x = datas.get(i).getData(j);
                if (x > quartiles.get(i)[2] + 3 * iqr) {
                    highExtremeLOutliers.add(x);
                } else if (x > quartiles.get(i)[2] + 1.5 * iqr) {
                    highLOutliers.add(x);
                } else if (x < quartiles.get(i)[0] - 3 * iqr) {
                    lowExtremeLOutliers.add(x);
                } else if (x < quartiles.get(i)[0] - 1.5 * iqr) {
                    lowLOutliers.add(x);
                } else if (x > maxIQ[i]) {
                    maxIQ[i] = x;
                } else if (x < minIQ[i]) {
                    minIQ[i] = x;
                }

                //ezek hatarozzak meg, hogy mekkorat rajzolunk:
                if (x > max[i] && x < quartiles.get(i)[2] + 4.5 * iqr)
                    max[i] = x;
                if (x < min[i] && x > quartiles.get(i)[0] - 4.5 * iqr)
                    min[i] = x;
            }
            highOutliers.add(highLOutliers);
            lowOutliers.add(lowLOutliers);
            highExtremeOutliers.add(highExtremeLOutliers);
            lowExtremeOutliers.add(lowExtremeLOutliers);
        }

        filenames = new String[n];
        for (int i = 0; i < n; i ++)
            filenames[i] = datas.get(i).getName();

        this.paramName = paramName;
    }

    @Override
    public void reloadTransformer() {
        double maxX = max[0];
        double minX = min[0];

        double maxY = n - 0.5;
        double minY = -0.5;

        for (int i = 1; i < n; i ++) {
            if (max[i] > maxX)
                maxX = max[i];
            if (min[i] < minX)
                minX = min[i];
        }

        transformer = new CoordinateTransformer(getWidth(), getHeight(),
            minX, minY, maxX, maxY, 100);
        this.repaint();
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        if (transformer == null)
            reloadTransformer();
        ((GraphHorizBoxPlotRenderer) renderer).render(graphics, transformer);
        for (int i = 0; i < n; i ++) {
            ((GraphHorizBoxPlotRenderer) renderer).renderSingleBoxPlot(graphics, transformer, n-i-1, colors.get(i), quartiles.get(i), filenames[i], highOutliers.get(i), lowOutliers.get(i), highExtremeOutliers.get(i), lowExtremeOutliers.get(i), maxIQ[i], minIQ[i]);
        }

        // label of x axis
        Rectangle2D rec = graphics.getFontMetrics().getStringBounds(paramName, graphics);
        int x = (transformer.virtualToScreen(0,0).x +
                transformer.virtualToScreen(transformer.getWidth(), 0).x) / 2 -
                (int) Math.round(rec.getWidth())/2;
        graphics.drawString(paramName, x, transformer.virtualToScreen(0,-20).y);
    }

    @Override
    public String name() {
        return paramName;
    }
}
