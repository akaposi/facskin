package facskin.graph;

import facskin.FacsKinApp;
import facskin.formats.KineticsData;
import facskin.kinetics.KineticsPanel;
import facskin.kinetics.ListColorScale;
import java.awt.Color;
import java.awt.Graphics;

/**
 * Graph showing median points and currently selected median functions
 * of a list of kinetics files in a {@link facskin.kinetics.KineticsPanel}
 * @author ambi
 */
public class GraphKinetics extends Graph {
    KineticsPanel father;

    public GraphKinetics(KineticsPanel father) {
        super();
        renderer = new GraphKineticsRenderer();
        this.father = father;
    }

    @Override
    public void reloadTransformer(){
        double maxY = Double.MIN_VALUE;
        double maxX = Double.MIN_VALUE;
        double minY = Double.MAX_VALUE;
        double minX = Double.MAX_VALUE;

        if (father.getDatas().isEmpty()) {
            maxY = 0;
            maxX = 0;
            minY = 0;
            minX = 0;
        }

        for (KineticsData data : father.getDatas()) {
            if (data.getMinX() < minX)
                minX = data.getMinX();
            if (data.getMinY() / data.getStandardizer() < minY)
                minY = data.getMinY() / data.getStandardizer();
            if (data.getMaxX() > maxX)
                maxX = data.getMaxX();
            if (data.getMaxY() / data.getStandardizer() > maxY)
                maxY = data.getMaxY() / data.getStandardizer();
        }
        transformer = new CoordinateTransformer(getWidth(), getHeight(),
            minX, minY, maxX, maxY);
        this.repaint();
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        if (transformer == null)
            reloadTransformer();
        for (int i = 0; i < father.getDatas().size(); i ++) {
            KineticsData data = father.getDatas().get(i);
            Color color = Color.GRAY;
            boolean wide = false;
            if (FacsKinApp.getShowDataByGroups()) {
                if (father.getGroupNumber(i) != -1) {
                    color = ListColorScale.getColor(father.getGroupNumber(i), father.getNumberOfGroups());
                    if (father.getSelectedGroup() == father.getGroupNumber(i))
                        wide = true;
                }
            } else if (FacsKinApp.getShowDataByPairs()) {
                if (father.getPairsNumber(i) != -1) {
                    if (FacsKinApp.getColorByGroups())
                        color = ListColorScale.getColor(father.getGroupNumber(i), father.getNumberOfGroups());
                    else
                        color = ListColorScale.getColor(father.getPairsNumber(i), father.getNumberOfPairsData());
                    if (father.getSelectedPairsData() == father.getPairsNumber(i)) wide = true;
                }
            } else {
                if (FacsKinApp.getColorByGroups())
                    color = ListColorScale.getColor(father.getGroupNumber(i), father.getNumberOfGroups());
                else
                    color = ListColorScale.getColor(i, father.getDatas().size());
                if (father.getSelectedKineticsData() == i || father.getSelectedPairsData() == i)
                    wide = true;
            }
            ((GraphKineticsRenderer) renderer).render(graphics, data, data.getFunction(father.getSelectedFunction()), transformer, color, wide);
        }
    }
}
