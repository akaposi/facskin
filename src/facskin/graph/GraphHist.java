package facskin.graph;

import facskin.fcs.GatePanel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Graph for a histogram of an FCS parameter. Used by {@link facskin.fcs.GatePanel}.
 * It also has support for selecting a range gate.
 * @author ambi
 */
public class GraphHist extends Graph {

    private int first = 0;
    private int second = 0;
    private GatePanel father;

    public GraphHist(GatePanel father) {
        super();
        this.father = father;
        renderer = new GraphHistRenderer(this);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
                mouseRelease(mouseEvent);
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                mousePress(mouseEvent);
            }
        });
    }

    private void mousePress(MouseEvent mouseEvent) {
        setFirst(mouseEvent.getPoint().x);
        repaint();
    }

    private void mouseRelease(MouseEvent mouseEvent) {
        setSecond(mouseEvent.getPoint().x);
        repaint();
    }

    @Override
    protected void mouseDrag(MouseEvent mouseEvent) {
        super.mouseDrag(mouseEvent);
        setSecond(mouseEvent.getPoint().x);
        repaint();
    }

    @Override
    public void reloadTransformer() {
        reloadTransformer(father.getSelectedParamX(), father.getSelectedParamY(), father.isLogX(), father.isLogY());
        this.repaint();
    }

    @Override
    public void reloadTransformer(int p1, int p2, boolean log1, boolean log2) { //p2-vel nem foglalkozunk
        if (p1 != -1) {
            double minX = father.getData().getHistogramData(p1, log1).getMinX();
            double minY = father.getData().getHistogramData(p1, log1).getMinY();
            double maxX = father.getData().getHistogramData(p1, log1).getMaxX();
            double maxY = father.getData().getHistogramData(p1, log1).getMaxY();
            setTransformer(new CoordinateTransformer(getWidth(), getHeight(), minX, minY, maxX, maxY));
        }
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);

        int p = father.getSelectedParamX();
        if (p == -1)
            p = 0;
        if (getTransformer() == null)
            reloadTransformer(p, 0, father.isLogX(), false);
        ((GraphHistRenderer) renderer).render(graphics, getTransformer(), p,
                father.isLogX());

        Color color = graphics.getColor();
        graphics.setColor(Color.RED);

        graphics.drawRect(getLeft(),
                0,
                getSize().width - getRight() - getLeft() - 1,
                getHeight() - 1);

        graphics.setColor(color);
    }

    public int getAbsoluteRight() {
        return Math.max(first, second);
    }

    public int getRight() {
        return getWidth() - Math.max(first, second);
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getLeft() {
        return Math.min(first, second);
    }

    public void setSecond(int second) {
        this.second = second;
        if (second == first) {
            first = 0;
            second = getWidth();
        }

    }

    @Override
    public void reset() {
        first = 0;
        second = getWidth();
        father.enableGateButton();
    }

    public GatePanel getFather() {
        return father;
    }

    @Override
    public String name() {
        return("GraphHist");
    }
}
