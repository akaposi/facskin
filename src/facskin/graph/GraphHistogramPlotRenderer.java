package facskin.graph;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.geom.Rectangle2D;

/**
 * Graph renderer of a histogram. Used by {@link facskin.compare.ComparePanel}.
 * @author ambi
 */
public class GraphHistogramPlotRenderer extends GraphRenderer {

    public GraphHistogramPlotRenderer() {
    }

    public void renderSingleHistogramPlot(Graphics g, CoordinateTransformer transformer, Color color, double[] count, double minX, double maxX, int n, String filename) {
        Graphics2D g2d = (Graphics2D) g;

        Paint oldPaint = g2d.getPaint();
        g2d.setPaint(color);

        Point p = transformer.realToScreen(minX, 0);

        double delta = (maxX - minX)/n;

        for (int i = 0; i < n; ++i) {
            Point p1 = transformer.realToScreen(minX + delta*(i+1), (double) count[i]);
            g2d.drawLine(p.x, p.y, p.x, p1.y);
            g2d.drawLine(p.x, p1.y, p1.x, p1.y);
            p = p1;
        }
        g2d.drawLine(p.x, p.y, p.x, transformer.realToScreen(0.0, 0.0).y);

        //// printing filename
        //Rectangle2D rec = g2d.getFontMetrics().getStringBounds(filename, g2d);
        //int xpos = transformer.realToScreen(x, 0).x - (int) Math.round(rec.getWidth()) / 2;
        //g2d.drawString(filename, xpos, transformer.virtualToScreen(0, -20).y);

        g2d.setPaint(oldPaint);
    }

    public void render(Graphics g, CoordinateTransformer transformer, String paramName) {
        this.transformer = transformer;

        // name of axis x
        Rectangle2D rec = g.getFontMetrics().getStringBounds(paramName, g);
        int x = (transformer.virtualToScreen(0,0).x +
                transformer.virtualToScreen(transformer.getWidth(), 0).x) / 2 -
                (int) Math.round(rec.getWidth())/2;
        g.drawString(paramName, x, transformer.virtualToScreen(0,-20).y);

        super.render(g, transformer);
    }

}
