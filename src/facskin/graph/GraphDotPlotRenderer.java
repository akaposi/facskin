package facskin.graph;

import facskin.formats.Data;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.geom.Rectangle2D;

/**
 * Graph renderer of a box plot. Used by {@link facskin.compare.ComparePanel}
 * @author ambi
 */
public class GraphDotPlotRenderer extends GraphRenderer {

    public GraphDotPlotRenderer() {
    }

    public void renderSingleDotPlot(Graphics g, CoordinateTransformer transformer, int y, Color color, Data d) {
        Graphics2D g2d = (Graphics2D) g;

        Paint oldPaint = g2d.getPaint();
        g2d.setPaint(color);

        for (int i = 0; i < d.getDataLength(); ++i) {
            double x = d.getData(i);
            Point p = transformer.realToScreen(x, y);
            int size = 3;
            g2d.drawLine(p.x - size, p.y - size, p.x + size, p.y + size);
            g2d.drawLine(p.x - size, p.y + size, p.x + size, p.y - size);
        }

        Rectangle2D rec = g2d.getFontMetrics().getStringBounds(d.getName(), g2d);
        int nameLen = d.getName().length();
        while (rec.getWidth() > 90 && nameLen > 1) {
            --nameLen;
            rec = g2d.getFontMetrics().getStringBounds(d.getName().substring(0, nameLen), g2d);
        }

        int ypos = transformer.realToScreen(0, y).y + (int) Math.round(rec.getHeight() / 2);
        g2d.drawString(d.getName().substring(0, nameLen), transformer.virtualToScreen(-90, 0).x, ypos);

        g2d.setPaint(oldPaint);
    }

    @Override
    public void render(Graphics g, CoordinateTransformer transformer) {
        this.transformer = transformer;

        renderYAxis(g);
        renderXAxis(g);

        Graphics2D g2d = (Graphics2D) g;
        printXLabel(0, g2d);
        printXLabel(transformer.getWidth(), g2d);
    }

}
