package facskin.graph;

import facskin.fcs.GateCheckBox;
import facskin.fcs.GatePanel;
import facskin.formats.GateData;
import facskin.formats.PlotList;
import java.awt.Color;
import java.awt.Stroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Graph representing a scatter plot for {@link facskin.fcs.GatePanel}. It
 * also has support for selecting a polygon or rectangular gate.
 * @author ambi
 */
public class GraphScatterPlot extends Graph {
    private List<Point> gatePoints = new ArrayList<Point>();
    private GatePanel father;
    
    //@author Daniel Herman
    private List<GateData> gatesPoints = new ArrayList<>();
    private GateCheckBox selectedGateCheckBox;
    //--
    
    private Point actualPoint = null; // the place of the mouse or null if there is no first point of the polygon
    private boolean almostReady = false; // true if the polygon is ready (is a circle) but the user hasn't clicked
    private boolean ready = false; // true if the polygon is ready (is a circle) and the user has clicked
    
    public GraphScatterPlot(GatePanel father) {
        super();
        this.father = father;
        renderer = new GraphScatterPlotRenderer(this);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
                mouseRelease(mouseEvent);
            }
        });
    }

    private void mouseRelease(MouseEvent evt) {
        if (evt.getButton() == MouseEvent.BUTTON1) {
            if (!ready) {
                if (actualPoint == null) // If we have just started
                    actualPoint = evt.getPoint();
                getGatePoints().add(actualPoint);
                if (almostReady && getGatePoints().size() > 2) {
                    ready = true; // now the gate is ready
                    father.enableGateButton();
                }
            }
        } else if (evt.getButton() == MouseEvent.BUTTON3) {
            gatePoints = new ArrayList<Point>();
            actualPoint = null;
            father.disableGateButton();
            ready = false;
        }
        repaint();
    }

    @Override
    protected void mouseMovement(MouseEvent mouseEvent) {
        super.mouseMovement(mouseEvent);
        if (getGatePoints().size() > 0 && !ready) {
            actualPoint = mouseEvent.getPoint();
            if (!father.isPolygonGate()) { // if we want a rectangular gate we only go horizontal or vertical
                if (Math.abs(getGatePoints().get(getGatePoints().size() - 1).x - actualPoint.x) >
                    Math.abs(getGatePoints().get(getGatePoints().size() - 1).y - actualPoint.y)) {
                    actualPoint.y = getGatePoints().get(getGatePoints().size() - 1).y;
                    if (Math.abs(actualPoint.x - getGatePoints().get(0).x) < 13)
                        actualPoint.x = getGatePoints().get(0).x;
                } else {
                    actualPoint.x = getGatePoints().get(getGatePoints().size() - 1).x;
                    if (Math.abs(actualPoint.y - getGatePoints().get(0).y) < 13)
                        actualPoint.y = getGatePoints().get(0).y;
                }
            }
            if (actualPoint.distance(getGatePoints().get(0).x, getGatePoints().get(0).y) < 10) {
                actualPoint = getGatePoints().get(0);
                father.enableGateButton();
                almostReady = true;
            } else {
                father.disableGateButton();
                almostReady = false;
            }
            repaint();
        }
    }

    @Override
    protected void mouseDrag(MouseEvent mouseEvent) {
        super.mouseDrag(mouseEvent);
        if (getGatePoints().size() > 0 && !ready) {
            actualPoint = mouseEvent.getPoint();
            if (actualPoint.distance(getGatePoints().get(0).x, getGatePoints().get(0).y) < 10) {
                actualPoint = getGatePoints().get(0);
                father.enableGateButton();
                almostReady = true;
            } else {
                father.disableGateButton();
                almostReady = false;
            }
            repaint();
        }
    }

    @Override
    public void reloadTransformer() {
        reloadTransformer(father.getSelectedParamX(), father.getSelectedParamY(), father.isLogX(), father.isLogY());
        this.repaint();
    }

    @Override
    public void reloadTransformer(int p1, int p2, boolean log1, boolean log2) {
        if (p1 != -1 && p2 != -1 && p2 < father.getData().getMinValues(log2).length) {
            double minX = father.getData().getMinValues(log1)[p1];
            double minY = father.getData().getMinValues(log2)[p2];
            double maxX = father.getData().getMaxValues(log1)[p1];
            double maxY = father.getData().getMaxValues(log2)[p2];
            setTransformer(new CoordinateTransformer(getWidth(), getHeight(), minX, minY, maxX, maxY));
        }
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);     
        int p1 = father.getSelectedParamX();
        int p2 = father.getSelectedParamY();
        if (p1 == -1)
            p1 = 0;
        if (p2 == -1)
            p2 = 0;
        
        if (getTransformer() == null)
            reloadTransformer(p1, p2, father.isLogX(), father.isLogY());

        ((GraphScatterPlotRenderer) renderer).render(graphics, getTransformer(), p1, p2,
                father.isLogX(), father.isLogY(), father.isShowMedianSelected());
           
        Graphics2D g2d = (Graphics2D) graphics;
        
        Color color = graphics.getColor();
        graphics.setColor(Color.RED);
        
        if (almostReady || ready)
            graphics.setColor(Color.GREEN);

        if (getGatePoints().size() == 1) {
            Point p = getGatePoints().get(0);
            g2d.drawLine(p.x, p.y, actualPoint.x, actualPoint.y);
        } else {
            Point last = null;
            for (Point p : gatePoints) {
                if (last != null){
                    g2d.drawLine(last.x, last.y, p.x, p.y);
                }
                last = p;
            }
            if (last != null && actualPoint != null)
                g2d.drawLine(last.x, last.y, actualPoint.x, actualPoint.y);
        }
        graphics.setColor(color);
        
    }
    
    //@author Daniel Herman
    /*public void drawAfterReset(GraphScatterPlotRenderer gpr) {
        var graphics = gpr.graphics; 
        var g2d = (Graphics2D) gpr.graphics;
        
        int scaleX = father.getData().getPlotScale();
        int scaleY = father.getData().getPlotScale();
        int width = getTransformer().realToVirtualDistX(gpr.swidth());
        int height = getTransformer().realToVirtualDistY(gpr.sheight());
   
        for (GateData p : gatesPoints) {*/

            /*if ( selectedGateCheckBox != null && !selectedGateCheckBox.data().equals(p) )
                continue;*/

           //graphics.setColor(p.getColor());
           /*if ( selectedGateCheckBox != null && selectedGateCheckBox.data().equals(p) ) {
                PlotList list = father.getData().getGatedPlotData(father.getSelectedParamX(), father.getSelectedParamY(), father.isLogX(), father.isLogY());
                //System.out.println("gatedlist" + list.toString());
                System.out.println(father.getData().getEventCount());
                System.out.println(list.size());
                for (int x = 0; x < scaleX; x ++) {
                     for (int y = 0; y < scaleY; y ++) {
                         Point start = gpr.convertPoint(x, y);
                         int value = (int) Math.round(Math.pow(1 - list.getNormalized(x, y), 5) * 255);
                         //System.out.println(list.getNormalized(x, y));
                         g2d.setPaint(new Color(value, 0, 0));
                         g2d.fillRect(start.x, start.y - height, width, height);
                     }
                 }
                 //System.out.println(list.toString());
           }*/
/*
           for (int i = 0; i<p.getPoints1().length-1;++i) {

               Point start = gpr.transformer.realToScreen( p.getPoints1()[i], p.getPoints2()[i]);
               Point end = gpr.transformer.realToScreen( p.getPoints1()[i+1], p.getPoints2()[i+1]);

               g2d.drawLine(start.x, start.y, end.x, end.y);

           }*/
      /* }
    }*/
    
    //@author Daniel Herman
    public void addGatePoints(GateData data) {
        gatesPoints.add(data);
    }
    
    public void setSelectedGateCheckBox(GateCheckBox selectedGateCheckBox) {
        this.selectedGateCheckBox = selectedGateCheckBox;
    }
    //--
    
    public List<Point> getGatePoints() {
        return gatePoints;
    }
    
    @Override
    public void reset() {
        gatePoints = new ArrayList<Point>();
        actualPoint = null;
        almostReady = false;
        ready = false;
        father.disableGateButton();
    }

    public GatePanel getFather() {
        return this.father;
    }
    
    @Override
    public String name() {
        return("GraphPlot");
    }
}
