package facskin.graph;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.util.List;

/**
 * Graph renderer of a box plot. Used by {@link facskin.compare.ComparePanel}
 * @author ambi
 */
public class GraphBoxPlotRenderer extends GraphRenderer {

    public GraphBoxPlotRenderer() {
    }

    public void renderSingleBoxPlot(Graphics g, CoordinateTransformer transformer, int x, Color color, double[] quartiles, String filename, List<Double> highOutliers, List<Double> lowOutliers, List<Double> highExtremeOutliers, List<Double> lowExtremeOutliers, double maxIQ, double minIQ) {
        Graphics2D g2d = (Graphics2D) g;

        Paint oldPaint = g2d.getPaint();
        g2d.setPaint(color);

        double width = 0.4;

        Point p = transformer.realToScreen(x - width / 2, quartiles[2]);
        int w = transformer.realToScreen(x + width / 2, 0).x - p.x;
        int h = transformer.realToScreen(0, quartiles[0]).y - p.y;
        g2d.drawRect(p.x, p.y, w, h);

        // draw median line
        Point p1 = transformer.realToScreen(x - width / 2, quartiles[1]);
        Point p2 = transformer.realToScreen(x + width / 2, quartiles[1]);
        g2d.drawLine(p1.x, p1.y, p2.x, p2.y);

        p1 = transformer.realToScreen(x, quartiles[2]);
        p2 = transformer.realToScreen(x, maxIQ);
        g2d.drawLine(p1.x, p1.y, p2.x, p2.y);

        p1 = transformer.realToScreen(x - width / 4, maxIQ);
        p2 = transformer.realToScreen(x + width / 4, maxIQ);
        g2d.drawLine(p1.x, p1.y, p2.x, p2.y);

        p1 = transformer.realToScreen(x, quartiles[0]);
        p2 = transformer.realToScreen(x, minIQ);
        g2d.drawLine(p1.x, p1.y, p2.x, p2.y);

        p1 = transformer.realToScreen(x - width / 4, minIQ);
        p2 = transformer.realToScreen(x + width / 4, minIQ);
        g2d.drawLine(p1.x, p1.y, p2.x, p2.y);

        for (Double y : highExtremeOutliers) {
            p = transformer.realToScreen(x, y);
            g2d.drawLine(p.x - 1, p.y - 1, p.x + 1, p.y + 1);
            g2d.drawLine(p.x - 1, p.y + 1, p.x + 1, p.y - 1);
        }

        for (Double y : lowExtremeOutliers) {
            p = transformer.realToScreen(x, y);
            g2d.drawLine(p.x - 1, p.y - 1, p.x + 1, p.y + 1);
            g2d.drawLine(p.x - 1, p.y + 1, p.x + 1, p.y - 1);
        }

        for (Double y : highOutliers) {
            p = transformer.realToScreen(x, y);
            g2d.drawOval(p.x, p.y, 2, 2);
        }

        for (Double y : lowOutliers) {
            p = transformer.realToScreen(x, y);
            g2d.drawOval(p.x, p.y, 2, 2);
        }

        // print filename
        Rectangle2D rec = g2d.getFontMetrics().getStringBounds(filename, g2d);
        int xpos = transformer.realToScreen(x, 0).x - (int) Math.round(rec.getWidth()) / 2;
        g2d.drawString(filename, xpos, transformer.virtualToScreen(0, -20).y);

        g2d.setPaint(oldPaint);
    }

    @Override
    public void render(Graphics g, CoordinateTransformer transformer) {
        this.transformer = transformer;
        renderYAxis(g);
        renderXAxis(g);

        Graphics2D g2d = (Graphics2D) g;
        printYLabel(0, g2d);
        printYLabel(transformer.getHeight(), g2d);
    }

}
