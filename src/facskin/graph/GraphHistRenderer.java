package facskin.graph;

import facskin.formats.HistogramList;
import java.awt.Graphics;
import java.awt.Point;

/**
 * Graph renderer for a histogram of an FCS parameter. Used by {@link facskin.fcs.GatePanel}.
 * @author ambi
 */
public class GraphHistRenderer extends GraphRenderer {
    GraphHist father;

    public GraphHistRenderer(GraphHist father) {
        this.father = father;
    }

    public void render(Graphics g2d, CoordinateTransformer ct, int p, boolean log){
        super.render(g2d, ct);
        renderValues(g2d, p, log);
    }

    private void renderValues(Graphics g2d, int p, boolean log) {
        HistogramList list = father.getFather().getData().getHistogramData(p, log);
        int scale = father.getFather().getData().getHistogramScale();
        double xoffset = list.getMinX();
        double yoffset = list.getMinY();
        double maxX = list.getMaxX();
        double maxY = list.getMaxY();
        double dwidth = maxX - xoffset;
        double dheight = maxY - yoffset;

        for (int i = 0; i < scale; i ++){
            Point start = transformer.realToScreen(xoffset + i * (dwidth / scale), yoffset);
            int width = transformer.realToVirtualDistX(dwidth / scale);
            int height = transformer.realToVirtualDistY(list.get(i));
            g2d.drawRect(start.x, start.y - height, width, height);
        }

//        // name o faxis x
//        Rectangle2D rec = g2d.getFontMetrics().getStringBounds(p.toString(),g2d);
//        int x = (transformer.virtualToScreen(0,0).x+transformer.virtualToScreen(transformer.getWidth(), 0).x) / 2 - (int)Math.round(rec.getWidth());
//        g2d.drawString(p.toString(),x,transformer.virtualToScreen(0,-20).y);
    }
}
