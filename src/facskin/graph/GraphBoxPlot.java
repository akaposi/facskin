package facskin.graph;

import facskin.formats.Data;
import facskin.math.MyDecimalFormat;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Graph for a box plot. Used by {@link facskin.compare.ComparePanel}
 * @author ambi
 */
public class GraphBoxPlot extends Graph {
    private int n;
    private List<double[]> quartiles;
    private List<Color> colors;
    private List<Double> values;
    private String[] filenames;
    private String paramName;
    private List<List<Double>> highOutliers;
    private List<List<Double>> lowOutliers;
    private List<List<Double>> highExtremeOutliers;
    private List<List<Double>> lowExtremeOutliers;
    private double[] max; //legnagyobb ertek (eddig rajzolunk)
    private double[] min; //legkisebb ertek (eddig rajzolunk)
    private double[] maxIQ; //legnagyobb ertek a kozepso 50%-bol
    private double[] minIQ; //legkisebb ertek a kozepso 50%-bol
    private List<Double> yLines;

    public GraphBoxPlot(List<Data> datas, List<Color> colors, String paramName) {
        super();
        init(datas, colors, paramName, new ArrayList<Double>(), new ArrayList<Double>());
    }
    
    public GraphBoxPlot(List<Data> datas, List<Color> colors, String paramName, List<Double> values, List<Double> yLines) {
        super();
        init(datas, colors, paramName, values, yLines);
    }

    private void init(List<Data> datas, List<Color> colors, String paramName, List<Double> values, List<Double> yLines) {
        this.colors = colors;
        this.values = values;
        this.yLines = yLines;
        renderer = new GraphBoxPlotRenderer();

        n = datas.size();

        quartiles = new ArrayList<double[]>();
        for (int i = 0; i < n; i ++) {
            double[] e = new double[3];
            e[0] = datas.get(i).getQuantile(0.25);
            e[1] = datas.get(i).getQuantile(0.50);
            e[2] = datas.get(i).getQuantile(0.75);
            quartiles.add(e);
        }

        highOutliers = new ArrayList<List<Double>>();
        lowOutliers = new ArrayList<List<Double>>();
        highExtremeOutliers = new ArrayList<List<Double>>();
        lowExtremeOutliers = new ArrayList<List<Double>>();
        max = new double[n];
        min = new double[n];
        maxIQ = new double[n];
        minIQ = new double[n];
        for (int i = 0; i < n; i ++) {
            max[i] = Double.MIN_VALUE;
            min[i] = Double.MAX_VALUE;
            maxIQ[i] = quartiles.get(i)[2];
            minIQ[i] = quartiles.get(i)[0];
            double iqr = quartiles.get(i)[2] - quartiles.get(i)[0];
            List<Double> highLOutliers = new ArrayList<Double>();
            List<Double> lowLOutliers = new ArrayList<Double>();
            List<Double> highExtremeLOutliers = new ArrayList<Double>();
            List<Double> lowExtremeLOutliers = new ArrayList<Double>();
            for (int j = 0; j < datas.get(i).getDataLength(); j ++) {
                double x = datas.get(i).getData(j);
                if (x > quartiles.get(i)[2] + 3 * iqr) {
                    highExtremeLOutliers.add(x);
                } else if (x > quartiles.get(i)[2] + 1.5 * iqr) {
                    highLOutliers.add(x);
                } else if (x < quartiles.get(i)[0] - 3 * iqr) {
                    lowExtremeLOutliers.add(x);
                } else if (x < quartiles.get(i)[0] - 1.5 * iqr) {
                    lowLOutliers.add(x);
                } else if (x > maxIQ[i]) {
                    maxIQ[i] = x;
                } else if (x < minIQ[i]) {
                    minIQ[i] = x;
                }

                //ezek hatarozzak meg, hogy mekkorat rajzolunk:
                if (x > max[i] && x < quartiles.get(i)[2] + 4.5 * iqr)
                    max[i] = x;
                if (x < min[i] && x > quartiles.get(i)[0] - 4.5 * iqr)
                    min[i] = x;
            }
            highOutliers.add(highLOutliers);
            lowOutliers.add(lowLOutliers);
            highExtremeOutliers.add(highExtremeLOutliers);
            lowExtremeOutliers.add(lowExtremeLOutliers);
        }

        filenames = new String[n];
        for (int i = 0; i < n; i ++)
            filenames[i] = datas.get(i).getName();

        this.paramName = paramName;
    }

    @Override
    public void reloadTransformer() {
        double maxY = max[0];
        double minY = min[0];

        double maxX = n - 0.5;
        double minX = -0.5;

        for (int i = 1; i < n; i ++) {
            if (max[i] > maxY)
                maxY = max[i];
            if (min[i] < minY)
                minY = min[i];
        }

        transformer = new CoordinateTransformer(getWidth(), getHeight(),
            minX, minY, maxX, maxY);
        this.repaint();
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        if (transformer == null)
            reloadTransformer();
        ((GraphBoxPlotRenderer) renderer).render(graphics, transformer);
        for (int i = 0; i < n; i ++) {
            ((GraphBoxPlotRenderer) renderer).renderSingleBoxPlot(graphics, transformer, i, colors.get(i), quartiles.get(i), filenames[i], highOutliers.get(i), lowOutliers.get(i), highExtremeOutliers.get(i), lowExtremeOutliers.get(i), maxIQ[i], minIQ[i]);
        }
        if (values.size() == n) {
            for (int i = 0; i < n; ++i) {
                String str = MyDecimalFormat.format(values.get(i));
                Rectangle2D rec = graphics.getFontMetrics().getStringBounds(str, graphics);
                graphics.drawString(str, transformer.realToScreen(i, 0).x - (int) Math.round(rec.getWidth()) / 2, transformer.virtualToScreen(0, -40).y);
            }

            String str = "T(chi): ";
            Rectangle2D rec = graphics.getFontMetrics().getStringBounds(str, graphics);
            graphics.drawString(str, transformer.virtualToScreen(0, 0).x - (int) Math.round(rec.getWidth()), transformer.virtualToScreen(0, -40).y);
        }

        //y tengely elnevezese
        Rectangle2D rec = graphics.getFontMetrics().getStringBounds(paramName, graphics);
        graphics.drawString(paramName, transformer.virtualToScreen(-45, 0).x, (int) Math.round(rec.getHeight()));

        if (yLines.size() > 0) {
            Graphics2D g2d = (Graphics2D) graphics;
            Paint oldPaint = g2d.getPaint();
            g2d.setPaint(Color.GRAY);
            for (int i = 0; i < yLines.size(); ++i) {
                Point p1 = transformer.realToScreen(0, yLines.get(i));
                Point p2 = transformer.virtualToScreen(0, 0);
                //Point p2 = transformer.realToScreen(yLines.get(i), maxY);
                g2d.drawLine(p2.x, p1.y, p2.x-5, p1.y);
            }

            g2d.setPaint(oldPaint);
        }
    }

    public void setYLines(List<Double> yLines) {
        this.yLines = yLines;
    }

    @Override
    public String name() {
        return paramName;
    }
}
