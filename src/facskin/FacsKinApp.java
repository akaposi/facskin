package facskin;

import java.io.File;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

/**
 * The main class of the application.
 */
public class FacsKinApp extends SingleFrameApplication {

    private String[] filesToOpen = null;
    private static FacsKinView facsKinView;

    //----------------------------------------------------------------
    // GLOBAL SETTINGS (Only for storage of settings. The getters and setters don't have any side effects.)
    //----------------------------------------------------------------

    private static char decimalSeparator = '.';
    public static char getDecimalSeparator() { return decimalSeparator; }
    public static void setDecimalSeparator(char decimalSeparator) { FacsKinApp.decimalSeparator = decimalSeparator; }

    public static char getListSeparator() { if (decimalSeparator == ',') return ';'; else return ','; }

    private static boolean standardisation = true;
    public static boolean getStandardisation() { return standardisation; }
    public static void setStandardisation(boolean standardisation) { FacsKinApp.standardisation = standardisation; }

    private static double maxTime = 600;
    public static double getMaxTime() { return maxTime; }
    public static void setMaxTime(double maxTime) { FacsKinApp.maxTime = maxTime; }

    private static double gapBeforeAppend = 30;
    public static double getGapBeforeAppend() { return gapBeforeAppend; }
    public static void setGapBeforeAppend(double gapBeforeAppend) { FacsKinApp.gapBeforeAppend = gapBeforeAppend; }

    private static boolean drawFunctionUntilMeasurementEnd = true;
    public static boolean getDrawFunctionUntilMeasurementEnd() { return drawFunctionUntilMeasurementEnd; }
    public static void setDrawFunctionUntilMeasurementEnd(boolean drawFunctionUntilMeasurementEnd) { FacsKinApp.drawFunctionUntilMeasurementEnd = drawFunctionUntilMeasurementEnd; }

    private static int numberofBreaksinHistogramPlot = 50;
    public static int getNumberofBreaksinHistogramPlot() { return numberofBreaksinHistogramPlot; }
    public static void setNumberofBreaksinHistogramPlot(int n) { numberofBreaksinHistogramPlot = n; }

    private static int numberofBreaksinKineticsPlot = 1000;
    public static int getNumberofBreaksinKineticsPlot() { return numberofBreaksinKineticsPlot; }
    public static void setNumberofBreaksinKineticsPlot(int n) { numberofBreaksinKineticsPlot = n; }
    
    private static boolean useOnlyMedians = false;
    public static boolean getUseOnlyMedians() { return useOnlyMedians; }
    public static void setUseOnlyMedians(boolean newValue) { useOnlyMedians = newValue; }

    private static boolean showDataByGroups = false;
    public static boolean getShowDataByGroups() { return showDataByGroups; }
    public static void setShowDataByGroups(boolean newValue) { showDataByGroups = newValue; }

    private static boolean showDataByPairs = false;
    public static boolean getShowDataByPairs() { return showDataByPairs; }
    public static void setShowDataByPairs(boolean newValue) { showDataByPairs = newValue; }

    private static boolean colorByGroups = false;
    public static boolean getColorByGroups() { return colorByGroups; }
    public static void setColorByGroups(boolean newValue) { colorByGroups = newValue; }

    private static double baselineTime = 10.0;
    public static double getBaselineTime() { return baselineTime; }
    public static void setBaselineTime(double baselineTime) { FacsKinApp.baselineTime = baselineTime; }


    //-------------------------
    // END OF GLOBAL SETTINGS
    //-------------------------

    @Override
    protected void initialize(String[] args) {
        if (args.length > 1)
            if (args[0].equals("-open")) {
                filesToOpen = new String[args.length - 1];
                for (int i = 0; i < args.length - 1; i ++)
                    filesToOpen[i] = args[i + 1];
            }
    }

    /**
     * At startup create and show the main frame of the application.
     */
    @Override
    protected void startup() {
        facsKinView = new FacsKinView(this);
        show(facsKinView);
        if (filesToOpen == null)
            facsKinView.showFileOpenPanel();
        else {
            facsKinView.openFile(filesToOpen[0]);
            //waiting 1 second
            long t0, t1;t0 = System.currentTimeMillis(); do { t1 = System.currentTimeMillis(); } while (t1-t0 < 1000);
            for (int i = 1; i < filesToOpen.length; i ++)
                facsKinView.openFile(filesToOpen[i]);
        }
    }

    /**
     * This method is to initialize the specified window by injecting resources.
     * Windows shown in our application come fully initialized from the GUI
     * builder, so this additional configuration is not needed.
     */
    @Override
    protected void configureWindow(java.awt.Window root) {
    }

    /**
     * A convenient static getter for the application instance.
     * @return the instance of FacsKinApp
     */
    public static FacsKinApp getApplication() {
        return Application.getInstance(FacsKinApp.class);
    }

    /**
     * Main method launching the application. It checks whether there is an
     * already running instance by calling {@link AppicationInstanceManager}'s
     * registerInstance() method.
     */
    public static void main(String[] args) {
        if (args.length > 1)
            if (args[0].equals("-open"))
                for (int i = 1; i < args.length; i ++)
                    args[i] = (new File(args[i])).getAbsolutePath();


        if (!ApplicationInstanceManager.registerInstance(args)) {
            // instance already running.
            System.out.println("Another instance of this application is already running.  Exiting.");
            System.exit(0);
        }
        ApplicationInstanceManager.setApplicationInstanceListener(new ApplicationInstanceListener() {
            @Override
            public void newInstanceCreated(String[] args) {
                System.out.println("New instance detected, args length=" + args.length);
                if (args.length > 1)
                    if (args[0].equals("-open")) {
                        if (facsKinView.getFileChooser().isVisible())
                            facsKinView.getFileChooser().cancelSelection();
                        facsKinView.openFile(args[1]);
                        //TODO: nem szabadna, hogy ilyen kelljen:
                        //waiting 1 second
                        try { Thread.sleep(1000); } catch (InterruptedException ex) {}
                        //long t0, t1;t0 = System.currentTimeMillis(); do { t1 = System.currentTimeMillis(); } while (t1-t0 < 1000);
                        for (int i = 2; i < args.length; i ++) {
                            facsKinView.openFile(args[i]);
                        }
                    }
            }
        });
        launch(FacsKinApp.class, args);
    }
}
