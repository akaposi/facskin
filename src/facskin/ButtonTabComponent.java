package facskin;

import javax.swing.*;
import javax.swing.plaf.basic.BasicButtonUI;
import java.awt.*;
import java.awt.event.*;

/**
 * Component to be used as as tabComponent; Contains a JLabel to show the
 * text and a JButton to close the tab it belongs to. The background
 * color can be set in the constructor.
 * <a href="http://weblogs.java.net/blog/alexfromsun/archive/2005/11/tabcomponents_i_1.html">Source</a>.
 */
public class ButtonTabComponent extends JPanel {

    private final JTabbedPane pane;
    private final JLabel label;
    private final JButton button = new TabButton();

    private FacsKinView parent;

    public ButtonTabComponent(String title, JTabbedPane pane, FacsKinView facsKinView, Color color) {
        //unset default FlowLayout' gaps
        super(new FlowLayout(FlowLayout.LEFT, 0, 0));
        if (pane == null) {
            throw new NullPointerException("TabbedPane is null");
        }
        this.pane = pane;
        setOpaque(false);
        label = new JLabel(title);

        add(label);
        //add more space between the label and the button
        label.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
        label.setForeground(color);
        add(button);
        //add more space to the top of the component
        setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));

        parent = facsKinView;
    }

    private class TabButton extends JButton implements ActionListener {
        public TabButton() {
            int size = 17;
            setPreferredSize(new Dimension(size, size));
            setToolTipText("close this tab");
            //Make the button looks the same for all Laf's
            setUI(new BasicButtonUI());
            //Make it transparent
            setContentAreaFilled(false);
            //No need to be focusable
            setFocusable(false);
            setBorder(BorderFactory.createEtchedBorder());
            setBorderPainted(false);
            //Making nice rollover effect
            //we use the same listener for all buttons
            addMouseListener(buttonMouseListener);
            setRolloverEnabled(true);
            //Close the proper tab by clicking the button
            addActionListener(this);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int i = pane.indexOfTabComponent(ButtonTabComponent.this);
            if (i != -1) {
                pane.remove(i);
            }
            if (parent != null)
                parent.tabRemoved();
        }

        //we don't want to update UI for this button
        @Override
        public void updateUI() {
        }

        //paint the cross
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g;
            Stroke stroke = g2.getStroke();
            //shift the image for pressed buttons
            if (!getModel().isPressed()) {
                g2.translate(-1, -1);
            }
            g2.setStroke(new BasicStroke(1));
            g.setColor(Color.BLACK);
            if (getModel().isRollover()) {
                g.setColor(Color.MAGENTA);
            }
            int delta = 6;
            g.drawLine(delta, delta, getWidth() - delta - 1, getHeight() - delta - 1);
            g.drawLine(getWidth() - delta - 1, delta, delta, getHeight() - delta - 1);
            //leave the graphics unchanged
            if (!getModel().isPressed()) {
                g.translate(1, 1);
            }
            g2.setStroke(stroke);
        }
    }

    private final static MouseListener buttonMouseListener = new MouseAdapter() {
        @Override
        public void mouseEntered(MouseEvent e) {
            Component component = e.getComponent();
            if (component instanceof AbstractButton) {
                AbstractButton button = (AbstractButton) component;
                button.setBorderPainted(true);
            }
        }

        @Override
        public void mouseExited(MouseEvent e) {
            Component component = e.getComponent();
            if (component instanceof AbstractButton) {
                AbstractButton button = (AbstractButton) component;
                button.setBorderPainted(false);
            }
        }
    };
}
