package facskin;

/**
 * An interface providing a function {@link #readyWithPainting()} which
 * is implemented by panels such as {@link FcsPanel} and is called by
 * {@link FacsKinView} when a long process (like reading a file) ended. It
 * signals that the UI should be repainted because the process ended.
 * @author ambi
 */
public interface FacsKinPanel {
    public void readyWithPainting();
}
