package facskin.kinetics;

import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 * ComboBoxModel for selecting pairs for kinetics files.
 * @author ambi
 */
public class PairsComboBoxModel implements ComboBoxModel {
    private KineticsPanel father;
    protected String selected = null;

    public PairsComboBoxModel(KineticsPanel father) {
        this.father = father;
    }

    @Override
    public void setSelectedItem(Object object) {
        selected = (String) object;
    }

    @Override
    public Object getSelectedItem() {
        return selected;
    }

    @Override
    public int getSize() {
        return father.getDatas().size() + 1;
    }

    @Override
    public Object getElementAt(int i) {
        if (i == 0)
            return "";
        else
            return father.getDatas().get(i - 1).getFilename();
    }

    @Override
    public void addListDataListener(ListDataListener listDataListener) {
    }

    @Override
    public void removeListDataListener(ListDataListener listDataListener) {
    }
}
