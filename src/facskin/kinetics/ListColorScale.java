package facskin.kinetics;

import java.awt.Color;

/**
 * Class for implementing a colour scale with arbitrary number of colours.
 * @author ambi
 */
public class ListColorScale {
    public static Color getColor(int index, int size) {
        double y;
        if ((size - 1) > 0)
            y = index * 2 / ((double) (size - 1));
        else
            y = 0;
        Color color;
        if (y <= 1)
            color = new Color((int) (255 * (1 - y)), (int) (255 * y), 0);
        else
            color = new Color(0, (int) (255 * (2 - y)), (int) (255 * (y - 1)));
        return color;
    }
}
