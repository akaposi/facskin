package facskin.kinetics;

import java.awt.Component;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * Table cell renderer for a ComboBox having support for setting the background.
 * @author ambi
 */
public class ComboBoxCellRenderer extends JComboBox implements TableCellRenderer {
    private int selectedRow = -1;

    public ComboBoxCellRenderer(ComboBoxModel comboBoxModel) {
        super(comboBoxModel);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        setSelectedItem((String) value);
        if (row == selectedRow) {
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
        }  else {
            setForeground(table.getForeground());
            setBackground(table.getBackground());
        }
        return this;
    }

    void setSelectedRow(int selectedRow) {
        this.selectedRow = selectedRow;
    }
}
