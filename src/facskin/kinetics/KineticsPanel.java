package facskin.kinetics;

import facskin.FacsKinApp;
import facskin.FacsKinPanel;
import facskin.FacsKinView;
import facskin.TaskWithProgress;
import facskin.formats.ComparisonData;
import facskin.formats.Data;
import facskin.formats.KineticsData;
import facskin.graph.Graph;
import facskin.graph.GraphBoxPlot;
import facskin.graph.GraphDotPlot;
import facskin.graph.GraphHistogramPlot;
import facskin.graph.GraphHorizBoxPlot;
import facskin.graph.GraphKinetics;
import facskin.io.KineticsZip;
import facskin.math.Functions;
import facskin.math.KruskalWallis;
import facskin.math.ProbabilityBinning;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;
import org.jdesktop.application.Action;
import org.jdesktop.application.Task;

/**
 * Panel for showing and manipulating a list of kinetics files. It has a
 * plot, a table and some buttons for grouping and comparing.
 * @author ambi
 */
public class KineticsPanel extends javax.swing.JPanel implements FacsKinPanel, ClipboardOwner {

    private FacsKinView father;

    // containers for kinetics data

    private List<KineticsData> datas = new ArrayList<KineticsData>();
    private List<String> groups = new ArrayList<String>();
    private List<KineticsData> pairs = new ArrayList<KineticsData>();

    // containers for paired data

    private List<List<Double>> pairsValues = new ArrayList<List<Double>>();
    private List<String> pairsNames = new ArrayList<String>();
    private List<String> pairsGroups = new ArrayList<String>();
    private List<String> pairsParamNames = new ArrayList<String>();
    private List<Integer> pairsOriginalPosition = new ArrayList<Integer>();

    // containers for grouped data (grouped first by kinetics, then by param)

    private List<List<Data>> groupedDatas = new ArrayList<List<Data>>();
    private List<String> groupedGroupNames = new ArrayList<String>();

    // containers for grouped data for comparison (grouped first by param, then by kinetics)

    private List<List<Data>> compareDatas = new ArrayList<List<Data>>();
    private List<String> compareGroupNames = new ArrayList<String>();

    // current selection

    private SelectionData sel = new SelectionData();

    // GUI
    
    private KineticsTableModel kineticsTableModel;

    private String newGroupString = "(new)";

    private StringCellRenderer   kineticsTableCellRenderer = new StringCellRenderer(true);
    private StringCellRenderer   groupTableCellRenderer    = new StringCellRenderer(false);
    private StringCellRenderer   pairsTableCellRenderer    = new StringCellRenderer(false);
    private ComboBoxCellRenderer comboBoxCellRenderer      = new ComboBoxCellRenderer(new GroupComboBoxModel(this, newGroupString));
    private ComboBoxCellRenderer pairsComboBoxCellRenderer = new ComboBoxCellRenderer(new PairsComboBoxModel(this));

    private JTable groupTable = new JTable();
    private JTable pairsTable = new JTable();

    private String currentTable = "kineticsTable";
    // TODO: replace FacsKinApp.getShowDataByGroups etc. with this

    //private String currentPlot = "boxplot";
    private String kineticsPlot = "horizboxplot";
    private String groupPlot = "dotplot";

    /** Creates new form KineticsPanel */
    public KineticsPanel(FacsKinView father) {
        this.father = father;
        kineticsTableModel = new KineticsTableModel(this, datas, sel, groups, pairs, newGroupString);
        initComponents();

        kineticsTable.setDefaultRenderer(String.class, kineticsTableCellRenderer);

        kineticsTable.setColumnModel(new DefaultTableColumnModel());
        kineticsTable.getColumnModel().addColumn(new TableColumn(0, 100));
        kineticsTable.getColumnModel().addColumn(new TableColumn(1, 50));
        kineticsTable.getColumnModel().addColumn(new TableColumn(2, 100));

        kineticsTable.getColumnModel().getColumn(1).setCellRenderer(comboBoxCellRenderer);
        kineticsTable.getColumnModel().getColumn(1).setCellEditor(new DefaultCellEditor(new JComboBox(new GroupComboBoxModel(this, newGroupString))));

        kineticsTable.getColumnModel().getColumn(2).setCellRenderer(pairsComboBoxCellRenderer);
        kineticsTable.getColumnModel().getColumn(2).setCellEditor(new DefaultCellEditor(new JComboBox(new PairsComboBoxModel(this))));

        for (int i = 3; i < 9; i ++)
            kineticsTable.getColumnModel().addColumn(new TableColumn(i, 60));

        for (int i = 0; i < kineticsTableModel.getColumnCount(); i ++)
            kineticsTable.getColumnModel().getColumn(i).setHeaderValue(kineticsTableModel.getColumnName(i));

        kineticsTable.getTableHeader().setReorderingAllowed(false);
        kineticsTable.setRowSelectionAllowed(false);
        kineticsTable.setColumnSelectionAllowed(false);

        kineticsTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (datas.size() > 0) {
                    Point p = e.getPoint();
                    int row = kineticsTable.rowAtPoint(p);
                    int column = kineticsTable.columnAtPoint(p);
                    if (column == 0) {
                        setSelection(row, -1, -1);
                        if (e.getButton() == MouseEvent.BUTTON3) {
                            kineticsMenuItem.setText(datas.get(row).getFilename());
                            kineticsTableRowPopupMenu.show(kineticsTable, p.x, p.y);
                        }
                    }
                    if (column >= 1 & column < 9) {
                        setSelection(-1, -1, -1);
                        if (e.getButton() == MouseEvent.BUTTON3)
                            tableGeneralPopupMenu.show(kineticsTable, p.x, p.y);
                    }
                    if (column >= 9) {
                        setSelection(-1, column - 9, -1);
                        if (e.getButton() == MouseEvent.BUTTON3)
                            tableColumnPopupMenu.show(kineticsTable, p.x, p.y);
                    }
                    updateDistribPlot();
                }
            }
        });
        kineticsTable.getTableHeader().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (datas.size() > 0) {
                    Point p = e.getPoint();
                    int column = kineticsTable.columnAtPoint(p);
                    if (column >= 9) {
                        setSelection(-1, column - 9, -1);
                        if (e.getButton() == MouseEvent.BUTTON3)
                            tableColumnPopupMenu.show(kineticsTable, p.x, p.y);
                    } else {
                        if (e.getButton() == MouseEvent.BUTTON1 && column >= 3 && column < 8)
                            setFunction(column - 3);
                        if (e.getButton() == MouseEvent.BUTTON3)
                            tableGeneralPopupMenu.show(kineticsTable, p.x, p.y);
                    }
                    updateDistribPlot();
                }
            }
        });

        pairsTable.setAutoCreateColumnsFromModel(true);
        pairsTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        pairsTable.setName("pairsTable");
        pairsTable.setRowHeight(21);
        pairsTable.getTableHeader().setReorderingAllowed(false);
        pairsTable.setDefaultRenderer(String.class, pairsTableCellRenderer);
        pairsTable.setRowSelectionAllowed(false);
        pairsTable.setColumnSelectionAllowed(false);
        pairsTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                Point p = e.getPoint();
                int row = pairsTable.rowAtPoint(p);
                pairsTableCellRenderer.setSelectedRow(row);
                pairsTableCellRenderer.setSelectedColumn(-1);
                setSelection(-1, -1, -1);
                sel.setPairs(row);
                pairsTable.repaint();
                if (e.getButton() == MouseEvent.BUTTON3 && pairsNames.size() > 0)
                    tableGeneralPopupMenu.show(pairsTable, p.x, p.y);
            }
        });
        pairsTable.getTableHeader().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                Point p = e.getPoint();
                groupTableCellRenderer.setSelectedRow(-1);
                groupTableCellRenderer.setSelectedColumn(-1);
                setSelection(-1, -1, -1);
                pairsTable.repaint();
                if (e.getButton() == MouseEvent.BUTTON3 && pairsNames.size() > 0)
                    tableGeneralPopupMenu.show(pairsTable, p.x, p.y);
            }
        });
        // TODO: colours on the plot if we have paired data

        groupTable.setAutoCreateColumnsFromModel(true);
        groupTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        groupTable.setName("groupTable");
        groupTable.setRowHeight(21);
        groupTable.getTableHeader().setReorderingAllowed(false);
        groupTable.setDefaultRenderer(String.class, groupTableCellRenderer);
        groupTable.setRowSelectionAllowed(false);
        groupTable.setColumnSelectionAllowed(false);
        groupTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                Point p = e.getPoint();
                int row = groupTable.rowAtPoint(p);
                int column = groupTable.columnAtPoint(p);
                int dcc = ((GroupTableModel) groupTable.getModel()).getDescriptionColumnCount();
                if (row < groupedDatas.size() || column >= dcc) {
                    if (column < dcc) {
                        groupTableCellRenderer.setSelectedRow(row);
                        groupTableCellRenderer.setSelectedColumn(-1);
                        setSelection(-1, -1, row);
                        if (e.getButton() == MouseEvent.BUTTON3 && groupedDatas.size() > 0)
                            groupTableRowPopupMenu.show(groupTable, p.x, p.y);
                    }
                    if (column >= dcc) {
                        groupTableCellRenderer.setSelectedRow(-1);
                        groupTableCellRenderer.setSelectedColumn(column);
                        setSelection(-1, column - dcc, -1);
                        if (e.getButton() == MouseEvent.BUTTON3 && groupedDatas.size() > 0)
                            tableColumnPopupMenu.show(groupTable, p.x, p.y);
                    }
                } else {
                    setSelection(-1, -1, -1);
                    if (e.getButton() == MouseEvent.BUTTON3 && groupedDatas.size() > 0)
                        tableGeneralPopupMenu.show(groupTable, p.x, p.y);
                }
                updateDistribPlot();
            }
        });
        groupTable.getTableHeader().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                Point p = e.getPoint();
                int column = groupTable.columnAtPoint(p);
                int dcc = ((GroupTableModel) groupTable.getModel()).getDescriptionColumnCount();
                if (column >= dcc) {
                    groupTableCellRenderer.setSelectedRow(-1);
                    groupTableCellRenderer.setSelectedColumn(column);
                    setSelection(-1, column - dcc, -1);
                    if (e.getButton() == MouseEvent.BUTTON3 && datas.size() > 0)
                        tableColumnPopupMenu.show(groupTable, p.x, p.y);
                } else {
                    if (e.getButton() == MouseEvent.BUTTON3 && datas.size() > 0)
                        tableGeneralPopupMenu.show(groupTable, p.x, p.y);
                }
                updateDistribPlot();
            }
        });

        graphDisplay.setComponentPopupMenu(graphDisplayPopupMenu);

        // because the NetBeans design view forgets these properties from time to time
        boxplotRadioButton.setEnabled(false);
        histogramRadioButton.setEnabled(false);
        dotplotRadioButton.setEnabled(false);
        horizBoxplotRadioButton.setEnabled(false);

        ensureShowingKineticsTable();
    }

    //------------------------------------------------
    // private helper functions
    //------------------------------------------------

    private void setFunction(int fct) {
        sel.setPar(-1);
        kineticsTableCellRenderer.setSelectedColumn(-1);
        for (int i = kineticsTable.getColumnCount() - 1; i >= 9; i --)
            kineticsTable.getColumnModel().removeColumn(kineticsTable.getColumnModel().getColumn(i));
        kineticsTableModel.fireTableStructureChanged();
        int prev_fun = sel.getFun();
        sel.setFun(fct);
        for (int i = 9; i < Functions.parameterCount(fct) + 9; i ++) {
            kineticsTable.getColumnModel().addColumn(new TableColumn(i, 100));
            kineticsTable.getColumnModel().getColumn(i).setHeaderValue(kineticsTableModel.getColumnName(i));
        }
        kineticsTable.getColumnModel().getColumn(prev_fun + 3).setHeaderValue(kineticsTableModel.getColumnName(prev_fun + 3));
        kineticsTable.getColumnModel().getColumn(sel.getFun() + 3).setHeaderValue(kineticsTableModel.getColumnName(sel.getFun() + 3));
        kineticsTableModel.fireTableStructureChanged();
        ((GraphKinetics) graphDisplay).reloadTransformer();
        graphDisplay.repaint();
    }
    
    private void setSelection(int kin, int par, int grp) {
        sel.setKin(kin);
        sel.setPar(par);
        sel.setGroup(grp);
        
        graphDisplay.repaint();

        if (currentTable.equals("kineticsTable")) {
            kineticsTableCellRenderer.setSelectedRow(kin);
            comboBoxCellRenderer.setSelectedRow(kin);
            if (par != -1)
                kineticsTableCellRenderer.setSelectedColumn(par + 9);
            else
                kineticsTableCellRenderer.setSelectedColumn(-1);
            kineticsTable.repaint();
        }

        if (currentTable.equals("groupTable")) {
            groupTableCellRenderer.setSelectedRow(grp);
            if (par != -1)
                groupTableCellRenderer.setSelectedColumn(par + ((GroupTableModel) groupTable.getModel()).getDescriptionColumnCount());
            else
                groupTableCellRenderer.setSelectedColumn(-1);
            groupTable.repaint();
        }

    }

    private boolean is_first(int l) {
        int j = 0;
        while (j < l && !groups.get(j).equals(groups.get(l)))
            ++j;
        return (j==l);
    }

    private boolean is_first_pairs(int l) {
        int j = 0;
        while (j < l && !pairsGroups.get(j).equals(pairsGroups.get(l)))
            ++j;
        return (j==l);
    }

    private void updateDistribPlot() {
        distribPlotPanel.removeAll();
        int param = sel.getPar();
        if (param != -1 && currentTable.equals("groupTable")) {
            boxplotRadioButton.setEnabled(true);
            histogramRadioButton.setEnabled(true);
            dotplotRadioButton.setEnabled(true);
            horizBoxplotRadioButton.setEnabled(true);

            List<Color> someColors = new ArrayList<Color>();
            for (int i = 0; i < compareDatas.get(param).size(); ++i)
                someColors.add(ListColorScale.getColor(i, compareDatas.get(param).size()));

            distribPlotPanel.add(new GraphBoxPlot(compareDatas.get(param), someColors, compareGroupNames.get(param)), "boxplot");
            distribPlotPanel.add(new GraphHistogramPlot(compareDatas.get(param), someColors, compareGroupNames.get(param)), "histogram");
            distribPlotPanel.add(new GraphDotPlot(compareDatas.get(param), someColors, compareGroupNames.get(param)), "dotplot");
            distribPlotPanel.add(new GraphHorizBoxPlot(compareDatas.get(param), someColors, compareGroupNames.get(param)), "horizboxplot");

            ((GraphBoxPlot) distribPlotPanel.getComponent(0)).setComponentPopupMenu(distribPlotPopupMenu);
            ((GraphHistogramPlot) distribPlotPanel.getComponent(1)).setComponentPopupMenu(distribPlotPopupMenu);

            ((CardLayout) distribPlotPanel.getLayout()).show(distribPlotPanel, groupPlot);
        } else if (param != -1 && currentTable.equals("kineticsTable") && !FacsKinApp.getUseOnlyMedians()) {
            boxplotRadioButton.setEnabled(true);
            histogramRadioButton.setEnabled(true);
            dotplotRadioButton.setEnabled(true);
            horizBoxplotRadioButton.setEnabled(true);

            List<Color> someColors = new ArrayList<Color>();
            for (int i = 0; i < datas.size(); ++i) {
                if (FacsKinApp.getColorByGroups())
                    someColors.add(ListColorScale.getColor(getGroupNumber(i), getNumberOfGroups()));
                else
                    someColors.add(ListColorScale.getColor(i, datas.size()));
            }

            List<Data> someDatas = new ArrayList<Data>();
            for (int i = 0; i < datas.size(); ++i) {
                ComparisonData d = new ComparisonData(datas.get(i).getFilename());
                d.addData(datas.get(i).getFunction(sel.getFun()).getParam(param));
                someDatas.add(d);
            }

            String paramName = datas.get(0).getFunction(sel.getFun()).getParam(param).getName();
            distribPlotPanel.add(new GraphBoxPlot(someDatas, someColors, paramName), "boxplot");
            distribPlotPanel.add(new GraphHistogramPlot(someDatas, someColors, paramName), "histogram");
            distribPlotPanel.add(new GraphDotPlot(someDatas, someColors, paramName), "dotplot");
            distribPlotPanel.add(new GraphHorizBoxPlot(someDatas, someColors, paramName), "horizboxplot");

            ((GraphBoxPlot) distribPlotPanel.getComponent(0)).setComponentPopupMenu(distribPlotPopupMenu);
            ((GraphHistogramPlot) distribPlotPanel.getComponent(1)).setComponentPopupMenu(distribPlotPopupMenu);

            ((CardLayout) distribPlotPanel.getLayout()).show(distribPlotPanel, kineticsPlot);
        } else {
            boxplotRadioButton.setEnabled(false);
            histogramRadioButton.setEnabled(false);
            dotplotRadioButton.setEnabled(false);
            horizBoxplotRadioButton.setEnabled(false);
            distribPlotPanel.repaint();
        }

        boxplotRadioButton.setSelected(false);
        histogramRadioButton.setSelected(false);
        dotplotRadioButton.setSelected(false);
        horizBoxplotRadioButton.setSelected(false);

        String currentPlot = getCurrentPlot();
        if (currentPlot.equals("boxplot")) boxplotRadioButton.setSelected(true);
        if (currentPlot.equals("histogram")) histogramRadioButton.setSelected(true);
        if (currentPlot.equals("dotplot")) dotplotRadioButton.setSelected(true);
        if (currentPlot.equals("horizboxplot")) horizBoxplotRadioButton.setSelected(true);
    }

    private String getCurrentPlot() {
        String currentPlot;
        if (currentTable.equals("kineticsTable"))
            currentPlot = kineticsPlot;
        else if (currentTable.equals("groupTable"))
            currentPlot = groupPlot;
        else currentPlot = "none";
        return currentPlot;
    }

    private void setCurrentPlot(String currentPlot) {
        if (currentTable.equals("kineticsTable"))
            kineticsPlot = currentPlot;
        else
            groupPlot = currentPlot;
    }
    
    private void maybeEnableUseOnlyMedians() {
        // if there is a kinetics file with only 1 quantile, use only medians is
        // the only option

        boolean b = false;
        for (KineticsData d : datas) {
            if (d.getFunction(0).getParam(0).getDataLength() == 1)
                b = true;
        }
        if (b) {
            if (!useOnlyMedianFunctionsCheckbox.isSelected())
                useOnlyMedianFunctionsCheckbox.doClick();
            useOnlyMedianFunctionsCheckbox.setEnabled(false);
        } else
            useOnlyMedianFunctionsCheckbox.setEnabled(true);
    }

    //------------------------------------------------
    // getters used by FacsKinView, GraphKinetics and table models
    //------------------------------------------------

    public List<KineticsData> getDatas()                { return datas; }
    public List<String>       getGroups()               { return groups; }
    public FacsKinView        getFather()               { return father; }
    public int                getSelectedKineticsData() { return sel.getKin(); }
    public int                getSelectedPairsData()    { return sel.getPairs(); }
    public int                getSelectedFunction()     { return sel.getFun(); }
    public int                getSelectedGroup()        { return sel.getGroup(); }

    public int getNumberOfGroups() {
        int size = 0;
        // number of different elements in groups
        for (int i = 0; i < groups.size(); ++i)
            if (is_first(i))
                ++size;
        return size;
    }

    /**
     * get the number of group in which the row^th kinetics file in kineticsTable
     * belongs (groups are numbered from 0 to getNumberOfGroups())
     * @param row
     * @return
     */
    public int getGroupNumber(int row) {
        int num = 0;
        int i = 0;
        while (!groups.get(i).equals(groups.get(row))) {
            if (is_first(i))
                ++num;
            ++i;
        }
        return num;
    }

    /**
     * get the number of group in which the row^th kinetics file in pairsTable
     * belongs (groups are numbered from 0 to getNumberOfGroups())
     * @param row
     * @return
     */
    public int getPairsGroupNumber(int row) {
        return getGroupNumber(pairsOriginalPosition.get(row));
    }

    /**
     * get the name of the n^th group (there are getNumberOfGroups() many groups)
     */
    public String getGroup(int n) {
        int i = 0;
        int j = 0;
        while (i < n) {
            ++j;
            if (is_first(j))
                ++i;
        }
        return groups.get(j);
    }

    public int getNumberOfPairsData() {
        return pairsNames.size();
    }

    public int getPairsNumber(int row) {
        int result = -1;
        if (0 <= row && row < datas.size()) {
            for (int i = 0; i < pairsNames.size() && result == -1; ++ i)
                if (pairsNames.get(i).equals(datas.get(row).getFilename()))
                    result = i;
            if (result == -1) {
                int toFind = -1;
                for (int i = 0; i < pairs.size() && toFind == -1; ++ i)
                    if (pairs.get(i) != null && pairs.get(i).getFilename().equals(datas.get(row).getFilename()))
                        toFind = i;
                if (toFind != -1)
                    for (int i = 0; i < pairsNames.size() && result == -1; ++ i)
                        if (pairsNames.get(i).equals(datas.get(toFind).getFilename()))
                            result = i;
            }
        }
        return result;
    }

    //------------------------------------------------
    // interface redraw firing functions used by FacsKinView or table models
    //------------------------------------------------

    @Override
    public void readyWithPainting() {
        ((GraphKinetics) graphDisplay).reloadTransformer();
        ((GraphKinetics) graphDisplay).repaint();
    }

    public void reloadComboBoxEditor() {
        // not really nice but if not done the combo box does not shhow anything after a while
        kineticsTable.getColumnModel().getColumn(1).setCellEditor(new DefaultCellEditor(new JComboBox(new GroupComboBoxModel(KineticsPanel.this, newGroupString))));
    }

    public final void ensureShowingKineticsTable() {

        tScoreButton.setText("Calculate T scores >");
        pairsButton.setText("Pair Data >");
        groupDataButton.setText("Group Data >");

        if (FacsKinApp.getUseOnlyMedians()) {
            tScoreButton.setEnabled(false);
            groupDataButton.setEnabled(true);
        } else {
            tScoreButton.setEnabled(true);
            groupDataButton.setEnabled(false);
        }

        ((CardLayout) tablePanel.getLayout()).show(tablePanel, "kineticsTable");
        currentTable = "kineticsTable";

        
        maybeEnableUseOnlyMedians();
        pairsButton.setEnabled(true);
    }

    public void redrawTableAndGraph() {
        ((GraphKinetics) graphDisplay).reloadTransformer();
        ((GraphKinetics) graphDisplay).repaint();
        if (currentTable.equals("kineticsTable")) {
            for (int i = 0; i < kineticsTable.getColumnModel().getColumnCount(); ++i) {
                TableColumn column = kineticsTable.getColumnModel().getColumn(i);
                column.setHeaderValue(kineticsTable.getModel().getColumnName(column.getModelIndex()));
            }
            kineticsTable.getTableHeader().repaint();
            kineticsTable.repaint();
        }
        if (currentTable.equals("pairsTable"))
            pairsTable.repaint();
        if (currentTable.equals("groupTable"))
            groupTable.repaint();
        updateDistribPlot();
    }

    //------------------------------------------------
    // user actions in FacsKinView that modify the state of KineticsPanel
    //------------------------------------------------

    public void addData(KineticsData data) {
        String group;
        if (groups.size() > 0)
            group = groups.get(groups.size() - 1);
        else
            group = "A";
        addData(data, group);
    }

    public void addData(KineticsData data, String group) {
        ensureShowingKineticsTable();
        datas.add(data);
        maybeEnableUseOnlyMedians();
        datas.get(datas.size() - 1).setStandardisation(FacsKinApp.getStandardisation());
        groups.add(group);
        pairs.add(null);
        sel.setKin(datas.size() - 1);
        groupTableCellRenderer.setSelectedRow(datas.size() - 1);
        pairsTableCellRenderer.setSelectedRow(datas.size() - 1);
        comboBoxCellRenderer.setSelectedRow(datas.size() - 1);
        if (sel.getFun() == -1) {
            sel.setFun(0);
            // I don't know why is this here:
            kineticsTable.getColumnModel().addColumn(new TableColumn(9, 100));
            kineticsTable.getColumnModel().getColumn(9).setHeaderValue(kineticsTableModel.getColumnName(9));
            kineticsTable.getColumnModel().getColumn(3).setHeaderValue(kineticsTableModel.getColumnName(3));
            setFunction(datas.get(0).getBestFunction());
        }
        kineticsTableModel.fireTableStructureChanged();
        ((GraphKinetics) graphDisplay).reloadTransformer();
        graphDisplay.repaint();
        kineticsTable.repaint();

        // if this is not here, when doing a File/Open kinetics file after having some
        // kinetics files in the table, the pair drop down menus will be empty
        kineticsTable.getColumnModel().getColumn(2).setCellEditor(new DefaultCellEditor(new JComboBox(new PairsComboBoxModel(this))));
    }

    public void addDatas(File file, TaskWithProgress task) throws FileNotFoundException, IOException {
        KineticsZip kz = new KineticsZip(file, task);

        int origSize = datas.size();

        if (kz.getGroups() != null) {
            for (int i = 0; i < kz.getDatas().size(); ++i)
                addData(kz.getDatas().get(i), kz.getGroups().get(i));
        } else {
            for (int i = 0; i < kz.getDatas().size(); ++i)
                addData(kz.getDatas().get(i));
        }

        if (kz.getPairs() != null)
            for (int i = 0; i < kz.getPairs().size(); ++i)
                pairs.set(origSize + i, kz.getPairs().get(i));

        if (kz.getSelectedFunction() != null)
            setFunction(kz.getSelectedFunction());
        if (kz.getMaxTime() != null)
            setMaxTime(kz.getMaxTime());
        if (kz.getBaselineTime() != null)
            setBaselineTime(kz.getBaselineTime());
        redrawTableAndGraph();
    }

    public void setMaxTime(double newMaxTime) {
        FacsKinApp.setMaxTime(newMaxTime);
        for (int i = 0; i < getDatas().size(); i ++)
            getDatas().get(i).setMaxTime(newMaxTime);
        redrawTableAndGraph();
        ensureShowingKineticsTable();
    }

    public void setBaselineTime(double newBaselineTime) {
        FacsKinApp.setBaselineTime(newBaselineTime);
        // we set standardization because it uses baseline time
        for (int i = 0; i < getDatas().size(); i ++)
            getDatas().get(i).setStandardisation(FacsKinApp.getStandardisation());
        redrawTableAndGraph();
        ensureShowingKineticsTable();
    }

    public void writeDatas(File file) throws FileNotFoundException, IOException {
        KineticsZip kz = new KineticsZip(datas, groups, pairs, sel.getFun(), FacsKinApp.getMaxTime(), FacsKinApp.getBaselineTime());
        kz.writeDatas(file);
    }

    //------------------------------------------------
    // user actions in KineticsPanel
    //------------------------------------------------

    @Override
    public void lostOwnership(Clipboard clipboard, Transferable contents) {}

    @Action
    /**
     * When they click "Copy table contents"
     */
    public void copyToClipboardClicked() {
        String s = "";
        if (currentTable.equals("kineticsTable"))
            s = kineticsTableModel.getContentsAsString();
        if (currentTable.equals("pairsTable"))
            s = ((PairsTableModel) pairsTable.getModel()).getContentsAsString();
        if (currentTable.equals("groupTable"))
            s = ((GroupTableModel) groupTable.getModel()).getContentsAsString();
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(s), this);
    }

    @Action
    public void copyParameterDistributionClicked() {
        if (sel.getPar() != -1) {
            String s = "";
            if (currentTable.equals("kineticsTable"))
                s = kineticsTableModel.getDistributionsFor1ParameterAsString(sel.getPar());
            if (currentTable.equals("groupTable"))
                s = ((GroupTableModel) groupTable.getModel()).getDistributionsFor1ParameterAsString(sel.getPar());
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(s), this);
        }
    }

    @Action
    public void copyParameterDistributionsClicked() {
        String s = "";
        if (currentTable.equals("kineticsTable") && sel.getKin() != -1)
            s = kineticsTableModel.getDistributionsFor1DataAsString(sel.getKin());
        if (currentTable.equals("groupTable") && sel.getGroup() != -1)
            s = ((GroupTableModel) groupTable.getModel()).getDistributionsFor1DataAsString(sel.getGroup());
        if (!s.equals(""))
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(s), this);
    }

    @Action
    public Task copyRCode() {
        return new CopyRCodeTask(org.jdesktop.application.Application.getInstance(facskin.FacsKinApp.class));
    }

    private class CopyRCodeTask extends org.jdesktop.application.Task<Object, Void> {
        CopyRCodeTask(org.jdesktop.application.Application app) {
            super(app);
        }
        @Override protected Object doInBackground() {
            String s = "";
            if (currentTable.equals("kineticsTable"))
                s = kineticsTableModel.getRCode();
            if (currentTable.equals("pairsTable"))
                s = ((PairsTableModel)pairsTable.getModel()).getRCode();
            if (currentTable.equals("groupTable"))
                s = ((GroupTableModel) groupTable.getModel()).getRCode();
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(s), KineticsPanel.this);
            return null;  // return your result
        }
        @Override protected void succeeded(Object result) {
        }
    }

    @Action
    public void useOnlyMedianFunctionsCheckboxChanged() {
        FacsKinApp.setUseOnlyMedians(useOnlyMedianFunctionsCheckbox.isSelected());
        ensureShowingKineticsTable();
        redrawTableAndGraph();
        updateDistribPlot();
    }

    @Action
    public void tScoreButtonClicked() {
        if (currentTable.equals("kineticsTable")) {
            String[] controls = new String[getNumberOfGroups() + 1];
            for (int i = 0; i < getNumberOfGroups(); ++i)
                controls[i] = getGroup(i);
            controls[getNumberOfGroups()] = "(combine all)";

            int binCount = ProbabilityBinning.defaultBinCount(datas, sel.getFun(), 0);

            PBOptionPanel optionPanel = new PBOptionPanel(controls, binCount);
            
            int result = JOptionPane.showConfirmDialog(this, optionPanel, "Calculate T scores", JOptionPane.OK_CANCEL_OPTION);
            if (result == 0) {
                int controlGroup = optionPanel.getSelectedGroup();
                boolean combineAll = controlGroup == getNumberOfGroups();

                if (optionPanel.getBinCount() > 2 && optionPanel.getBinCount() < 200)
                    binCount = optionPanel.getBinCount();

                tScoreButton.setText("< Back (no T scores)");
                groupDataButton.setEnabled(true);
                pairsButton.setEnabled(false);
                useOnlyMedianFunctionsCheckbox.setEnabled(false);

                pairsValues.clear();
                pairsNames.clear();
                pairsGroups.clear();
                pairsParamNames.clear();
                pairsOriginalPosition.clear();

                int paramCount = Functions.parameterCount(sel.getFun());

                for (int i = 0; i < datas.size(); ++i) {
                    pairsValues.add(new ArrayList<Double>());
                    pairsNames.add(datas.get(i).getFilename());
                    pairsGroups.add(groups.get(i));
                    pairsOriginalPosition.add(i);
                }

                for (int p = 0; p < paramCount; ++ p) {
                    ComparisonData controlDatas = new ComparisonData(Functions.getParameterName(sel.getFun(), p));
                    for (int i = 0; i < datas.size(); ++i) {
                        if (getGroupNumber(i) == controlGroup || combineAll)
                            controlDatas.addData(datas.get(i).getFunction(sel.getFun()).getParam(p));
                    }

                    ProbabilityBinning pb = new ProbabilityBinning(controlDatas, binCount);

                    for (int i = 0; i < datas.size(); ++i)
                        pairsValues.get(i).add(pb.compare(datas.get(i).getFunction(sel.getFun()).getParam(p))[0]);
                }

                for (int p = 0; p < paramCount; ++ p)
                    pairsParamNames.add("T(" + Functions.getParameterName(sel.getFun(), p) + ")");

                pairsTable.setModel(new PairsTableModel(this, pairsValues, pairsGroups, pairsNames, pairsParamNames, sel));
                for (int i = 1; i < pairsTable.getColumnCount(); ++i)
                    pairsTable.getColumnModel().getColumn(i).setPreferredWidth(100);

                JScrollPane pairsScrollPane = new JScrollPane();
                pairsScrollPane.setName("pairsScrollPane");
                pairsScrollPane.setViewportView(pairsTable);
                tablePanel.add(pairsScrollPane, "pairsTable");

                ((CardLayout) tablePanel.getLayout()).show(tablePanel, "pairsTable");
                currentTable = "pairsTable";

                setSelection(-1, -1, -1);
            }
        } else {
            ensureShowingKineticsTable();
            if (tablePanel.getComponentCount() > 1)
                ((Container) tablePanel).remove(tablePanel.getComponent(1));
            setSelection(-1, -1, -1);
        }
        updateDistribPlot();
    } // tScoreButtonClicked()

    @Action
    public void pairButtonClicked() {
        if (currentTable.equals("kineticsTable")) {
            FacsKinApp.setShowDataByPairs(true);
            pairsButton.setText("< Back (no pairing)");
            groupDataButton.setEnabled(true);
            tScoreButton.setEnabled(false);
            useOnlyMedianFunctionsCheckbox.setEnabled(false);

            pairsValues.clear();
            pairsNames.clear();
            pairsGroups.clear();
            pairsParamNames.clear();
            pairsOriginalPosition.clear();

            int paramCount = Functions.parameterCount(sel.getFun());
            int binCount = ProbabilityBinning.defaultBinCount(datas, sel.getFun(), 0);

            for (int i = 0; i < datas.size(); ++ i) {
                if (pairs.get(i) != null) {
                    pairsValues.add(new ArrayList<Double>());
                    pairsNames.add(datas.get(i).getFilename());
                    pairsGroups.add(groups.get(i));
                    pairsOriginalPosition.add(i);
                    for (int p = 0; p < paramCount; ++ p) {
                        if (FacsKinApp.getUseOnlyMedians()) {
                            pairsValues.get(pairsValues.size() - 1).add(
                                    datas.get(i).getFunction(sel.getFun()).getParam(p).getMidData() -
                                    pairs.get(i).getFunction(sel.getFun()).getParam(p).getMidData());
                        }  else {
                            ProbabilityBinning pb = new ProbabilityBinning(pairs.get(i).getFunction(sel.getFun()).getParam(p), binCount);
                            pairsValues.get(pairsValues.size() - 1).add(pb.compare(datas.get(i).getFunction(sel.getFun()).getParam(p))[0]);
                        }
                    }
                }
            }

            for (int p = 0; p < paramCount; ++ p) {
                String parName = Functions.getParameterName(sel.getFun(), p);
                if (FacsKinApp.getUseOnlyMedians())
                    parName = "Delta(Median(" + parName + "))";
                else
                    parName = "T(" + parName + ")";
                pairsParamNames.add(parName);
            }

            pairsTable.setModel(new PairsTableModel(this, pairsValues, pairsGroups, pairsNames, pairsParamNames, sel));
            for (int i = 1; i < pairsTable.getColumnCount(); ++i)
                pairsTable.getColumnModel().getColumn(i).setPreferredWidth(100);

            JScrollPane pairsScrollPane = new JScrollPane();
            pairsScrollPane.setName("pairsScrollPane");
            pairsScrollPane.setViewportView(pairsTable);
            tablePanel.add(pairsScrollPane, "pairsTable");

            ((CardLayout) tablePanel.getLayout()).show(tablePanel, "pairsTable");
            currentTable = "pairsTable";

            setSelection(-1, -1, -1);
        } else { // if we go back from pairing
            FacsKinApp.setShowDataByPairs(false);
            ensureShowingKineticsTable();
            if (tablePanel.getComponentCount() > 1)
                ((Container) tablePanel).remove(tablePanel.getComponent(1));
            setSelection(-1, -1, -1);            
        }
        updateDistribPlot();
    } // pairButtonClicked()

    @Action
    public void groupDataButtonClicked() {
        if (currentTable.equals("groupTable")) { // go back from grouping
            FacsKinApp.setShowDataByGroups(false);
            groupDataButton.setText("Group Data >");
            if (FacsKinApp.getUseOnlyMedians() && !FacsKinApp.getShowDataByPairs())
                ensureShowingKineticsTable();
            else {
                if (FacsKinApp.getShowDataByPairs())
                    pairsButton.setEnabled(true);
                else
                    tScoreButton.setEnabled(true);
                ((CardLayout) tablePanel.getLayout()).show(tablePanel, "pairsTable");
                currentTable = "pairsTable";
            }

            ((Container) tablePanel).remove(tablePanel.getComponent(tablePanel.getComponentCount() - 1));
            setSelection(-1, -1, -1);
        } else { // grouping

            int paramCount = Functions.parameterCount(sel.getFun());

            if (FacsKinApp.getUseOnlyMedians() && !FacsKinApp.getShowDataByPairs()) {
                // if we haven't made pairs, we fake them

                pairsValues.clear();
                pairsNames.clear();
                pairsGroups.clear();
                pairsParamNames.clear();

                for (int i = 0; i < datas.size(); ++i) {
                    pairsValues.add(new ArrayList<Double>());
                    pairsNames.add(datas.get(i).getFilename());
                    pairsGroups.add(groups.get(i));
                    for (int p = 0; p < paramCount; ++ p)
                        pairsValues.get(pairsValues.size() - 1).
                                add(datas.get(i).getFunction(sel.getFun()).getParam(p).getMidData());
                }

                for (int p = 0; p < paramCount; ++ p)
                    pairsParamNames.add("Median(" + Functions.getParameterName(sel.getFun(), p) + ")");

            }

            FacsKinApp.setShowDataByGroups(true);
            groupDataButton.setText("< Back (no grouping)");
            pairsButton.setEnabled(false);
            tScoreButton.setEnabled(false);
            useOnlyMedianFunctionsCheckbox.setEnabled(false);

            groupedDatas.clear();
            groupedGroupNames.clear();

            for (int i = 0; i < pairsGroups.size(); ++i) { // we go through all pairs
                int j = 0;
                while (j < groupedDatas.size() && !groupedGroupNames.get(j).equals(pairsGroups.get(i)))
                    ++j;
                if (j == groupedDatas.size()) {
                    groupedDatas.add(new ArrayList<Data>());
                    for (int p = 0; p < pairsParamNames.size(); ++p)
                        groupedDatas.get(j).add(new ComparisonData(pairsParamNames.get(p)));
                    groupedGroupNames.add(pairsGroups.get(i));
                }
                for (int p = 0; p < Functions.parameterCount(sel.getFun()); ++p)
                    ((ComparisonData) groupedDatas.get(j).get(p)).addData(pairsValues.get(i).get(p));
            }

            // fill in compareData

            compareDatas.clear();
            compareGroupNames.clear();

            if (groupedDatas.size() > 0) { // maybe this is not needed
                for (int param = 0; param < paramCount; ++param) {
                    List<Data> someDatas = new ArrayList<Data>();

                    for (int i = 0; i < groupedDatas.size(); ++i) {
                        ComparisonData parData = new ComparisonData(groupedGroupNames.get(i));
                        parData.addData(groupedDatas.get(i).get(param));
                        someDatas.add(parData);
                    }

                    compareDatas.add(someDatas);
                    compareGroupNames.add(groupedDatas.get(0).get(param).getName());
                }
            }

            List<double[]> comparisonResults = new ArrayList<double[]>();
            // perform comparison
            for (int param = 0; param < compareDatas.size(); ++param) {
                List<List<Double>> datasList = new ArrayList<List<Double>>();
                for (int i = 0; i < compareDatas.get(param).size(); ++i)
                    datasList.add(((ComparisonData) compareDatas.get(param).get(i)).getDatas());
                comparisonResults.add(KruskalWallis.test(datasList));
            }

            List<Color> someColors = new ArrayList<Color>();
            for (int i = 0; i < groupedDatas.size(); ++i)
                someColors.add(ListColorScale.getColor(i, groupedDatas.size()));

            groupTable.setModel(new GroupTableModel(groupedDatas, groupedGroupNames, someColors, sel, comparisonResults));
            for (int i = 1; i < groupTable.getColumnCount(); ++i)
                groupTable.getColumnModel().getColumn(i).setPreferredWidth(100);

            JScrollPane groupScrollPane = new JScrollPane();
            groupScrollPane.setName("groupScrollPane");
            groupScrollPane.setViewportView(groupTable);
            tablePanel.add(groupScrollPane, "groupTable");

            ((CardLayout) tablePanel.getLayout()).show(tablePanel, "groupTable");
            currentTable = "groupTable";

            setSelection(-1, -1, -1);
        }
        updateDistribPlot();
    } // groupDataButtonClicked()

    @Action
    public void getInfoClicked() {
        // this is only called from the popup menu for kineticsTable

        String s = "Filename: " + datas.get(sel.getKin()).getFilename() + "\n\n";
        String[] params = {"Measurement time: ", "Measurement name: ", "FCS file name: ", "Email: ", "Gate description:\n", "Analysed parameter: "};
        for (int i = 0; i < datas.get(sel.getKin()).getMetadata().length; i ++) {
            if (i < params.length)
                s += params[i] + datas.get(sel.getKin()).getMetadata()[i] + "\n";
            else
                s += datas.get(sel.getKin()).getMetadata()[i] + "\n";
        }
        JOptionPane.showMessageDialog(this, s, datas.get(sel.getKin()).getFilename(), JOptionPane.INFORMATION_MESSAGE);
    }

    @Action
    public void removeRowClicked() {
        // this is only called when currentTable == "kineticsTable"

        if (sel.getKin() != -1) {
            int r = sel.getKin();
            for (int i = 0; i < pairs.size(); ++i)
                if (pairs.get(i) == datas.get(r))
                        pairs.set(i, null);
            datas.remove(r);
            groups.remove(r);
            pairs.remove(r);
            kineticsTableModel.fireTableStructureChanged();
            ((GraphKinetics) graphDisplay).reloadTransformer();
            graphDisplay.repaint();
            if (datas.isEmpty())
                father.disableSaveAs();
            sel.setKin(-1);
            updateDistribPlot();
        }
        
        maybeEnableUseOnlyMedians();
    }

    @Action
    public void renameKineticsFileClicked() {
        // this is only called when currentTable == "kineticsTable"

        int r = sel.getKin();
        if (r != -1) {
            String newFilename;
            newFilename = JOptionPane.showInputDialog(this, "Rename kinetics file:", datas.get(r).getFilename());
            if (newFilename != null) {
                newFilename = newFilename.replaceAll("[^a-zA-Z0-9.-@]", "_");
                if (!newFilename.toLowerCase().endsWith(".kinetics"))
                    newFilename = newFilename + ".kinetics";
                datas.get(r).setFilename(newFilename);
                kineticsTable.repaint();
            }
        }
    }

    @Action
    public void saveImageClicked() {
        JFileChooser fileChooser = father.getFileChooser();
        fileChooser.setSelectedFile(new File("kinetics.png"));
        int option = fileChooser.showSaveDialog(father.getFrame());
        if (JFileChooser.APPROVE_OPTION == option) {
            try {
                ((GraphKinetics) graphDisplay).saveImage(fileChooser.getSelectedFile());
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(this, "Could not write to file.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    @Action
    public void saveDistribAsPNGClicked() {
        int whichPlot = 0;
        String currentPlot = getCurrentPlot();

        if (currentPlot.equals("histogram")) whichPlot = 1;
        if (currentPlot.equals("dotplot")) whichPlot = 2;
        if (currentPlot.equals("horizboxplot")) whichPlot = 3;
        String paramName = ((Graph) distribPlotPanel.getComponent(whichPlot)).name().replaceAll("[^a-zA-Z0-9.-@]", "_");

        JFileChooser fileChooser = father.getFileChooser();
        fileChooser.setSelectedFile(new File("comparison_" + paramName + ".png"));
        int option = fileChooser.showSaveDialog(father.getFrame());
        if (JFileChooser.APPROVE_OPTION == option) {
            try {
                ((Graph) distribPlotPanel.getComponent(whichPlot)).saveImage(fileChooser.getSelectedFile());
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(this, "Could not write to file.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    @Action
    public void boxplotRadioClicked() {
        if (!getCurrentPlot().equals("boxplot")) {
            setCurrentPlot("boxplot");
            ((CardLayout) distribPlotPanel.getLayout()).show(distribPlotPanel, "boxplot");
            boxplotRadioButton.setSelected(true);
            histogramRadioButton.setSelected(false);
            dotplotRadioButton.setSelected(false);
            horizBoxplotRadioButton.setSelected(false);
        }
    }

    @Action
    public void histogramRadioClicked() {
        if (!getCurrentPlot().equals("histogram")) {
            setCurrentPlot("histogram");
            ((CardLayout) distribPlotPanel.getLayout()).show(distribPlotPanel, "histogram");
            boxplotRadioButton.setSelected(false);
            histogramRadioButton.setSelected(true);
            dotplotRadioButton.setSelected(false);
            horizBoxplotRadioButton.setSelected(false);
        }
    }

    @Action
    public void dotplotRadioClicked() {
        if (!getCurrentPlot().equals("dotplot")) {
            setCurrentPlot("dotplot");
            ((CardLayout) distribPlotPanel.getLayout()).show(distribPlotPanel, "dotplot");
            boxplotRadioButton.setSelected(false);
            histogramRadioButton.setSelected(false);
            dotplotRadioButton.setSelected(true);
            horizBoxplotRadioButton.setSelected(false);
        }
    }

    @Action
    public void horizBoxplotRadioClicked() {
        if (!getCurrentPlot().equals("horizboxplot")) {
            setCurrentPlot("horizboxplot");
            ((CardLayout) distribPlotPanel.getLayout()).show(distribPlotPanel, "horizboxplot");
            boxplotRadioButton.setSelected(false);
            histogramRadioButton.setSelected(false);
            dotplotRadioButton.setSelected(false);
            horizBoxplotRadioButton.setSelected(true);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        graphDisplayPopupMenu = new javax.swing.JPopupMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        tableColumnPopupMenu = new javax.swing.JPopupMenu();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        kineticsTableRowPopupMenu = new javax.swing.JPopupMenu();
        kineticsMenuItem = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem14 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JSeparator();
        jMenuItem15 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        tableGeneralPopupMenu = new javax.swing.JPopupMenu();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        groupTableRowPopupMenu = new javax.swing.JPopupMenu();
        jMenuItem16 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem12 = new javax.swing.JMenuItem();
        distribPlotPopupMenu = new javax.swing.JPopupMenu();
        jMenuItem13 = new javax.swing.JMenuItem();
        useOnlyMedianFunctionsCheckbox = new javax.swing.JCheckBox();
        groupDataButton = new javax.swing.JButton();
        jSplitPane1 = new javax.swing.JSplitPane();
        tablePanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        kineticsTable = new javax.swing.JTable();
        jSplitPane2 = new javax.swing.JSplitPane();
        graphDisplay = new GraphKinetics(this);
        jPanel1 = new javax.swing.JPanel();
        boxplotRadioButton = new javax.swing.JRadioButton();
        histogramRadioButton = new javax.swing.JRadioButton();
        distribPlotPanel = new javax.swing.JPanel();
        dotplotRadioButton = new javax.swing.JRadioButton();
        horizBoxplotRadioButton = new javax.swing.JRadioButton();
        pairsButton = new javax.swing.JButton();
        tScoreButton = new javax.swing.JButton();

        graphDisplayPopupMenu.setInvoker(graphDisplay);
        graphDisplayPopupMenu.setName("graphDisplayPopupMenu"); // NOI18N

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(facskin.FacsKinApp.class).getContext().getActionMap(KineticsPanel.class, this);
        jMenuItem1.setAction(actionMap.get("saveImageClicked")); // NOI18N
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(facskin.FacsKinApp.class).getContext().getResourceMap(KineticsPanel.class);
        jMenuItem1.setText(resourceMap.getString("jMenuItem1.text")); // NOI18N
        jMenuItem1.setName("jMenuItem1"); // NOI18N
        graphDisplayPopupMenu.add(jMenuItem1);

        tableColumnPopupMenu.setName("tableColumnPopupMenu"); // NOI18N

        jMenuItem7.setAction(actionMap.get("copyParameterDistributionClicked")); // NOI18N
        jMenuItem7.setText(resourceMap.getString("jMenuItem7.text")); // NOI18N
        jMenuItem7.setActionCommand(resourceMap.getString("jMenuItem7.actionCommand")); // NOI18N
        jMenuItem7.setName("jMenuItem7"); // NOI18N
        tableColumnPopupMenu.add(jMenuItem7);

        jMenuItem4.setAction(actionMap.get("copyToClipboardClicked")); // NOI18N
        jMenuItem4.setName("jMenuItem4"); // NOI18N
        tableColumnPopupMenu.add(jMenuItem4);

        jMenuItem2.setAction(actionMap.get("copyRCode")); // NOI18N
        jMenuItem2.setText(resourceMap.getString("jMenuItem2.text")); // NOI18N
        jMenuItem2.setName("jMenuItem2"); // NOI18N
        tableColumnPopupMenu.add(jMenuItem2);

        kineticsTableRowPopupMenu.setName("kineticsTableRowPopupMenu"); // NOI18N

        kineticsMenuItem.setText(resourceMap.getString("kineticsMenuItem.text")); // NOI18N
        kineticsMenuItem.setEnabled(false);
        kineticsMenuItem.setName("kineticsMenuItem"); // NOI18N
        kineticsTableRowPopupMenu.add(kineticsMenuItem);

        jMenuItem11.setAction(actionMap.get("getInfoClicked")); // NOI18N
        jMenuItem11.setText(resourceMap.getString("jMenuItem11.text")); // NOI18N
        jMenuItem11.setName("jMenuItem11"); // NOI18N
        kineticsTableRowPopupMenu.add(jMenuItem11);

        jMenuItem3.setAction(actionMap.get("removeRowClicked")); // NOI18N
        jMenuItem3.setName("jMenuItem3"); // NOI18N
        kineticsTableRowPopupMenu.add(jMenuItem3);

        jMenuItem14.setAction(actionMap.get("renameKineticsFileClicked")); // NOI18N
        jMenuItem14.setName("jMenuItem14"); // NOI18N
        kineticsTableRowPopupMenu.add(jMenuItem14);

        jSeparator1.setName("jSeparator1"); // NOI18N
        kineticsTableRowPopupMenu.add(jSeparator1);

        jMenuItem15.setAction(actionMap.get("copyParameterDistributionsClicked")); // NOI18N
        jMenuItem15.setText(resourceMap.getString("jMenuItem15.text")); // NOI18N
        jMenuItem15.setName("jMenuItem15"); // NOI18N
        kineticsTableRowPopupMenu.add(jMenuItem15);

        jMenuItem5.setAction(actionMap.get("copyToClipboardClicked")); // NOI18N
        jMenuItem5.setName("jMenuItem5"); // NOI18N
        kineticsTableRowPopupMenu.add(jMenuItem5);

        jMenuItem8.setAction(actionMap.get("copyRCode")); // NOI18N
        jMenuItem8.setText(resourceMap.getString("jMenuItem8.text")); // NOI18N
        jMenuItem8.setName("jMenuItem8"); // NOI18N
        kineticsTableRowPopupMenu.add(jMenuItem8);

        tableGeneralPopupMenu.setName("tableGeneralPopupMenu"); // NOI18N

        jMenuItem6.setAction(actionMap.get("copyToClipboardClicked")); // NOI18N
        jMenuItem6.setName("jMenuItem6"); // NOI18N
        tableGeneralPopupMenu.add(jMenuItem6);

        jMenuItem10.setAction(actionMap.get("copyRCode")); // NOI18N
        jMenuItem10.setText(resourceMap.getString("jMenuItem10.text")); // NOI18N
        jMenuItem10.setName("jMenuItem10"); // NOI18N
        tableGeneralPopupMenu.add(jMenuItem10);

        jMenuItem16.setAction(actionMap.get("copyParameterDistributionsClicked")); // NOI18N
        jMenuItem16.setText(resourceMap.getString("jMenuItem16.text")); // NOI18N
        jMenuItem16.setActionCommand(resourceMap.getString("jMenuItem16.actionCommand")); // NOI18N
        jMenuItem16.setName("jMenuItem16"); // NOI18N
        groupTableRowPopupMenu.add(jMenuItem16);

        jMenuItem9.setAction(actionMap.get("copyToClipboardClicked")); // NOI18N
        jMenuItem9.setName("jMenuItem9"); // NOI18N
        groupTableRowPopupMenu.add(jMenuItem9);

        jMenuItem12.setAction(actionMap.get("copyRCode")); // NOI18N
        jMenuItem12.setText(resourceMap.getString("jMenuItem12.text")); // NOI18N
        jMenuItem12.setName("jMenuItem12"); // NOI18N
        groupTableRowPopupMenu.add(jMenuItem12);

        distribPlotPopupMenu.setName("distribPlotPopupMenu"); // NOI18N

        jMenuItem13.setAction(actionMap.get("saveDistribAsPNGClicked")); // NOI18N
        jMenuItem13.setName("jMenuItem13"); // NOI18N
        distribPlotPopupMenu.add(jMenuItem13);

        setName("Form"); // NOI18N

        useOnlyMedianFunctionsCheckbox.setAction(actionMap.get("useOnlyMedianFunctionsCheckboxChanged")); // NOI18N
        useOnlyMedianFunctionsCheckbox.setText(resourceMap.getString("useOnlyMedianFunctionsCheckbox.text")); // NOI18N
        useOnlyMedianFunctionsCheckbox.setName("useOnlyMedianFunctionsCheckbox"); // NOI18N

        groupDataButton.setAction(actionMap.get("groupDataButtonClicked")); // NOI18N
        groupDataButton.setText(resourceMap.getString("groupDataButton.text")); // NOI18N
        groupDataButton.setMaximumSize(new java.awt.Dimension(170, 25));
        groupDataButton.setMinimumSize(new java.awt.Dimension(170, 25));
        groupDataButton.setName("groupDataButton"); // NOI18N
        groupDataButton.setPreferredSize(new java.awt.Dimension(170, 25));

        jSplitPane1.setDividerLocation(300);
        jSplitPane1.setDividerSize(8);
        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPane1.setName("jSplitPane1"); // NOI18N

        tablePanel.setName("tablePanel"); // NOI18N
        tablePanel.setLayout(new java.awt.CardLayout());

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        kineticsTable.setAutoCreateColumnsFromModel(false);
        kineticsTable.setModel(kineticsTableModel);
        kineticsTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        kineticsTable.setName("kineticsTable"); // NOI18N
        kineticsTable.setRowHeight(21);
        jScrollPane1.setViewportView(kineticsTable);

        tablePanel.add(jScrollPane1, "kineticsTable");

        jSplitPane1.setRightComponent(tablePanel);

        jSplitPane2.setDividerLocation(500);
        jSplitPane2.setDividerSize(8);
        jSplitPane2.setName("jSplitPane2"); // NOI18N

        graphDisplay.setName("graphDisplay"); // NOI18N

        javax.swing.GroupLayout graphDisplayLayout = new javax.swing.GroupLayout(graphDisplay);
        graphDisplay.setLayout(graphDisplayLayout);
        graphDisplayLayout.setHorizontalGroup(
            graphDisplayLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 499, Short.MAX_VALUE)
        );
        graphDisplayLayout.setVerticalGroup(
            graphDisplayLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 297, Short.MAX_VALUE)
        );

        jSplitPane2.setLeftComponent(graphDisplay);

        jPanel1.setName("jPanel1"); // NOI18N

        boxplotRadioButton.setAction(actionMap.get("boxplotRadioClicked")); // NOI18N
        boxplotRadioButton.setText(resourceMap.getString("boxplotRadioButton.text")); // NOI18N
        boxplotRadioButton.setName("boxplotRadioButton"); // NOI18N

        histogramRadioButton.setAction(actionMap.get("histogramRadioClicked")); // NOI18N
        histogramRadioButton.setText(resourceMap.getString("histogramRadioButton.text")); // NOI18N
        histogramRadioButton.setName("histogramRadioButton"); // NOI18N

        distribPlotPanel.setName("distribPlotPanel"); // NOI18N
        distribPlotPanel.setLayout(new java.awt.CardLayout());

        dotplotRadioButton.setAction(actionMap.get("dotplotRadioClicked")); // NOI18N
        dotplotRadioButton.setText(resourceMap.getString("dotplotRadioButton.text")); // NOI18N
        dotplotRadioButton.setName("dotplotRadioButton"); // NOI18N

        horizBoxplotRadioButton.setAction(actionMap.get("horizBoxplotRadioClicked")); // NOI18N
        horizBoxplotRadioButton.setText(resourceMap.getString("horizBoxplotRadioButton.text")); // NOI18N
        horizBoxplotRadioButton.setName("horizBoxplotRadioButton"); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(boxplotRadioButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(histogramRadioButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dotplotRadioButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(horizBoxplotRadioButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(distribPlotPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 386, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(distribPlotPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 272, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(boxplotRadioButton)
                    .addComponent(histogramRadioButton)
                    .addComponent(dotplotRadioButton)
                    .addComponent(horizBoxplotRadioButton)))
        );

        jSplitPane2.setRightComponent(jPanel1);

        jSplitPane1.setLeftComponent(jSplitPane2);

        pairsButton.setAction(actionMap.get("pairButtonClicked")); // NOI18N
        pairsButton.setText(resourceMap.getString("pairsButton.text")); // NOI18N
        pairsButton.setMaximumSize(new java.awt.Dimension(170, 25));
        pairsButton.setMinimumSize(new java.awt.Dimension(170, 25));
        pairsButton.setName("pairsButton"); // NOI18N
        pairsButton.setPreferredSize(new java.awt.Dimension(170, 25));

        tScoreButton.setAction(actionMap.get("tScoreButtonClicked")); // NOI18N
        tScoreButton.setText(resourceMap.getString("tScoreButton.text")); // NOI18N
        tScoreButton.setMaximumSize(new java.awt.Dimension(170, 25));
        tScoreButton.setMinimumSize(new java.awt.Dimension(170, 25));
        tScoreButton.setName("tScoreButton"); // NOI18N
        tScoreButton.setPreferredSize(new java.awt.Dimension(170, 25));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(useOnlyMedianFunctionsCheckbox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 171, Short.MAX_VALUE)
                .addComponent(tScoreButton, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pairsButton, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(groupDataButton, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 896, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 479, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(useOnlyMedianFunctionsCheckbox)
                    .addComponent(groupDataButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pairsButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tScoreButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton boxplotRadioButton;
    private javax.swing.JPanel distribPlotPanel;
    private javax.swing.JPopupMenu distribPlotPopupMenu;
    private javax.swing.JRadioButton dotplotRadioButton;
    private javax.swing.JPanel graphDisplay;
    private javax.swing.JPopupMenu graphDisplayPopupMenu;
    private javax.swing.JButton groupDataButton;
    private javax.swing.JPopupMenu groupTableRowPopupMenu;
    private javax.swing.JRadioButton histogramRadioButton;
    private javax.swing.JRadioButton horizBoxplotRadioButton;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem15;
    private javax.swing.JMenuItem jMenuItem16;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JMenuItem kineticsMenuItem;
    private javax.swing.JTable kineticsTable;
    private javax.swing.JPopupMenu kineticsTableRowPopupMenu;
    private javax.swing.JButton pairsButton;
    private javax.swing.JButton tScoreButton;
    private javax.swing.JPopupMenu tableColumnPopupMenu;
    private javax.swing.JPopupMenu tableGeneralPopupMenu;
    private javax.swing.JPanel tablePanel;
    private javax.swing.JCheckBox useOnlyMedianFunctionsCheckbox;
    // End of variables declaration//GEN-END:variables
}
