package facskin.kinetics;

import facskin.FacsKinApp;
import facskin.formats.FunctionData;
import facskin.formats.KineticsData;
import facskin.formats.ParameterData;
import facskin.math.Functions;
import facskin.math.MyDecimalFormat;
import java.awt.Color;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 * TableModel showing kinetics data.
 * @author ambi
 */
public class KineticsTableModel extends AbstractTableModel {
    private KineticsPanel father;
    private List<KineticsData> datas;
    private List<KineticsData> pairs;
    private SelectionData sel;
    private List<String> groups;
    private String newGroupString;

    private String[] columnNames;

    public KineticsTableModel(KineticsPanel father, List<KineticsData> datas, SelectionData sel, List<String> groups, List<KineticsData> pairs, String newGroupString) {
        this.father = father;
        this.datas = datas;
        this.pairs = pairs;
        this.sel = sel;
        this.groups = groups;
        this.newGroupString = newGroupString;
        
        columnNames = new String[9];
        columnNames[0] = "Filename";
        columnNames[1] = "Group";
        columnNames[2] = "Pair";
        columnNames[3] = Functions.getFunctionName(0);
        columnNames[4] = Functions.getFunctionName(1);
        columnNames[5] = Functions.getFunctionName(2);
        columnNames[6] = Functions.getFunctionName(3);
        columnNames[7] = Functions.getFunctionName(4);
        columnNames[8] = "SAD";
    }

    @Override
    public int getColumnCount() {
        if (sel.getFun() == -1)
            return 9;
        return 9 + Functions.parameterCount(sel.getFun());
    }

    @Override
    public int getRowCount() {
        return datas.size();
    }

    private Object getUnformattedValueAt(int row, int col) {
        if (col == 0)
            return datas.get(row).getFilename();
        if (col == 1)
            return groups.get(row);
        if (col == 2)
            return pairs.get(row) == null ? "" : pairs.get(row).getFilename();
        if (col >= 3 && col < 8)
            return MyDecimalFormat.format(datas.get(row).getFunction(col - 3).get_cv_value());
        if (col == 8)
            return MyDecimalFormat.format(datas.get(row).getFunction(sel.getFun()).get_sad());

        if (FacsKinApp.getUseOnlyMedians())
            return MyDecimalFormat.format(datas.get(row).getFunction(sel.getFun()).getParam(col - 9).getMidData());
        else
            return MyDecimalFormat.format(datas.get(row).getFunction(sel.getFun()).getParam(col - 9).getQuantile(0.5)) +
                    " [" +
                    MyDecimalFormat.format(datas.get(row).getFunction(sel.getFun()).getParam(col - 9).getQuantile(0.25)) +
                    ", " +
                    MyDecimalFormat.format(datas.get(row).getFunction(sel.getFun()).getParam(col - 9).getQuantile(0.75)) +
                    "]";
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (col == 0) {
            Color color;
            if (FacsKinApp.getColorByGroups())
                color = ListColorScale.getColor(father.getGroupNumber(row), father.getNumberOfGroups());
            else
                color = ListColorScale.getColor(row, datas.size());
            return "<html><font color=\"#" + give0(Integer.toString(color.getRed(), 16)) + give0(Integer.toString(color.getGreen(), 16)) + give0(Integer.toString(color.getBlue(), 16)) + "\">" + getUnformattedValueAt(row, col) + "</font></html>";
        }
        if (3 <= col && col < 8 && col - 3 == datas.get(row).getBestFunction())
            return "<html><font color=\"red\">" + getUnformattedValueAt(row, col) + "</font></html>";

        KineticsData data = datas.get(row);
        double st = data.getStandardizer();
        if (col >= 9 && Functions.isParameterOutside(sel.getFun(), col-9, data.getFunction(sel.getFun()).getMedianParams(), data.getMinX(), data.getMaxX(), data.getMinY()/st, data.getMaxY()/st, data.getFirstY()/st, data.getLastY()/st))
            return "<html><font color=\"#EECC88\">" + getUnformattedValueAt(row, col) + "</font></html>";
        return getUnformattedValueAt(row, col);
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        if (col == 1) {
            if (!((String) value).equals(newGroupString))
                groups.set(row, (String) value);
            else {
                String response = JOptionPane.showInputDialog(father.getFather().getFrame(), "New group:", "");
                if (response != null && response.length() > 0) {
                    groups.set(row, response);
                    father.reloadComboBoxEditor();
                }
            }
        }
        if (col == 2) {
            if (((String) value).equals(""))
                pairs.set(row, null);
            else {
                int i = 0;
                while (!datas.get(i).getFilename().equals((String) value))
                    ++i;
                pairs.set(row, datas.get(i));
            }
        }
    }

    private String getUnformattedColumnName(int col) {
        if (col < 3)
            return columnNames[col];
        if (col < 9 && sel.getFun() != col - 3)
            return columnNames[col];
        if (col < 9 && sel.getFun() == col - 3)
            return columnNames[col];
        if (FacsKinApp.getUseOnlyMedians())
            return "Median (" + Functions.getParameterName(sel.getFun(), col - 9) + ")";
        else
            return Functions.getParameterName(sel.getFun(), col - 9);
    }

    @Override
    public String getColumnName(int col) {
        String s = getUnformattedColumnName(col);
        if (3 <= col && col < 9 && sel.getFun() == col - 3)
            return "<html><b>" + s + "</b></html>";
        else
            return s;
    }

    @Override
    public Class getColumnClass(int col) {
        if (col == 1)
            return Boolean.class;
        return String.class;
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        if (col == 1 || col == 2)
            return true;
        return false;
    }

    public String getContentsAsString() {
        String s = "";
        for (int j = 0; j < getColumnCount() - 1; j ++) {
            if (sel.getFun() + 3 == j) s += "*";
            s += getUnformattedColumnName(j);
            if (sel.getFun() + 3 == j) s += "*";
            s += "\t";
        }
        s += getColumnName(getColumnCount() - 1) + "\n";
        for (int i = 0; i < getRowCount(); i ++) {
            for (int j = 0; j < getColumnCount() - 1; j ++)
                s += getUnformattedValueAt(i, j) + "\t";
            s += getUnformattedValueAt(i, getColumnCount() - 1) + "\n";
        }
        return s;
    }

    public String getRCode() {
        String s = "fct <- \"" + Functions.getFunctionName(sel.getFun()) + "\"\n";
        s += "params <- list()\n";
        for (int i = 0; i < datas.size(); ++i) {
            FunctionData data = datas.get(i).getFunction(sel.getFun());
            for (int p = 0; p < data.getParamCount(); ++p) {
                s += "params[[\"" + datas.get(i).getFilename().replace("\"", "\\\"") + "\"]]";
                ParameterData pd = data.getParam(p);
                s += "[[\"" + pd.getName() + "\"]] <- ";
                if (FacsKinApp.getUseOnlyMedians()) {
                    s += MyDecimalFormat.formatLongPoint(pd.getMidData());
                } else {
                    s += "c(";
                    for (int j = 0; j < pd.getDataLength() - 1; ++ j)
                        s += MyDecimalFormat.formatLongPoint(pd.getData(j)) + ", ";
                    s += MyDecimalFormat.formatLongPoint(pd.getData(pd.getDataLength() - 1));
                    s += ")";
                }
                s += "\n";
            }
        }
        s += "metadata <- as.data.frame(cbind(names(params), NA))\n";
        s += "names(metadata) <- c(\"name\", \"size\")\n";
        s += "metadata$name <- as.character(metadata$name)\n";
        s += "metadata$size <- as.numeric(metadata$size)\n";
        s += "for (g in 1:length(params)) metadata$size[g] <- length(params[[g]][[1]])\n";
        s += "rm(g)\n";
        s += "print(\"Selected function:\")\n";
        s += "fct\n";
        s += "print(\"Names and sizes of groups:\")\n";
        s += "metadata\n";
        s += "print(\"Names of parameters:\")\n";
        s += "names(params[[1]])\n";
        return s;
    }

    public String getDistributionsFor1ParameterAsString(int param) {
        String s = "";
        for (int i = 0; i < datas.size(); i ++) {
            s += datas.get(i).getFilename() + "\t";
            s += datas.get(i).getFunction(sel.getFun()).getName() + "\t";
            s += Functions.getParameterName(sel.getFun(), param) + "\t";
            if (FacsKinApp.getUseOnlyMedians())
                s += MyDecimalFormat.formatLong(datas.get(i).getFunction(sel.getFun()).getParam(param).getMidData());
            else
                for (int j = 0; j < datas.get(i).getFunction(sel.getFun()).getParam(param).getDataLength(); j ++)
                    s += MyDecimalFormat.formatLong(datas.get(i).getFunction(sel.getFun()).getParam(param).getData(j)) + "\t";
            s += "\n";
        }
        return s;
    }

    public String getDistributionsFor1DataAsString(int data) {
        String s = "";
        for (int param = 0; param < Functions.parameterCount(sel.getFun()); param ++) {
            s += datas.get(data).getFilename() + "\t";
            s += datas.get(data).getFunction(sel.getFun()).getName() + "\t";
            s += Functions.getParameterName(sel.getFun(), param) + "\t";
            if (FacsKinApp.getUseOnlyMedians())
                s += MyDecimalFormat.formatLong(datas.get(data).getFunction(sel.getFun()).getParam(param).getMidData());
            else
                for (int j = 0; j < datas.get(data).getFunction(sel.getFun()).getParam(param).getDataLength(); j ++)
                    s += MyDecimalFormat.formatLong(datas.get(data).getFunction(sel.getFun()).getParam(param).getData(j)) + "\t";
            s += "\n";
        }
        return s;        
    }

    private String give0(String s) {
        if (s.length() == 1)
            return "0" + s;
        else
            return s;
    }

}
