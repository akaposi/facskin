package facskin.kinetics;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * TableCellRenderer with support for setting background and printing text in gary color.
 * @author ambi
 */
public class StringCellRenderer extends DefaultTableCellRenderer {
    private int selectedColumn = -1;
    private int selectedRow = -1;
    private boolean makeGrayColumns;

    public StringCellRenderer(boolean makeGrayColumns) {
        this.makeGrayColumns = makeGrayColumns;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if (column == selectedColumn || row == selectedRow) {
            c.setForeground(table.getSelectionForeground());
            c.setBackground(table.getSelectionBackground());
        }  else {
            if (3 <= column && column <= 7 && makeGrayColumns)
                c.setBackground(new Color(240, 240, 240));
            else {
                c.setForeground(table.getForeground());
                c.setBackground(table.getBackground());
            }
        }
        return c;
    }

    void setSelectedColumn(int selectedColumn) {
        this.selectedColumn = selectedColumn;
    }

    void setSelectedRow(int selectedRow) {
        this.selectedRow = selectedRow;
    }
}
