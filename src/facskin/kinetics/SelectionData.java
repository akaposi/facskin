package facskin.kinetics;

/**
 * Class representing a selected function, selected kinetics data and selected
 * parameter of a list of kinetics files. It has also support for paired data.
 * @author ambi
 */
public class SelectionData {
    private int selectedKineticsData = -1;
    private int selectedParameter = -1;
    private int selectedFunction = -1;
    private int selectedGroup = -1;
    private int selectedPairsData = -1;

    public SelectionData() {
    }

    public int getKin() {
        return selectedKineticsData;
    }

    public void setKin(int i) {
        selectedKineticsData = i;
    }

    public int getPar() {
        return selectedParameter;
    }

    public void setPar(int i) {
        selectedParameter = i;
    }

    public int getFun() {
        return selectedFunction;
    }

    public void setFun(int i) {
        selectedFunction = i;
    }

    public int getGroup() {
        return selectedGroup;
    }

    public void setGroup(int i) {
        selectedGroup = i;
    }

    public int getPairs() {
        return selectedPairsData;
    }

    public void setPairs(int i) {
        selectedPairsData = i;
    }
}
