package facskin.kinetics;

import facskin.formats.Data;
import facskin.math.Functions;
import facskin.math.MyDecimalFormat;
import java.awt.Color;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * TableModel showing grouped KineticsData.
 * @author ambi
 */
public class GroupTableModel extends AbstractTableModel {
    private List<List<Data>> datas;
    private List<String> names;
    private List<Color> colors;
    private SelectionData sel;
    private List<double[]> comparisonResults;
    private int descriptionColumnCount = 2;

    public GroupTableModel(List<List<Data>> datas, List<String> names, List<Color> colors, SelectionData sel, List<double[]> comparisonResults) {
        this.datas = datas;
        this.names = names;
        this.colors = colors;
        this.sel = sel;
        this.comparisonResults = comparisonResults;
    }

    @Override
    public int getRowCount() {
        if (datas.isEmpty())
            return 0;
        else
            return datas.size() + 2;
    }

    @Override
    public int getColumnCount() {
        if (datas.isEmpty())
            return 0;
        else
            return datas.get(0).size() + descriptionColumnCount;
    }

    public int getDescriptionColumnCount() {
        return descriptionColumnCount;
    }

    @Override
    public Object getValueAt(int row, int col) {
        String str = "";
        boolean was_formatting = false;
        if (col == 0 && row < datas.size()) {
            Color color = colors.get(row);
            str += "<html><font color=\"#" + give0(Integer.toString(color.getRed(), 16)) + give0(Integer.toString(color.getGreen(), 16)) + give0(Integer.toString(color.getBlue(), 16)) + "\">";
            was_formatting = true;
        }
        if (col >= descriptionColumnCount && row >= datas.size()) {
            Color color;
            if (col >= descriptionColumnCount && row == datas.size() + 1 && comparisonResults.get(col-descriptionColumnCount)[1] < 0.05)
                color = Color.RED;
            else
                color = Color.GRAY;
            str += "<html><font color=\"#" + give0(Integer.toString(color.getRed(), 16)) + give0(Integer.toString(color.getGreen(), 16)) + give0(Integer.toString(color.getBlue(), 16)) + "\">";
            was_formatting = true;
        }
        str += (String) getUnformattedValueAt(row, col);
        if (was_formatting)
            str += "</font></html>";
        return str;
    }

    public Object getUnformattedValueAt(int row, int col) {
        if (row < datas.size()) {
            if (col == 0)
                return names.get(row);
            if (col == 1)
                return String.valueOf(datas.get(row).get(0).getDataLength());
            return MyDecimalFormat.format(datas.get(row).get(col-descriptionColumnCount).getQuantile(0.5)) +
                    " [" +
                    MyDecimalFormat.format(datas.get(row).get(col-descriptionColumnCount).getQuantile(0.25)) +
                    ", " +
                    MyDecimalFormat.format(datas.get(row).get(col-descriptionColumnCount).getQuantile(0.75)) +
                    "]";
        } else {
            if (col == 0 && row == datas.size()) return "Chi-squared value (Kruskal-Wallis test)";
            if (col == 0 && row == datas.size() + 1) return "p value (Kruskal-Wallis test)";
            if (col == 1) return "";
            //if (col >= descriptionColumnCount)
            return MyDecimalFormat.format(comparisonResults.get(col-descriptionColumnCount)[row - datas.size()]);
        }
    }

    @Override
    public void setValueAt(Object value, int row, int col) {}

    @Override
    public String getColumnName(int col) {
        if (col == 0)
            return "group";
        else {
            if (col == 1)
                return "size";
            return datas.get(0).get(col-descriptionColumnCount).getName();
        }
    }

    @Override
    public Class getColumnClass(int col) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int row, int col) { return false; }

    private String give0(String s) {
        if (s.length() == 1) return "0" + s; else return s;
    }

    String getContentsAsString() {
        String s = "";
        for (int j = 0; j < getColumnCount() - 1; j ++)
            s += getColumnName(j) + "\t";
        s += getColumnName(getColumnCount() - 1) + "\n";
        for (int i = 0; i < getRowCount(); i ++) {
            for (int j = 0; j < getColumnCount() - 1; j ++)
                s += getUnformattedValueAt(i, j) + "\t";
            s += getUnformattedValueAt(i, getColumnCount() - 1) + "\n";
        }
        return s;
    }

    public String getRCode() {
        String s = "fct <- \"" + Functions.getFunctionName(sel.getFun()) + "\"\n";
        s += "params <- list()\n";
        for (int i = 0; i < datas.size(); ++i) {
            for (int p = 0; p < Functions.parameterCount(sel.getFun()); ++p) {
                s += "params[[\"" + names.get(i).replace("\"", "\\\"") + "\"]]";
                Data pd = datas.get(i).get(p);
                s += "[[\"" + pd.getName() + "\"]] <- ";
                s += "c(";
                for (int j = 0; j < pd.getDataLength() - 1; ++ j)
                    s += MyDecimalFormat.formatLongPoint(pd.getData(j)) + ", ";
                s += MyDecimalFormat.formatLongPoint(pd.getData(pd.getDataLength() - 1));
                s += ")";
                s += "\n";
            }
        }
        s += "metadata <- as.data.frame(cbind(names(params), NA))\n";
        s += "names(metadata) <- c(\"name\", \"size\")\n";
        s += "metadata$name <- as.character(metadata$name)\n";
        s += "metadata$size <- as.numeric(metadata$size)\n";
        s += "for (g in 1:length(params)) metadata$size[g] <- length(params[[g]][[1]])\n";
        s += "rm(g)\n";
        s += "print(\"Selected function:\")\n";
        s += "fct\n";
        s += "print(\"Names and sizes of groups:\")\n";
        s += "metadata\n";
        s += "print(\"Names of parameters:\")\n";
        s += "names(params[[1]])\n";
        return s;
    }

    String getDistributionsFor1ParameterAsString(int par) {
        String s = "";
        for (int i = 0; i < datas.size(); i ++) {
            s += names.get(i) + "\t";
            s += datas.get(i).get(par).getName() + "\t";
            for (int j = 0; j < datas.get(i).get(par).getDataLength(); j ++)
                s += MyDecimalFormat.formatLong(datas.get(i).get(par).getData(j)) + "\t";
            s += "\n";
        }
        return s;
    }

    String getDistributionsFor1DataAsString(int group) {
        String s = "";
        for (int param = 0; param < datas.get(group).size(); param ++) {
            s += names.get(group) + "\t";
            s += datas.get(group).get(param).getName() + "\t";
            for (int j = 0; j < datas.get(group).get(param).getDataLength(); j ++)
                s += MyDecimalFormat.formatLong(datas.get(group).get(param).getData(j)) + "\t";
            s += "\n";
        }
        return s;
    }
}
