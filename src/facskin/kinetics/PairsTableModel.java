package facskin.kinetics;

import facskin.FacsKinApp;
import facskin.math.Functions;
import facskin.math.MyDecimalFormat;
import java.awt.Color;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * TableModel for paired KineticsData.
 * @author ambi
 */
public class PairsTableModel extends AbstractTableModel {
    private KineticsPanel father;
    private List<List<Double>> datas;
    private List<String> groups;
    private List<String> names;
    private List<String> parNames;
    private SelectionData sel;

    private int descriptionColumnCount = 2;

    public PairsTableModel(KineticsPanel father, List<List<Double>> datas, List<String> groups, List<String> names, List<String> parNames, SelectionData sel) {
        this.father = father;
        this.datas = datas;
        this.groups = groups;
        this.names = names;
        this.parNames = parNames;
        this.sel = sel;
    }

    @Override
    public int getRowCount() { return datas.size(); }

    @Override
    public int getColumnCount() {
        if (datas.isEmpty()) return 0; else return datas.get(0).size() + descriptionColumnCount;
    }

    public int getDescriptionColumnCount() {
        return descriptionColumnCount;
    }

    @Override
    public Object getValueAt(int row, int col) {
        String str = "";
        if (col == 0) {
            Color color;
            if (FacsKinApp.getColorByGroups())
                color = ListColorScale.getColor(father.getPairsGroupNumber(row), father.getNumberOfGroups());
            else
                color = ListColorScale.getColor(row, datas.size());
            str += "<html><font color=\"#" + give0(Integer.toString(color.getRed(), 16)) + give0(Integer.toString(color.getGreen(), 16)) + give0(Integer.toString(color.getBlue(), 16)) + "\">";
        }
        str += (String) getUnformattedValueAt(row, col);
        if (col == 0)
            str += "</font></html>";
        return str;
    }

    public Object getUnformattedValueAt(int row, int col) {
        if (col == 0)
            return names.get(row);
        if (col == 1)
            return groups.get(row);
        return MyDecimalFormat.format(datas.get(row).get(col-descriptionColumnCount));
    }

    @Override
    public void setValueAt(Object value, int row, int col) {}

    @Override
    public String getColumnName(int col) {
        if (col == 0)
            return "Filename";
        if (col == 1)
            return "group";
        return parNames.get(col-descriptionColumnCount);
    }

    @Override
    public Class getColumnClass(int col) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int row, int col) { return false; }

    private String give0(String s) {
        if (s.length() == 1) return "0" + s; else return s;
    }

    String getContentsAsString() {
        String s = "";
        for (int j = 0; j < getColumnCount() - 1; j ++)
            s += getColumnName(j) + "\t";
        s += getColumnName(getColumnCount() - 1) + "\n";
        for (int i = 0; i < getRowCount(); i ++) {
            for (int j = 0; j < getColumnCount() - 1; j ++)
                s += getUnformattedValueAt(i, j) + "\t";
            s += getUnformattedValueAt(i, getColumnCount() - 1) + "\n";
        }
        return s;
    }

    public String getRCode() {
        String s = "fct <- \"" + Functions.getFunctionName(sel.getFun()) + "\"\n";
        s += "params <- list()\n";
        for (int i = 0; i < datas.size(); ++i) {
            for (int p = 0; p < Functions.parameterCount(sel.getFun()); ++p) {
                s += "params[[\"" + names.get(i).replace("\"", "\\\"") + "\"]]";
                s += "[[\"" + parNames.get(p) + "\"]] <- ";
                s += MyDecimalFormat.formatLongPoint(datas.get(i).get(p));
                s += "\n";
            }
        }
        s += "metadata <- as.data.frame(cbind(names(params), NA))\n";
        s += "names(metadata) <- c(\"name\", \"size\")\n";
        s += "metadata$name <- as.character(metadata$name)\n";
        s += "metadata$size <- as.numeric(metadata$size)\n";
        s += "for (g in 1:length(params)) metadata$size[g] <- length(params[[g]][[1]])\n";
        s += "rm(g)\n";
        s += "print(\"Selected function:\")\n";
        s += "fct\n";
        s += "print(\"Names and sizes of groups:\")\n";
        s += "metadata\n";
        s += "print(\"Names of parameters:\")\n";
        s += "names(params[[1]])\n";
        return s;
    }

}
