package facskin.kinetics;

import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 * ComboBoxModel for selection of groups.
 * @author ambi
 */
public class GroupComboBoxModel implements ComboBoxModel {
    private KineticsPanel father;
    protected String selected = null;
    private String newGroupString;

    public GroupComboBoxModel(KineticsPanel father, String newGroupString) {
        this.father = father;
        this.newGroupString = newGroupString;
    }

    @Override
    public void setSelectedItem(Object object) {
        selected = (String) object;
    }

    @Override
    public Object getSelectedItem() {
        return selected;
    }

    private boolean is_first(int l) {
        int j = 0;
        while (j < l && !father.getGroups().get(j).equals(father.getGroups().get(l)))
            ++j;
        return (j==l);
    }

    @Override
    public int getSize() {
        return father.getNumberOfGroups() + 1;
    }

    @Override
    public Object getElementAt(int i) {
        int k = 0;
        int l = 0; // groups-ban az ennyiedik lesz, amit keresunk
        while (k < i) {
            ++l;
            while (l < father.getGroups().size() && !is_first(l))
                ++l;
            ++k;
        }
        if (l==father.getGroups().size()) {
            return newGroupString;
        } else {
            return father.getGroups().get(l);
        }
    }

    @Override
    public void addListDataListener(ListDataListener listDataListener) {
    }

    @Override
    public void removeListDataListener(ListDataListener listDataListener) {
    }
}
