package facskin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Class providing a network socket for detecting new instances of an
 * application. The application should call method {@link #registerInstance(String[])}
 * in it's main function. If it returns false, the application should stop (it
 * is not the first instance), otherwise it is the first instance and should
 * register a listener with method {@link #setApplicationInstanceListener(ApplicationInstanceListener)}.
 * <a href="http://www.rbgrn.net/content/43-java-single-application-instance">Source</a>.
 */
public class ApplicationInstanceManager {
    //

    private static ApplicationInstanceListener subListener;

    /** Randomly chosen, but static, high socket number */
    public static final int SINGLE_INSTANCE_NETWORK_SOCKET = 42539;

    /** Must end with newline */
    public static final String SINGLE_INSTANCE_SHARED_KEY = "$$NewInstance$$\n";

    /**
     * Registers this instance of the application.
     *
     * @return true if first instance, false if not.
     */
    public static boolean registerInstance(final String[] args) {
        // returnValueOnError should be true if lenient (allows app to run on network error) or false if strict.
        boolean returnValueOnError = true;
        // try to open network socket
        // if success, listen to socket for new instance message, return true
        // if unable to open, connect to existing and send new instance message, return false
        try {
            final ServerSocket socket = new ServerSocket(SINGLE_INSTANCE_NETWORK_SOCKET, 10, InetAddress
                    .getLocalHost());
            System.out.println("Listening for application instances on socket " + SINGLE_INSTANCE_NETWORK_SOCKET);
            Thread instanceListenerThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    boolean socketClosed = false;
                    while (!socketClosed) {
                        if (socket.isClosed()) {
                            socketClosed = true;
                        } else {
                            try {
                                Socket client = socket.accept();
                                BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                                String message = in.readLine();
                                String key = message.substring(0, SINGLE_INSTANCE_SHARED_KEY.length() - 1);
                                if (SINGLE_INSTANCE_SHARED_KEY.trim().equals(key.trim())) {
                                    System.out.println("Shared key matched - new application instance found");
                                    int n = Integer.parseInt(in.readLine());
                                    String[] arguments = new String[n];
                                    for (int i = 0; i < n; i ++) {
                                        String line = in.readLine();
                                        line.replaceAll("##", "#");
                                        line.replaceAll("#n", "\n");
                                        arguments[i] = line;
                                    }
                                    subListener.newInstanceCreated(arguments);
                                }
                                in.close();
                                client.close();
                            } catch (IOException e) {
                                socketClosed = true;
                            }
                        }
                    }
                }
            });
            instanceListenerThread.start();
            // listen
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return returnValueOnError;
        } catch (IOException e) {
            System.out.println("Port is already taken.  Notifying first instance.");
            try {
                Socket clientSocket = new Socket(InetAddress.getLocalHost(), SINGLE_INSTANCE_NETWORK_SOCKET);
                OutputStream out = clientSocket.getOutputStream();
                String arguments = "";
                for (int i = 0; i < args.length; i ++) {
                    args[i].replaceAll("#", "##");
                    args[i].replaceAll("\n", "#n");
                    arguments += args[i] + "\n";
                }
                String length = args.length + "\n";
                out.write((SINGLE_INSTANCE_SHARED_KEY + length + arguments).getBytes());
                out.close();
                clientSocket.close();
                System.out.println("Successfully notified first instance.");
                return false;
            } catch (UnknownHostException e1) {
                e1.printStackTrace();
                return returnValueOnError;
            } catch (IOException e1) {
                System.out.println("Error connecting to local port for single instance notification");
                e1.printStackTrace();
                return returnValueOnError;
            }

        }
        return true;
    }

    /**
     * Sets the listener function of the application.
     * @param listener This object's newInstanceCreated(String[])
     *                 method is called when a new instance created.
     */
    public static void setApplicationInstanceListener(ApplicationInstanceListener listener) {
        subListener = listener;
    }
}
